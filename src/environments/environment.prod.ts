export const environment = {
  production: true,
  backendApiURL: "http://solace.spicelabs.in:7777/sbi/config/v1/",
  backendApiURLMG: "https://sbi.spicelabs.in/sbi/config/v1/"
  // backendApiURL: "https://smsguiuat.sbi.co.in:8443/sbi/config/v1/",
  // backendApiURLMG: "https://smsguiuat.sbi.co.in:8443/sbi/config/v1/"
};
