import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import { RegisterComponent } from './register/register.component';
import { StatusComponent } from './status/status.component';
import { routing } from './registration.routing';
import { SharedModule } from '../common/shared.modules';
import { RegHomeComponent } from './reg-home/reg-home.component';


@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    routing
  ],
  declarations: [RegistrationComponent, RegisterComponent, StatusComponent, RegHomeComponent]
})
export class RegistrationModule { }