import { Component, OnInit } from "@angular/core";
import { RegistrationService } from "../registration.service";
import { RESPONSE, USER_REG_STATUS } from "src/app/common/common.const";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";
import * as FileSaver from "file-saver";
@Component({
  selector: "app-status",
  templateUrl: "./status.component.html",
  styleUrls: ["./status.component.scss"],
  providers: [RegistrationService]
})
export class StatusComponent implements OnInit {
  requestList: Array<object> = [];
  searchValue: string = "";

  constructor(
    private registrationService: RegistrationService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {}

  getRequestsList() {
    if (this.searchValue.trim().length > 0) {
      this.spinner.show();
      this.registrationService
        .getRequestList(this.searchValue)
        .subscribe(data => {
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            if (data["data"].length > 0) {
              let tmp = data["data"];

              tmp.forEach(element => {
                if (element.l1Status["id"] == USER_REG_STATUS.PENDING) {
                  element["overallStatus"] = "Pending at L1";
                } else if (element.l1Status["id"] == USER_REG_STATUS.REJECTED) {
                  element["overallStatus"] = "Rejected at L1";
                } else if (element.l2Status["id"] == USER_REG_STATUS.PENDING) {
                  element["overallStatus"] = "Pending at L2";
                } else if (element.l2Status["id"] == USER_REG_STATUS.REJECTED) {
                  element["overallStatus"] = "Rejected at L2";
                } else if (element.l3Status["id"] == USER_REG_STATUS.PENDING) {
                  element["overallStatus"] = "Pending at L3";
                } else if (element.l3Status["id"] == USER_REG_STATUS.REJECTED) {
                  element["overallStatus"] = "Rejected at L3";
                } else if (element.l3Status["id"] == USER_REG_STATUS.APPROVED) {
                  element["overallStatus"] = "Approved";
                }
              });
              this.requestList = tmp;
            } else {
              this.requestList = [];
              Swal.fire({
                type: "error",
                title: "No Record Found",
                text: "No record matched to submitted search criteria"
              });
            }
          } else {
            Swal.fire({
              type: "error",
              title: "Error",
              text: "Something went wrong!"
            });
          }
          this.spinner.hide();
        });
    }
  }

  downloadFile(id) {
    this.spinner.show();
    this.registrationService.downloadFile(id).subscribe(data => {
      debugger
      const blob = new Blob([data], { type: "application/pdf" });
      this.spinner.hide();
      FileSaver.saveAs(data, id + "_" + new Date().getTime() + ".pdf");
    },
    err => {
      debugger
    });
  }
}
