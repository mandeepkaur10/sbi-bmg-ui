import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { CommonService } from "../../common/common.service";
import { RegistrationService } from "../registration.service";
import { RESPONSE } from "src/app/common/common.const";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
  providers: [RegistrationService]
})
export class RegisterComponent implements OnInit {
  NEW_REQUEST = "N";
  EXISTING_REQUEST = "E";

  registrationForm: FormGroup;
  isFormSubmitted: boolean = false;
  registrationSubmited: boolean = false;
  formEnabled: boolean = false;

  isUniqueUsername: boolean = true;
  usernameNotFound: boolean = false;
  uniqueUsernameMessage: string = "";

  requestTypeList: Array<object> = [];
  selectedRequestType: object = {};

  fileUploadForm: FormData = null;

  validUsername: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private registartionService: RegistrationService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.prepareRegistrationForm();
    this.getUploadRequestTypeList();
  }

  getUploadRequestTypeList() {
    this.spinner.show();
    this.registartionService.getRequestTypeList().subscribe(
      data => {
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          this.requestTypeList = data["data"];
        } else {
          this.commonService.showErrorAlertMessage("Something went wrong!");
        }
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
        this.commonService.showErrorAlertMessage("Something went wrong!");
      }
    );
  }

  prepareRegistrationForm() {
    this.spinner.show();
    this.registrationForm = this.formBuilder.group({
      type: ["", [Validators.required]],
      email: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.email)
      ]),
      username: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50),
        Validators.pattern(
          this.commonService.patterns.alphaNumericStartWithOnlyAlphabet
        )
      ]),
      formPath: new FormControl({ disabled: true, value: null }, [
        Validators.required
      ]),
      // mobileNumber: [
      //   "",
      //   [
      //     Validators.required,
      //     Validators.pattern(this.commonService.patterns.numberOnly),
      //     Validators.minLength(10),
      //     Validators.maxLength(10)
      //   ]
      // ],
      comments: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(200),
        Validators.pattern(this.commonService.patterns.remarksDescription)
      ])
    });
    this.spinner.hide();
  }

  validateUsername() {
    if (this.registrationForm.controls.username.valid) {
      let value = this.registrationForm.controls.username.value;
      if (this.selectedRequestType["validateUsername"] == this.NEW_REQUEST) {
        this.registartionService
          .validateUsernameRequest(value, 1)
          .subscribe(data => {
            if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
              this.isUniqueUsername = data["data"];
              if (this.isUniqueUsername) this.validUsername = true;
            } else {
              this.isUniqueUsername = false;
              this.validUsername = false;
            }
          });
      } else if (
        this.selectedRequestType["validateUsername"] == this.EXISTING_REQUEST
      ) {
        this.registartionService
          .validateUsernameRequest(value, 2)
          .subscribe(data => {
            if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
              this.usernameNotFound = data["data"];
              if (!this.usernameNotFound) {
                this.validUsername = true;
              }
            } else {
              this.usernameNotFound = true;
              this.validUsername = false;
            }
          });
      }
    }
  }

  requestTypeChaned() {
    if (!this.formEnabled) {
      this.spinner.show();
      this.formEnabled = true;
      this.registrationForm.controls.email.enable();
      this.registrationForm.controls.username.enable();
      this.registrationForm.controls.formPath.enable();
      this.registrationForm.controls.comments.enable();
      this.spinner.hide();
    }

    let val = this.registrationForm.controls.type.value;

    this.selectedRequestType = this.requestTypeList.find(x => x["id"] == val);

    this.registrationForm.controls.username.setValue("");
    this.isUniqueUsername = true;
    this.usernameNotFound = false;
    this.validUsername = false;
  }
  isExceeded: boolean = false;

  uploadFile(event) {
    this.isExceeded = false;
    const file = event.target.files[0];
    if (file) {
      const filemb = file.size / (1024 * 1024);
      const fileExt = file.name.split(".").pop();
      if (
        file.type.indexOf("pdf") > -1 &&
        fileExt == "pdf" &&
        filemb <= this.registartionService.MAX_FILE_SIZE
      ) {
        this.fileUploadForm = new FormData();
        this.fileUploadForm.append("formData", event.target.files[0]);
      } else {
        this.registrationForm.controls.formPath.setValue(null);
        this.isExceeded = true;
      }
    }
  }

  submitRegistration() {
    this.isFormSubmitted = true;
    if (this.registrationForm.valid && this.validUsername) {
      this.registrationSubmited = true;
      this.spinner.show();

      this.fileUploadForm.append(
        "email",
        this.registrationForm.controls.email.value
      );
      this.fileUploadForm.append(
        "type",
        this.registrationForm.controls.type.value
      );
      this.fileUploadForm.append(
        "username",
        this.registrationForm.controls.username.value
      );
      this.fileUploadForm.append(
        "comments",
        this.registrationForm.controls.comments.value
      );

      this.registartionService
        .submitRegistrationRequest(this.fileUploadForm)
        .subscribe(data => {
          this.spinner.hide();
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            let successMessage =
              "Your request is successfully processed. Please use <b>" +
              data["data"] +
              "</b> reference id for future references.";

            Swal.fire({
              title: "Transaction #" + data["data"],
              type: "info",
              html: successMessage,
              showCloseButton: true
            });
            this.resetForm();
          } else {
            this.commonService.showErrorAlertMessage("Something went wrong!");
          }
        });
    }
  }

  resetForm() {
    this.registrationForm.controls.email.setValue("");
    this.registrationForm.controls.type.setValue("");
    this.registrationForm.controls.username.setValue("");
    this.registrationForm.controls.comments.setValue("");
    this.registrationForm.controls.formPath.setValue(null);

    this.registrationForm.controls.email.disable();
    this.registrationForm.controls.username.disable();
    this.registrationForm.controls.formPath.disable();
    this.registrationForm.controls.comments.disable();

    this.isFormSubmitted = false;
    this.registrationSubmited = false;
    this.formEnabled = false;
    this.selectedRequestType = {};
    this.isUniqueUsername = true;
    this.usernameNotFound = false;
    this.uniqueUsernameMessage = "";
  }
}
