import { Injectable } from "@angular/core";
import { NetworkService } from "../common/network.service";
import { CommonService } from "../common/common.service";

@Injectable()
export class RegistrationService {
  NO_HEADER = null;
  MAX_FILE_SIZE = 5; //unit in MB
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {}

  submitRegistrationRequest(data) {
    return this._networkService.uploadFile("register", data, this.NO_HEADER);
  }

  getRequestList(value) {
    return this._networkService.get("register/status/" + value, this.NO_HEADER);
  }

  validateUsernameRequest(value, type) {
    return this._networkService.get(
      "register/validate/username/" + value + "?type=" + type,
      this.NO_HEADER
    );
  }

  getRequestFormsList() {
    return this._networkService.get("register/form/list", this.NO_HEADER);
  }

  getRequestTypeList() {
    return this._networkService.get("register/type/list", this.NO_HEADER);
  }

  downloadFile(id) {
    return this._networkService.getFile("register/form/" + id);
  }

  downloadRawFormFile(type) {
    return this._networkService.getFile("register/form/download/" + type);
  }
}
