import { RegistrationComponent } from "./registration.component";
import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { RegisterComponent } from "./register/register.component";
import { StatusComponent } from "./status/status.component";
import {RegHomeComponent} from './reg-home/reg-home.component';

export const routes: Routes = [
  {
    path: "registration",
    component: RegistrationComponent,
    children: [
      { path: "", redirectTo: "/registration/reg-home", pathMatch: "full" },
      {
        path: "reg-home",
        component: RegHomeComponent
      },
      {
        path: "register",
        component: RegisterComponent
      },
      {
        path: "status",
        component: StatusComponent
      }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
