import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER  } from "@angular/core";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { AuthModule } from "./auth/auth.module";
import { MainModule } from "./main/main.module";
import { RegistrationModule } from "./registration/registration.module";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuardService, ModuleGuardService } from "./guards/auth.guard";
import { httpInterceptorProviders } from "./common/interceptors/interceptors";
import { CommonService } from "./common/common.service";
import { NetworkService } from "./common/network.service";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ConfirmationComponent } from "./common/components/confirmation/confirmation.component";
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  RxReactiveFormsModule } from "@rxweb/reactive-form-validators";
import { AppInitService } from './app-init.service';

export function initializeApp1(appInitService: AppInitService) {
  return (): Promise<any> => { 
    return appInitService.Init();
  }
}

const routes: Routes = [
  { path: "", redirectTo: "auth/login", pathMatch: "full" }
];

@NgModule({
  declarations: [AppComponent, ConfirmationComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AuthModule,
    MainModule,
    RegistrationModule,
    HttpClientModule,
    NgbModule,
    FormsModule,  
    ReactiveFormsModule , RxReactiveFormsModule,                     // <========== Add this line!
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  providers: [
    AppInitService,
    { provide: APP_INITIALIZER,useFactory: initializeApp1, deps: [AppInitService], multi: true},
    CommonService,
    NetworkService,
    AuthGuardService,
    ModuleGuardService,
    httpInterceptorProviders
  ],
  entryComponents: [ConfirmationComponent],
  bootstrap: [AppComponent]
})
export class AppModule {}
