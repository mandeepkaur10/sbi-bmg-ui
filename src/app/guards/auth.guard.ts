import { CanActivate, Router } from "@angular/router";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router/src/router_state";
import { Injectable } from "@angular/core";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    
    if (sessionStorage.getItem("_bearerTkn")) {
      // logged in so return true
      return true;
    }
    // not logged in so redirect to login page with the return url
    // { queryParams: { returnUrl: state.url } }
    this.router.navigate(["/auth/login"]);
    return false;
  }
}

@Injectable()
export class ModuleGuardService implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    
    if (sessionStorage.getItem("_userdata")) {
      let moduleInfo = route.data;
      let permissionList = JSON.parse(
        JSON.parse(sessionStorage.getItem("_userdata")).permissionJson
      );
      let permission = permissionList[moduleInfo["moduleName"]];
      let isAllowed = false;

      for (let y = 0; y < moduleInfo["permissions"].length; y++) {
        if (permission == moduleInfo["permissions"][y]) {
          isAllowed = true;
          break;
        }
      }

      if (isAllowed) {
        //this.router.navigate(["main/unauthorised"]);
        return true;
      }
    }
    return false;
  }
}
