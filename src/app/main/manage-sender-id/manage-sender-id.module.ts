import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ManageSenderIdComponent} from './manage-sender-id.component';
import {ListSenderIdComponent} from './list-sender-id/list-sender-id.component';
import {CreateSenderIdComponent} from './create-sender-id/create-sender-id.component';
import {PendingSenderIdComponent} from './pending-sender-id/pending-sender-id.component';
import {UploadBulkSenderIdComponent} from './upload-bulk-sender-id/upload-bulk-sender-id.component';
import { routing } from './manage-sender-id.routing';
import { SharedModule } from "../../common/shared.modules";
import { SenderIdService } from './manage-sender-id.service';
// import { SearchFilterPipe } from '../../common/filters/search.pipe';


@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule
  ],
  declarations: [ManageSenderIdComponent, ListSenderIdComponent, CreateSenderIdComponent,
     PendingSenderIdComponent, UploadBulkSenderIdComponent],
  providers: [SenderIdService]
})
export class ManageSenderIdModule { }
