import { Component, OnInit } from '@angular/core';
import { SenderIdService } from '../manage-sender-id.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";

@Component({
  selector: 'app-create-sender-id',
  templateUrl: './create-sender-id.component.html',
  styleUrls: ['./create-sender-id.component.scss']
})
export class CreateSenderIdComponent implements OnInit {

  users = [];
  senderIdForm: FormGroup;
  formSubmitted: boolean = false;
  isEdit: boolean = false;

  constructor(private senderIdService: SenderIdService, private fb: FormBuilder, private spinner: NgxSpinnerService, private commonService: CommonService) { }

  ngOnInit() {
    this.getAllUsers();
    this.initialiseForms();
  }

  initialiseForms() {
    this.senderIdForm = this.fb.group({
      user: ['', [Validators.required]],
      transactionId: ['', [Validators.required]],
      senderId: ['', [Validators.required, Validators.pattern(/^([a-zA-Z0-9]{6},){0,9}([a-zA-Z0-9]{6})$/)]]
    });
  }

  getAllUsers() {
    this.spinner.show();
    this.senderIdService.getAllUsers().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.users = res["data"];
        } else {
          this.users = [];
        }
      });
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.senderIdForm.valid) {
      let reqObj = {
        "userId": this.senderIdForm.controls["user"].value,
        "transactionId": this.senderIdForm.controls["transactionId"].value,
        "senderId": this.senderIdForm.controls["senderId"].value,
        "loginId": this.commonService.getUser()
      }
      this.spinner.show();
      this.senderIdService.saveSenderId(reqObj).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast('create');
            this.initialiseForms();
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
          this.formSubmitted = false;         
        });
    }
  }

}
