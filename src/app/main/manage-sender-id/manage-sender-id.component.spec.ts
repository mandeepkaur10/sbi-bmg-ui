import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSenderIdComponent } from './manage-sender-id.component';

describe('ManageSenderIdComponent', () => {
  let component: ManageSenderIdComponent;
  let fixture: ComponentFixture<ManageSenderIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSenderIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSenderIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
