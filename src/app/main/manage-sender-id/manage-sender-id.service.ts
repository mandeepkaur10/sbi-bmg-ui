
import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';

@Injectable()
export class SenderIdService {

  constructor( private _networkService: NetworkService) { }

  getSenderIdList(id:any) {
    return this._networkService.get('senderid/info?epoch='+ id + '&showAll=true', null, 'bearer');
  }

  getAllUsers() {
    return this._networkService.get('user', null, 'bearer');
  }

  getAllAccountTypes() {
    return this._networkService.get('types', null, 'bearer');
  }

  saveSenderId(req: any){
    return this._networkService.post('senderid/info/'+req.userId+'?loginId=' + req.loginId + '&transactionId=' + req.transactionId, {"senderid": req.senderId}, null, 'bearer');
  }

  getPendingSenderIdList(id: any){
    return this._networkService.get('senderid/info/'+ id +'/pending', null, 'bearer');
  }

  deleteSenderId(id: any){
    return this._networkService.delete('senderid/'+ id +'?loginId=12', null, 'bearer');
  }

  updateSattus(req: any){
    return this._networkService.put('senderid/approve/'+ req["senderid"] +'?approvedBy='+ req["approvedBy"] +'&status=' + req["status"], null, null, 'bearer');
  }


//   addContent(req:any) {
//     return this._networkService.post('fgcContent/add', req, null, 'bearer');
//   }

//   editContent(req:any) {
//     return this._networkService.post('fgcContent/update', req, null, 'bearer');
//   }

//   deleteContent(appID:any) {
//     return this._networkService.post('fgcContent/delete', { id: appID }, null, 'bearer');
//   }

//   // uploadImage(req:any) {
//   //   return this._networkService.uploadImages('upload/images', req, null, 'bearer');
//   // }

//   uploadImages(req:any){
//     return this._networkService.uploadImages('multi-upload/images', req, null, 'bearer');
//   }

}

