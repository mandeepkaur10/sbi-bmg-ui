import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSenderIdComponent } from './list-sender-id.component';

describe('ListSenderIdComponent', () => {
  let component: ListSenderIdComponent;
  let fixture: ComponentFixture<ListSenderIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSenderIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSenderIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
