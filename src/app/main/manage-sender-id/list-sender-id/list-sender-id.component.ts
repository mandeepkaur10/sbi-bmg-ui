import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { SenderIdService } from '../manage-sender-id.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE} from "../../../common/common.const";
import { MODULE } from "../../../common/common.const";

@Component({
  selector: 'app-list-sender-id',
  templateUrl: './list-sender-id.component.html',
  styleUrls: ['./list-sender-id.component.scss']
})
export class ListSenderIdComponent implements OnInit {

  MODULES = MODULE;
  pageLimit: number;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  p: number = 1;
  senderIdList = [];
  cliSorted: boolean = false;
  departmentUserSorted: boolean = false;
  statusSorted: boolean = false;

  constructor(private modalService: NgbModal, private senderIdService: SenderIdService,
    private spinner: NgxSpinnerService, private excelService: ExcelService, public commonService: CommonService) {
  }

  ngOnInit() {
    this.getSenderIdList();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  getSenderIdList() {
    let id = 0;
    this.spinner.show();
    this.senderIdService.getSenderIdList(id).subscribe(
    res => {    
      debugger
      this.spinner.hide();  
        if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
          this.senderIdList = res["data"];          
        }else{
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.senderIdList = [];
        }        
      });
  }

  confirmDeleteRecord(record: any) {
    
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to delete this record?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.deleteRecord(record);
        }
      },
      (reason: any) => {

      });
  }

  deleteRecord(record: any) {
    this.spinner.show();
    this.senderIdService.deleteSenderId(record['id']).subscribe(
      res => {
        this.spinner.hide();
        if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
          this.commonService.showSuccessToast("delete");
          this.getSenderIdList();
        }else{
          this.commonService.showErrorToast(res['result']['userMsg']);
        }       
      });
  }
 
  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {  
    this.senderIdList = this.commonService.sortArray(isAssending, sortBy, this.senderIdList, isNumber );    
  }

  changeStatus(event:  any, record: any){
    let req = {
      "senderid": record['id'],
      "approvedBy": record['approvedBy'],
      "status": event ? 1 : 2
    };
    this.spinner.show();
    this.senderIdService.updateSattus(req).subscribe(
    res => {
      this.spinner.hide();           
        if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
          this.commonService.showSuccessToast("update");
          this.getSenderIdList();     
        }else{
          this.commonService.showErrorToast(res['result']['userMsg']);
        }        
      });
  }

  exportAs(fileType: string){
    let data = [];   
    this.senderIdList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Sender ID"] = element.senderid;
      excelDataObject["Name"] = element.username;
      excelDataObject["Department"] = element.department;      
      excelDataObject["Status"] = element.status == 1 ? 'Enable' : 'Disable';
      data.push(excelDataObject);
    });
    switch(fileType){
      case 'excel':
      this.excelService.exportAsExcelFile(data, 'CLI-List');
      break;
      case 'csv':
      this.excelService.exportAsCsvFile(data, 'CLI-List');
      break;
      default:
      this.excelService.exportAsExcelFile(data, 'CLI-List');
      break;
    }
  }

}
