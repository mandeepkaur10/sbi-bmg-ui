import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadBulkSenderIdComponent } from './upload-bulk-sender-id.component';

describe('UploadBulkSenderIdComponent', () => {
  let component: UploadBulkSenderIdComponent;
  let fixture: ComponentFixture<UploadBulkSenderIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadBulkSenderIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadBulkSenderIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
