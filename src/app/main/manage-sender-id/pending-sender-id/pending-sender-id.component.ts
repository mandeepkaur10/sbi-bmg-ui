import { Component, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { SenderIdService } from '../manage-sender-id.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-pending-sender-id',
  templateUrl: './pending-sender-id.component.html',
  styleUrls: ['./pending-sender-id.component.scss']
})
export class PendingSenderIdComponent implements OnInit {

 
  rows = [];
  temp = [];
  columns = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  table: DatatableComponent;
  private modalRef: NgbModalRef;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";

  constructor(private modalService: NgbModal, private senderIdService: SenderIdService,
    private spinner: NgxSpinnerService) {
  }

  ngOnInit() {
    this.getSenderIdList();
  }

  getSenderIdList() {
    let id = 1000;
    this.spinner.show();
    this.senderIdService.getPendingSenderIdList(id).subscribe(
    res => {
        // debugger
        this.spinner.hide();
        if(res['result']['statusCode'] == 200){
          this.rows = res["data"];
        }
        // this.spinner.hide();
        // if (data["statusCode"] == 150) {
        //   this.duplicateMessage = data["message"];
        //   this.duplicatemodalRef = this.modalService.open(duplicate, this.modelOptions);
        //   this.duplicatemodalRef.result.then((result) => {
        //     this.closeResult = `Closed with: ${result}`;
        //   }, (reason) => {
        //     this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        //   });
        // } else {
        //   this.formSubmitted = false;
        //   this.modalRef.close();
        //   this.getContentList();
        // }
      });
  }

  confirmDeleteRecord(record: any) {
    
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to delete this record?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.deleteRecord(record);
        }
      },
      (reason: any) => {

      });
  }

  deleteRecord(record: any) {

  }

  editRecord(record: any) {

  }

  applySearch(){
    let self = this;
    this.rows = this.rows.filter(function (item) {
      return JSON.stringify(item).toLowerCase().includes(self.searchText);
    });
  }

  search(){
    this.getSenderIdList();
  }

  searchTextChanged(event: any){
    if(event == ""){
      this.getSenderIdList();
    }
  }
}
