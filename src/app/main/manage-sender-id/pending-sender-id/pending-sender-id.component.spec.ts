import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingSenderIdComponent } from './pending-sender-id.component';

describe('PendingSenderIdComponent', () => {
  let component: PendingSenderIdComponent;
  let fixture: ComponentFixture<PendingSenderIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingSenderIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingSenderIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
