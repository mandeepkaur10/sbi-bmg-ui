import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageSmsTemplateComponent } from './manage-sms-template.component';

describe('ManageSmsTemplateComponent', () => {
  let component: ManageSmsTemplateComponent;
  let fixture: ComponentFixture<ManageSmsTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageSmsTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageSmsTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
