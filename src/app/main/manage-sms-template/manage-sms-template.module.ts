import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ManageSmsTemplateComponent} from './manage-sms-template.component';
import {ApprovedSmsTemplateComponent} from './approved-sms-template/approved-sms-template.component';
import {TemplatePendingComponent} from './template-pending/template-pending.component';
import {BulkUploadTemplateComponent} from './bulk-upload-template/bulk-upload-template.component';
import { routing } from './manage-sms-template.routing';
import { ManageSmsTemplateService } from "./manage-sms-template.service";
import { SharedModule } from '../../common/shared.modules';
import { CreateSmsTemplateComponent } from './create-sms-template/create-sms-template.component';


@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule
  ],
  declarations: [ManageSmsTemplateComponent, ApprovedSmsTemplateComponent, TemplatePendingComponent, BulkUploadTemplateComponent,
     CreateSmsTemplateComponent],
  providers: [ManageSmsTemplateService]
})
export class ManageSmsTemplateModule { }



