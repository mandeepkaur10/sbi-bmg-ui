import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BulkUploadTemplateComponent } from './bulk-upload-template.component';

describe('BulkUploadTemplateComponent', () => {
  let component: BulkUploadTemplateComponent;
  let fixture: ComponentFixture<BulkUploadTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BulkUploadTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BulkUploadTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
