import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovedSmsTemplateComponent } from './approved-sms-template.component';

describe('ApprovedSmsTemplateComponent', () => {
  let component: ApprovedSmsTemplateComponent;
  let fixture: ComponentFixture<ApprovedSmsTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovedSmsTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovedSmsTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
