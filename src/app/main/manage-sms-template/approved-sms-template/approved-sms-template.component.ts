import { Component, OnInit } from '@angular/core';
import { ManageSmsTemplateService } from "../manage-sms-template.service";
import { NgxSpinnerService } from "ngx-spinner";
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { CommonService } from "../../../common/common.service";
import { RESPONSE, MODULE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";

@Component({
  selector: 'app-approved-sms-template',
  templateUrl: './approved-sms-template.component.html',
  styleUrls: ['./approved-sms-template.component.scss']
})

export class ApprovedSmsTemplateComponent implements OnInit {

  MODULES = MODULE;
  templateList = [];
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  pageLimit: number;
  searchText: string = "";
  p: number = 1;
  templateNameSorted: boolean = false;
  contentTypeSorted: boolean = false;
  assignedToSorted: boolean = false;
  createdBySorted: boolean = false;
  approvedBySorted: boolean = false;
  statusSorted: boolean = false;

  constructor(private manageSmsTemplateService: ManageSmsTemplateService, private spinner: NgxSpinnerService,
    private modalService: NgbModal, public commonService: CommonService, private excelService: ExcelService) { }

  ngOnInit() {
    this.getTemplateList();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  getTemplateList() {
    let req = {
      "id": 0,
      "userId": this.commonService.getUser(),
      "channelId": this.commonService.getChannelId()
    }
    this.spinner.show();
    this.manageSmsTemplateService.getTemplateList(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.templateList = res["data"];
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.templateList = [];
        }
      });
  }

  confirmDeleteRecord(record: any) {

    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to delete this record?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.componentInstance.remarks = true;
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.deleteRecord(record);
        }
      },
      (reason: any) => {

      });
  }

  deleteRecord(record: any) {
    let req = {
      "templateId": record['id'],
      "remarks": "delete template",
      "loginId": this.commonService.getUser()
    };
    this.spinner.show();
    this.manageSmsTemplateService.deleteTemplate(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.commonService.showSuccessToast("delete");
          this.getTemplateList();
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  changeStatus(event: any, record: any) {
    let req = {
      "id": record['id'],
      "approvedBy": record['approvedBy'],
      "status": event ? 1 : 2
    };
    this.spinner.show();
    this.manageSmsTemplateService.updateSattus(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.commonService.showSuccessToast("update");
          this.getTemplateList();
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {  
    this.templateList = this.commonService.sortArray(isAssending, sortBy, this.templateList, isNumber );    
  }

  exportAs(fileType: string){
    let data = [];   
    this.templateList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Template Name"] = element.templateName;
      excelDataObject["Template Content Type"] = element.templateContentType;
      excelDataObject["Template  Text"] = element.template;  
      excelDataObject["Assigned To"] = element.username;  
      excelDataObject["Created By"] = element.createdBy;  
      excelDataObject["Approved By"] = element.approvedBy;      
      excelDataObject["Status"] = element.status == 1 ? 'Enable' : 'Disable';
      data.push(excelDataObject);
    });
    switch(fileType){
      case 'excel':
      this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
      break;
      case 'csv':
      this.excelService.exportAsCsvFile(data, 'SMS-Template-List');
      break;
      default:
      this.excelService.exportAsExcelFile(data, 'SMS-Template-List');
      break;
    }
  }

}
