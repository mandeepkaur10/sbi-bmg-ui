import { Component, OnInit } from '@angular/core';
import { ManageSmsTemplateService } from '../manage-sms-template.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";

@Component({
  selector: 'app-create-sms-template',
  templateUrl: './create-sms-template.component.html',
  styleUrls: ['./create-sms-template.component.scss']
})
export class CreateSmsTemplateComponent implements OnInit {

  users = [];
  smsTemplateForm: FormGroup;
  formSubmitted: boolean = false;
  isEdit: boolean = false;
  math = Math;
  placeholder: boolean = false;
  departments = [];

  constructor(private smsTemplateService: ManageSmsTemplateService, private fb: FormBuilder,
     private spinner: NgxSpinnerService, private commonService: CommonService) { }

  ngOnInit() {   
    this.getUserList();
    this.initialiseForms();
  }

  getUserList() {
    this.spinner.show();
    this.smsTemplateService.getAllUser().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.users = res['data'];
        }else{
          this.users = [];
        }
      });
  }

  initialiseForms() {
    this.smsTemplateForm = this.fb.group({
      contentType: ['', [Validators.required]],
      name: ['', [Validators.required]],
      language: [''],
      maskFlag: ['', [Validators.required]],
      maskText: [''],
      content: ['', [Validators.required]],
      user: ['', [Validators.required]],
      requestId: ['', [Validators.required]],
      placeHolder: ['']
    });
  }

  maskFlagChanged(event: any){
    if(event == 'Y'){
      this.smsTemplateForm.controls['maskText'].setValidators(Validators.required);
      this.smsTemplateForm.controls['maskText'].updateValueAndValidity();     
    }else{
      this.smsTemplateForm.controls['maskText'].clearValidators();
      this.smsTemplateForm.controls['maskText'].updateValueAndValidity();  
    }
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.smsTemplateForm.valid) {
      let reqObj = {
        "userId": this.smsTemplateForm.controls["user"].value,
        "transactionId": this.smsTemplateForm.controls["requestId"].value,
        "loginId": this.commonService.getUser(),
        "reqObj": {
          "language": this.smsTemplateForm.controls["contentType"].value,
          "maskFlag": this.smsTemplateForm.controls["maskFlag"].value,
          "maskText": this.smsTemplateForm.controls["maskText"].value,
          "template": this.smsTemplateForm.controls["content"].value,
          "templateName": this.smsTemplateForm.controls["name"].value
        }
      }

      this.spinner.show();
      this.smsTemplateService.addTemplate(reqObj).subscribe(
        res => {              
          this.spinner.hide();
          if (res['result']['statusCode'] == 200) {
            this.commonService.showSuccessToast('create'); 
            this.initialiseForms();         
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);       
          }
          this.formSubmitted = false;         
        });
    }
  }

  addPlaceholder() {
    this.placeholder = true;
  }

  appendPlaceholder() {
    let contentValue = this.smsTemplateForm.controls['content'].value;
    this.smsTemplateForm.controls['content'].setValue(contentValue + " <!" + this.smsTemplateForm.controls['placeHolder'].value + "!> ");
    this.smsTemplateForm.controls['placeHolder'].setValue("");
    this.placeholder = false;
  }

}
