import { Component, OnInit } from '@angular/core';
import { ManageSmsTemplateService } from "../manage-sms-template.service";
import { NgxSpinnerService } from "ngx-spinner";
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";

@Component({
  selector: 'app-template-pending',
  templateUrl: './template-pending.component.html',
  styleUrls: ['./template-pending.component.scss']
})
export class TemplatePendingComponent implements OnInit {

  pendingTemplateList = [];
  // private modalRef: NgbModalRef;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  pageLimit: number = 10;
  searchText: string = "";
  p: number = 1;

  constructor(private manageSmsTemplateService: ManageSmsTemplateService, private spinner: NgxSpinnerService,
    private modalService: NgbModal) { }

    templateList = [];

    ngOnInit() {
      this.getPendingTemplateList();
    }
  
    getPendingTemplateList(){
      let id = 7;
      this.spinner.show();
      this.manageSmsTemplateService.getPendingTemplateList(id).subscribe(
        res => {
          this.spinner.hide();
          if(res['result']['statusCode'] == 200){
            this.pendingTemplateList = res["data"];
          }else{
            this.pendingTemplateList = [];
          }  
        });
    }
  
    confirmDeleteRecord(record: any) {
      
      let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
      modalRef.componentInstance.content = 'Are you sure you want to delete this record?';
      modalRef.componentInstance.leftButton = 'No';
      modalRef.componentInstance.rightButton = 'Yes';
      modalRef.result.then(
        (data: any) => {
          if (data == "yes") {
            this.deleteRecord(record);
          }
        },
        (reason: any) => {
  
        });
    }
  
    deleteRecord(record: any) {
      debugger
      this.spinner.show();
      this.manageSmsTemplateService.deleteTemplate(record['id']).subscribe(
        res => {
          this.spinner.hide();
          this.getPendingTemplateList();
        });
    }
  
    changeStatus(record: any){
  
    }
  
    applySearch(){
      let self = this;
      this.pendingTemplateList = this.pendingTemplateList.filter(function (item) {
        return JSON.stringify(item).toLowerCase().includes(self.searchText);
      });
    }
  
    search(){
      this.getPendingTemplateList();
    }
  
    searchTextChanged(event: any){
      if(event == ""){
        this.getPendingTemplateList();
      }
    }

}
