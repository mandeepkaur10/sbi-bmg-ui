import { Component, OnInit } from '@angular/core';
import { MODULE } from "../../common/common.const";
import { CommonService } from "../../common/common.service";

@Component({
  selector: 'app-manage-sms-template',
  templateUrl: './manage-sms-template.component.html',
  styleUrls: ['./manage-sms-template.component.scss']
})
export class ManageSmsTemplateComponent implements OnInit {

  MODULES = MODULE;
  constructor( public commonService: CommonService ) { }

  ngOnInit() {
  }

}
