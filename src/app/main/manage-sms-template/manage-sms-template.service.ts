import { Injectable } from '@angular/core';
import { NetworkService } from "../../common/network.service";

@Injectable()
export class ManageSmsTemplateService {

    constructor(private _networkService: NetworkService) { }

    getTemplateList(req: any) {
        if(req['channelId']  == 2){
            return this._networkService.get('campaign/' + req['userId']+ '/template', null, 'bearer');
        }else{
            return this._networkService.get('template/info?epoch=' + req['id'] + '&showAll=true', null, 'bearer');
        }
        
    }

    deleteTemplate(req: any) {
        return this._networkService.delete('template/' + req['templateId'] + '?loginId=' + req['loginId'] + '&remarks=' + req['remarks'] , null, 'bearer');
    }

    getPendingTemplateList(id: any){
        return this._networkService.get('template/info/'+ id +'/pending', null, 'bearer');
    }

    updateSattus(req: any){
        return this._networkService.put('template/approve/'+ req["id"] +'?approvedBy='+ req["approvedBy"] +'&status=' + req["status"], null, null, 'bearer');
    }

    getAllUser(){
        return this._networkService.get('user', null, 'bearer');
    }

    getLanguage(){
        return this._networkService.get('master/language', null, 'bearer');
    }

    addTemplate( req: any ){
        return this._networkService.post('template/info/'+ req['userId'] +'?loginId='+ req['loginId']+'&transactionId=' + req['transactionId'],
         req['reqObj'], null, 'bearer');
    }
}