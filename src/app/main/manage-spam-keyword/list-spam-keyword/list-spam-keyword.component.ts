import { Component, OnInit } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
// import { SenderIdService } from '../manage-sender-id.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../../../common/common.service';
import { ManageSpamKeywordService } from '../manage-spam-keyword.service';
import { RESPONSE } from '../../../common/common.const';

@Component({
  selector: 'app-list-spam-keyword',
  templateUrl: './list-spam-keyword.component.html',
  styleUrls: ['./list-spam-keyword.component.scss']
})
export class ListSpamKeywordComponent implements OnInit {

  spamKeywordsList = [];
  pageLimit: number;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  spamKeywordList = [];
  p: number = 1;

  spamKeywordForm: FormGroup;
  isEdit: boolean = false;
  formSubmitted: boolean = false;

  constructor(private modalService: NgbModal,
    private spinner: NgxSpinnerService, private fb: FormBuilder, public commonService: CommonService, private spamService: ManageSpamKeywordService) { }

  initializeForm() {
    this.spamKeywordForm = this.fb.group({
      keywords: ['', [Validators.required]],
      level: ['', [Validators.required]],
      status: ['', [Validators.required]]
    });
  }

  get f() { return this.spamKeywordForm.controls; }

  ngOnInit() {
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.getSpamKeywordList();
  }

  getSpamKeywordList() {
    this.spinner.show();
    this.spamService.getSpamKeywords().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.spamKeywordsList = res["data"];
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.spamKeywordsList = [];
        }
      });
  }

  confirmDeleteRecord(record: any) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to delete this record?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.deleteRecord(record);
        }
      },
      (reason: any) => {
      });
  }

  deleteRecord(record: any) {
    let req = {
      "keywordId": record["id"],
      "loginId": this.commonService.getUser(),
      "remarks": "Delete Spam Keywords"
    };
    this.spinner.show();
    this.spamService.deleteSpamKeywords(req).subscribe(
      res => {
        this.spinner.hide();
        this.getSpamKeywordList();
      });
  }

  updateRecord(updatemodal: any, record: any) {
    this.formSubmitted = false;
    this.initializeForm();
    debugger
    this.spamKeywordForm.controls['keywords'].setValue(record['keywords']);
    this.spamKeywordForm.controls['level'].setValue(record['level']);
    this.spamKeywordForm.controls['status'].setValue(record['status']);
    let modalRef = this.modalService.open(updatemodal, this.modelOptions);
    modalRef.result.then((result) => {
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  saveCLI() {
    this.formSubmitted = true;
    if (this.spamKeywordForm.valid) {
    }
  }
  changeStatus(record: any) {
    if(record['status']=="D"){
      record['status']="E";
    } else{
      record['status']="D";
    }
  }
}

  // editRecord(record: any) {

  // }

  // applySearch() {
  //   let self = this;
  //   this.spamKeywordList = this.spamKeywordList.filter(function (item) {
  //     return JSON.stringify(item).toLowerCase().includes(self.searchText);
  //   });
  // }

  // search() {
  //   this.getSpamKeywordList();
  // }

  // searchTextChanged(event: any) {
  //   if (event == "") {
  //     this.getSpamKeywordList();
  //   }
  // }