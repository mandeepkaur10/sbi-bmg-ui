import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListSpamKeywordComponent } from './list-spam-keyword.component';

describe('ListSpamKeywordComponent', () => {
  let component: ListSpamKeywordComponent;
  let fixture: ComponentFixture<ListSpamKeywordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListSpamKeywordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSpamKeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
