import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageSpamKeywordComponent } from "./manage-spam-keyword.component";
import { ListSpamKeywordComponent } from "./list-spam-keyword/list-spam-keyword.component";
import { CreateSpamKeywordComponent } from "./create-spam-keyword/create-spam-keyword.component";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageSpamKeywordComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-spam-keyword/list-spam-keyword",
        pathMatch: "full"
      },
      {
        path: "list-spam-keyword",
        component: ListSpamKeywordComponent,
        data: { moduleName: MODULE.SPAM_KEYWORD, permissions: ["R", "RW"] }
      },
      {
        path: "create-spam-keyword",
        component: CreateSpamKeywordComponent,
        data: { moduleName: MODULE.SPAM_KEYWORD, permissions: ["RW"] }
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
