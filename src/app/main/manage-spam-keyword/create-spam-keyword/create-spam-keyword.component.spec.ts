import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSpamKeywordComponent } from './create-spam-keyword.component';

describe('CreateSpamKeywordComponent', () => {
  let component: CreateSpamKeywordComponent;
  let fixture: ComponentFixture<CreateSpamKeywordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateSpamKeywordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSpamKeywordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
