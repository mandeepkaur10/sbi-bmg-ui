import { Component, OnInit } from '@angular/core';
// import { SenderIdService } from '../manage-sender-id.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from '../../../common/common.service';
import { ManageSpamKeywordService } from '../manage-spam-keyword.service';
import { RESPONSE } from "../../../common/common.const";
import { Router } from "@angular/router";

@Component({
  selector: 'app-create-spam-keyword',
  templateUrl: './create-spam-keyword.component.html',
  styleUrls: ['./create-spam-keyword.component.scss']
})
export class CreateSpamKeywordComponent implements OnInit {
  spamKeywordForm: FormGroup;
  formSubmitted: boolean = false;
  isEdit: boolean = false;
  private modalRef: NgbModalRef;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };

  constructor(private fb: FormBuilder, private router: Router,
    private modalService: NgbModal, private spinner: NgxSpinnerService,
    private spamService: ManageSpamKeywordService, private commonService: CommonService, ) { }

  ngOnInit() {
    this.initialiseForms();
  }

  initialiseForms() {
    this.spamKeywordForm = this.fb.group({
      level: ['', [Validators.required]],
      keyword: ['', [Validators.required, Validators.pattern(this.commonService.patterns.tenCommaSeparatedKeywords)]]
    });
  }
  submitForm() {
    this.formSubmitted = true;
    if (this.spamKeywordForm.valid) {
      let reqObj = {
        "loginId": this.commonService.getUser(),
        "loginUsername": this.commonService.getUserName(),
        "keyword": this.spamKeywordForm.controls.keyword.value,
        "level": this.spamKeywordForm.controls.level.value
      }
      this.spinner.show();
      this.spamService.createSpamKeywords(reqObj).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast('Spam keywords successfully created');
            this.router.navigate(["/main/manage-spam-keyword/list-spam-keyword"]);
          } else {
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        }
      );
    }
  }
}