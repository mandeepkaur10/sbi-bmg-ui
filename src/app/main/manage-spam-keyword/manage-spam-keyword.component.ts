import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common/common.service";

@Component({
  selector: 'app-manage-spam-keyword',
  templateUrl: './manage-spam-keyword.component.html',
  styleUrls: ['./manage-spam-keyword.component.scss']
})
export class ManageSpamKeywordComponent implements OnInit {

  constructor( public commonService: CommonService ) { }

  ngOnInit() {
  }

}
