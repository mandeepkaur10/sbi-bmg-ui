import { Injectable } from '@angular/core';
import { NetworkService } from "../../common/network.service";

@Injectable()
export class ManageSpamKeywordService {
    constructor(private _networkService: NetworkService) { }
    createSpamKeywords(req: any) {
         return this._networkService.post('spamkeyword?loginId=' + req["loginId"] + '&loginUsername=' + req["loginUsername"]+ '&level=' + req["level"], req, null, 'bearer');
        // return this._networkService.post('spamkeyword', req, null, 'bearer');
    }
    deleteSpamKeywords(req: any) {
        return this._networkService.delete('spamkeyword/' + req['keywordId'] + '?loginId=' + req['loginId'] + '&remarks=' + req['remarks'], null, 'bearer');
    }
    getSpamKeywords() {
        return this._networkService.get('spamkeyword', null, 'bearer');
    }
    updateSpamKeywords() {
        return 1;
    }

}