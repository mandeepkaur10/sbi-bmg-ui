import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";
import { MODULE } from "src/app/common/common.const";

@Injectable()
export class ChangePasswordService {
  NO_HEADER = null;
  userId;
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {
    this.userId = "userId=" + _commonService.getUser();
  }

  updatePassword(data) {
    return this._networkService.put(
      "login/password/reset" + "?" + this.userId,
      data,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }
}
