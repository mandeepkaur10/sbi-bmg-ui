import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { CommonService } from "../../common/common.service";
import { ChangePasswordService } from "./change-password.service";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";
import { validateConfig } from "@angular/router/src/config";

@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"],
  providers: [ChangePasswordService]
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;

  formSubmitted: boolean = false;
  constructor(
    private formBuilder: FormBuilder,
    public commonService: CommonService,
    private changePasswordService: ChangePasswordService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.prepareChangePasswordForm();
  }

  prepareChangePasswordForm() {
    this.changePasswordForm = this.formBuilder.group({
      currentPassword: ["", [Validators.required]],
      newPassword: [
        "",
        Validators.compose([
          Validators.required,
          this.matchCurrentAndNewPassword
        ])
      ],
      confirmNewPassword: [
        "",
        Validators.compose([
          Validators.required,
          this.matchNewAndConfirmPassword
        ])
      ]
    });
  }

  matchCurrentAndNewPassword(control: FormControl) {
    if (control.root && control.root["controls"]) {
      let currentPassword = control.root["controls"][
        "currentPassword"
      ].value.trim();
      let newPassword = control.root["controls"]["newPassword"].value.trim();
      if (
        currentPassword.length > 0 &&
        newPassword.length > 0 &&
        currentPassword == newPassword
      ) {
        return { currentAndNewPasswordMatched: true };
      }
    }
    return null;
  }

  matchNewAndConfirmPassword(control: FormControl) {
    if (control.root && control.root["controls"]) {
      let confirmNewPassword = control.root["controls"][
        "confirmNewPassword"
      ].value.trim();
      let newPassword = control.root["controls"]["newPassword"].value.trim();
      if (
        confirmNewPassword.length > 0 &&
        newPassword.length > 0 &&
        confirmNewPassword != newPassword
      ) {
        return { confirmAndNewPasswordNotMatched: true };
      }
    }
    return null;
  }

  changePassword() {
    this.formSubmitted = true;
    if (this.changePasswordForm.valid) {
      this.spinner.show();
      let obj = {
        newPassword: btoa(this.changePasswordForm.controls.newPassword.value),
        oldPassword: btoa(
          this.changePasswordForm.controls.currentPassword.value
        ),
        username: this.commonService.getUserName()
      };
      this.changePasswordService.updatePassword(obj).subscribe(
        data => {
          this.spinner.hide();
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.resetForm();
            this.commonService.showSuccessToast('create');
          } else {
            this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
          }
        });
    }
  }

  resetForm() {
    this.changePasswordForm.controls.currentPassword.setValue("");
    this.changePasswordForm.controls.newPassword.setValue("");
    this.changePasswordForm.controls.confirmNewPassword.setValue("");
  }
}
