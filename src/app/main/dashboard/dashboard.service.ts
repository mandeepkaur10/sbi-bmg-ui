import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';

@Injectable()
export class DashboardService {
    constructor(private _networkService: NetworkService) { }

    getuserWiseTrafficDetail(req: any) {
        return this._networkService.get('dashboard/userwiseTrafficDetails?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
            '&userId=' + req['userId'], null, 'bearer');
    }
    getGeneralData(req: any) {
        return this._networkService.get('dashboard/generalData?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
            '&userId=' + req['userId'], null, 'bearer');
    }

    getDeptApiTraffic(req: any) {
        return this._networkService.get('dashboard/deptartmentApiTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
            '&userId=' + req['userId'], null, 'bearer');
    }
    getCampaignBulkTraffic(req: any) {
        return this._networkService.get('dashboard/campaignBulkTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
            '&userId=' + req['userId'], null, 'bearer');
    }
    getCampaignBoardData(req: any) {
        return this._networkService.get('dashboard/campaignBoardData?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
            '&userId=' + req['userId'], null, 'bearer');
    }
    getApiTraffic(req: any) {
        return this._networkService.get('dashboard/apiTraffic?endDate=' + req["endDate"] + '&startDate=' + req['startDate'] +
            '&userId=' + req['userId'], null, 'bearer');
    }
}