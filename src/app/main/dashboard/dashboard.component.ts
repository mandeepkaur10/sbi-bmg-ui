import { Component, OnInit } from '@angular/core';
// import { ChartOptions, ChartType, ChartDataSets } from '../../../../node_modules/@types/chart.js';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from '../../../../node_modules/ng2-charts';
import { DashboardService } from "./dashboard.service";
import { NgxSpinnerService } from 'ngx-spinner';
import { RESPONSE } from "../../common/common.const";
import { CommonService } from "../../common/common.service";
import { element } from '../../../../node_modules/@angular/core/src/render3/instructions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private dashboardService: DashboardService, private spinner: NgxSpinnerService,
    public commonService: CommonService) { }

  userWiseTrafficList = [];
  generalDataList = [];
  campaignBoardList = [];
  deptApiTrafficList = [];
  campaignBulkTrafficList: any;
  apiTrafficList: any;
  requestParam: any;
  totalCampTile: number;
  totalSMSCampTile: number;
  successSMSCampTile: number;
  pendingSMSCampTile: number;
  failedSMSCampTile: number;
  totalSMSTile: number;
  successSMSTile: number;
  pendingSMSTile: number;
  failedSMSTile: number;
  public pieChartData: number[];

  ngOnInit() {
    this.requestParam = {
      "userId": this.commonService.getUser(),
      "startDate": "2019-03-23",
      "endDate": [new Date().toISOString().substr(0, 10)]
    };

    this.getUserWiseTrafficDetail(this.requestParam);          //Table Data
    this.getGeneralData(this.requestParam);
    this.getDeptApiTraffic(this.requestParam);
    this.getApiTraffic(this.requestParam);
    this.getCampaignBoardData(this.requestParam);     // want tps for each hour
    this.getCampaignBulkTraffic(this.requestParam);    //why array of object
  }
  //Tabular data
  getUserWiseTrafficDetail(req: any) {
    this.spinner.show();
    this.dashboardService.getuserWiseTrafficDetail(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.userWiseTrafficList = res['data'];
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }
  //total dept, aggrigator, total api, total bulk, users success and fail in %age
  getGeneralData(req: any) {
    this.spinner.show();
    this.dashboardService.getGeneralData(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.generalDataList = res['data'];
          this.pieChartData = [this.generalDataList['campaignFailurePercent'], this.generalDataList['campaignSuccessPercent']];
        } else {
          this.generalDataList = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      },
      err => {
        this.generalDataList = [];
      });
  }
  // PIE CHART
  public pieChartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Success', 'Fail'];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: ['#66bb6a', '#e57373'],
    },
  ];
  //campaign Dynamic rows
  getCampaignBoardData(req: any) {
    this.spinner.show();
    this.dashboardService.getCampaignBoardData(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          res['data'].forEach(element => {
            let tileObj = {
              "campaignId": 0,
              "campaignName": '',
              "status": '',
              "departmentName": '',
              "departmentUser": '',
              "senderId": '',
              "startTime": '',
              "endTime": '',
              "totalSmsCount": 0,
              "dndCount": 0,
              "smsSent": 0,
              "smsSuccess": 0,
              "smsFailed": 0,
              "smsPending": 0,
              "currentTps": 0,
              "maxTps": 0,
              "hours": [],
              "date": [],
              "lineChart": []
            };
            tileObj['campaignId'] = element['campaignId'];
            tileObj['campaignName'] = element['campaignName'];
            tileObj['status'] = element['status'];
            tileObj['departmentName'] = element['departmentName'];
            tileObj['departmentUser'] = element['departmentUser'];
            tileObj['senderId'] = element['senderId'];
            tileObj['startTime'] = element['startTime'];
            tileObj['endTime'] = element['endTime'];
            tileObj['totalSmsCount'] = element['totalSmsCount'];
            tileObj['dndCount'] = element['dndCount'];
            tileObj['smsSent'] = element['smsSent'];
            tileObj['smsSuccess'] = element['smsSuccess'];
            tileObj['smsFailed'] = element['smsFailed'];
            tileObj['smsPending'] = element['smsPending'];
            tileObj['currentTps'] = element['currentTps'];
            tileObj['maxTps'] = element['maxTps'];
            tileObj['date'] = element.campaignWiseTpsGraph.map(a => a.date);
            tileObj['hours'] = element.campaignWiseTpsGraph.map(a => a.hour);
            tileObj['hours'] = [].concat.apply([], tileObj['hours']);
            tileObj['hours'] = Array.from(new Set(tileObj['hours']));
            tileObj['hours'].sort(function (a, b) { return a - b });

            let dataArrayLine = element.campaignWiseTpsGraph.map(a => a.tps);
            for (let i = 0; i < dataArrayLine.length; i++) {
              let obj = {};
              obj['data'] = dataArrayLine[i];
              obj['label'] = tileObj['campaignName'];
              obj['fill'] = false
              tileObj['lineChart'].push(obj);
            }
            this.campaignBoardList.push(tileObj);
            console.log(this.campaignBoardList);
            debugger
          })
        } else {
          this.campaignBoardList = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      },
      err => {
        this.campaignBoardList = [];
      });
  }
  // public campaignWiseData: ChartDataSets[] = [
  //   { data: [20, 59, 80, 30, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 25, 40, 100, 39, 20], label: 'User 1', fill: false }
  // ];
  // public campaignWiseLabels: Label[] = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'];
  public campaignWiseOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    spanGaps: true,
    scales: {
      xAxes: [{
        scaleLabel: { display: true, labelString: 'Hours', fontSize: 16, fontColor: '#8f3e92' },
        ticks: {
          autoSkip: false,
        }
      }],
      yAxes: [
        {
          scaleLabel: { display: true, labelString: 'TPS', fontSize: 16, fontColor: 'rgba(0,191,165,1)' },
          ticks: { min: 0, beginAtZero: true, fontSize: 13 }
        },
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public campaignWiseColors: Array<any> = [
    {
      backgroundColor: '#f9bd63',
      borderColor: '#ffa726',
      pointBackgroundColor: '#ffa726',
      pointBorderColor: '#ffa726',
      pointHoverBackgroundColor: '#ffa726',
      pointHoverBorderColor: '#ffa726'
    }
  ];
  public campaignWiseLegend = true;
  public campaignWiseType = 'line';
  //User total SMS, Success SMS,fail bar graph and userwise TPS
  getDeptApiTraffic(req: any) {
    this.spinner.show();
    this.dashboardService.getDeptApiTraffic(req).subscribe(
      res => {
        this.spinner.hide();
        console.log("API TRAFIC");
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          res['data'].forEach(element => {
            let tileObj = {
              "lineChart": [],
              "barChart": [],
              "users": [],
              "hours": [],
              "deptID": '',
              "deptName": ''
            };
            tileObj['deptID'] = element['deptId'];
            tileObj['deptName'] = element['deptName'];
            let dataArrayLine = element.userWiseHourlyTps.map(a => a.tpsInfo.map(b => b.tps));
            let labelArrayLine = element.userWiseHourlyTps.map(a => a.username);
            for (let i = 0; i < dataArrayLine.length; i++) {
              let obj = {};
              obj['data'] = dataArrayLine[i];
              obj['label'] = labelArrayLine[i];
              tileObj['lineChart'].push(obj);
            }
            let dataArrayBarTotal = element.userWiseSmsStatus.map(a => a.totalSmsSent);
            let obj = {};
            obj['data'] = dataArrayBarTotal;
            obj['label'] = "Total";
            tileObj['barChart'].push(obj);
            let dataArrayBarSuccess = element.userWiseSmsStatus.map(a => a.success);
            obj = {};
            obj['data'] = dataArrayBarSuccess;
            obj['label'] = "Success";
            tileObj['barChart'].push(obj);
            let dataArrayBarFailed = element.userWiseSmsStatus.map(a => a.fail);
            obj = {};
            obj['data'] = dataArrayBarFailed;
            obj['label'] = "Failed";
            tileObj['barChart'].push(obj);
            let dataArrayBarPending = element.userWiseSmsStatus.map(a => a.pending);
            obj = {};
            obj['data'] = dataArrayBarPending;
            obj['label'] = "Pending";
            tileObj['barChart'].push(obj);
            let lebelArrayBar = element.userWiseSmsStatus.map(a => a.username);
            tileObj['users'] = lebelArrayBar;
            let lebelArrayLine = element.userWiseSmsStatus.map(a => a.username);
            tileObj['hours'] = element.userWiseHourlyTps.map(a => a.tpsInfo.map(b => b.hour));
            tileObj['hours'] = [].concat.apply([], tileObj['hours']);
            tileObj['hours'] = Array.from(new Set(tileObj['hours']));
            tileObj['hours'].sort(function (a, b) { return a - b });
            this.deptApiTrafficList.push(tileObj);
            console.log("tile data", this.deptApiTrafficList);
          });
        } else {
          this.deptApiTrafficList = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      },
      err => {
        this.deptApiTrafficList = [];
      });
  }
  public userwiseSMSOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    scales:
    {
      yAxes:
        [
          {
            scaleLabel: { display: true, labelString: 'Count', fontSize: 16, fontColor: '#66bb6a' },
            position: 'left', id: '1', type: 'linear',
            ticks: { min: 0, beginAtZero: true, fontSize: 13 }
          }
        ],
      xAxes:
        [
          {
            scaleLabel: { display: true, labelString: 'Users', fontSize: 16, fontColor: '#e57373' },
          }
        ]
    },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public userwiseSMSColors: Array<any> = [
    {
      backgroundColor: '#f29fbb',
      borderColor: '#f48fb1',
      pointBackgroundColor: '#f48fb1',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#f48fb1'
    },
    {
      backgroundColor: '#88d88c',
      borderColor: '#66bb6a',
      pointBackgroundColor: '#66bb6a',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#66bb6a'
    },
    {
      backgroundColor: '#f78f8f',
      borderColor: '#e57373',
      pointBackgroundColor: '#e57373',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#e57373'
    },
    {
      backgroundColor: '#fcd864',
      borderColor: '#ffd54f',
      pointBackgroundColor: '#ffd54f',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#ffd54f'
    }
  ];
  public userwiseSMSType: ChartType = 'bar';
  public userwiseSMSLegend = true;

  // LINE GRAPH FOR API TRAFFIC USERWISE
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    maintainAspectRatio: false,
    spanGaps: true,
    scales: {
      xAxes: [{
        scaleLabel: { display: true, labelString: 'Hours', fontSize: 16, fontColor: '#8f3e92' },
        ticks: {
          autoSkip: false,
        }
      }],
      yAxes: [
        {
          scaleLabel: { display: true, labelString: 'TPS', fontSize: 16, fontColor: 'rgba(0,191,165,1)' },
          ticks: { min: 0, beginAtZero: true, fontSize: 13 }
        },
      ]
    },
    annotation: {
      annotations: [
        {
          type: 'line',
          mode: 'vertical',
          scaleID: 'x-axis',
          value: 'March',
          borderColor: 'orange',
          borderWidth: 2,
          label: {
            enabled: true,
            fontColor: 'orange',
            content: 'LineAnno'
          }
        },
      ],
    },
  };
  public lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(171,234,225,0.5)',
      borderColor: '#00B0FB	',
      pointBackgroundColor: '#00B0FB',
      pointBorderColor: '#00B0FB',
      pointHoverBackgroundColor: '#00B0FB',
      pointHoverBorderColor: '#00B0FB'
    },
    {
      backgroundColor: 'rgba(171,234,225,0.5)',
      borderColor: '#FF9C00',
      pointBackgroundColor: '#FF9C00',
      pointBorderColor: '#FF9C00',
      pointHoverBackgroundColor: '#FF9C00',
      pointHoverBorderColor: '#FF9C00'
    },
    {
      backgroundColor: 'rgba(171,234,225,0.5)',
      borderColor: '#009754',
      pointBackgroundColor: '#009754',
      pointBorderColor: '#009754',
      pointHoverBackgroundColor: '#009754',
      pointHoverBorderColor: '#009754'
    }
  ];

  public lineChartLegend = true;
  public lineChartType = 'line';

  getCampaignBulkTraffic(req: any) {
    this.spinner.show();
    this.dashboardService.getCampaignBulkTraffic(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.campaignBulkTrafficList = res['data'];
          this.totalCampTile = this.campaignBulkTrafficList.map(a => a.totalSuccessCampaign);
          this.totalSMSCampTile = this.campaignBulkTrafficList.map(a => a.smsSent);
          this.successSMSCampTile = this.campaignBulkTrafficList.map(a => a.successSms);
          this.pendingSMSCampTile = this.campaignBulkTrafficList.map(a => a.pendingSms);
          this.failedSMSCampTile = this.campaignBulkTrafficList.map(a => a.failedSms);
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  getApiTraffic(req: any) {
    this.spinner.show();
    this.dashboardService.getApiTraffic(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.apiTrafficList = res['data'];
          this.totalSMSTile = this.apiTrafficList.map(a => a.sentSms);
          this.successSMSTile = this.apiTrafficList.map(a => a.successSms);
          this.pendingSMSTile = this.apiTrafficList.map(a => a.pendingSms);
          this.failedSMSTile = this.apiTrafficList.map(a => a.failedSms);
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }
}
