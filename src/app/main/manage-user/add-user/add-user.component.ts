import { Component, OnInit } from "@angular/core";
import Stepper from "bs-stepper";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageUserService } from "../manage-user.service";
import Swal from "sweetalert2";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";

@Component({
  selector: "app-add-user",
  templateUrl: "./add-user.component.html",
  styleUrls: ["./add-user.component.scss"],
  providers: [ManageUserService]
})
export class AddUserComponent implements OnInit {
  private stepper: Stepper;

  HTTP: number = 1;
  BULK: number = 2;
  FLAG_YES: string = "Y";

  currentStep: number = 1;

  isUniqueUsername: boolean = true;
  isUniqueEmailAddress: boolean = true;
  skipConfigurations: boolean = false;
  disableSubmit: boolean = false;

  mainForm: FormGroup;

  stepOne = { formSubmitted: false, formCompleted: false };
  stepTwo = { formSubmitted: false, formCompleted: false };
  stepThree = { formSubmitted: false, formCompleted: false };

  topologyList: Array<object> = [];
  reportPrivileges: Array<object> = [];

  categoryList: Array<object> = [];
  channelList: Array<object> = [];
  //topicList: Array<object> = [];
  approverList: Array<object> = [];
  roleList: Array<object> = [];
  allDepartmentList: Array<object> = [];
  departmentList: Array<object> = [];
  accessPermissionViewList: Array<object> = [];

  selectedRole: object = {};

  userId = -1;
  userInfo: object = {};

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public commonService: CommonService,
    private manageUserService: ManageUserService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params["userId"]) this.userId = params["userId"];
    });

    this.stepper = new Stepper(document.querySelector("#stepper1"), {
      linear: true,
      animation: true
    });
    this.prepareAccessPermissionViewList();
    this.userRegistrationFormSetup();
    this.loadRoleList();

    if (this.userId != -1) {
      this.loadDepartmentList();
      this.fetchExistingUserDetails();
    }
  }
  prepareAccessPermissionViewList() {
    this.accessPermissionViewList = this.manageUserService.getPermissionsViewList();
  }

  userRegistrationFormSetup() {
    this.mainForm = this.formBuilder.group({
      personalInfo: this.formBuilder.group(
        {
          firstName: [
            "",
            [
              Validators.required,
              Validators.minLength(3),
              Validators.maxLength(50),
              Validators.pattern(this.commonService.patterns.name)
            ]
          ],
          lastName: [
            "",
            [
              Validators.required,
              Validators.maxLength(50),
              Validators.pattern(this.commonService.patterns.lastName)
            ]
          ],
          userName: [
            "",
            [
              Validators.required,
              Validators.minLength(5),
              Validators.maxLength(50),
              Validators.pattern(
                this.commonService.patterns.alphaNumericStartWithOnlyAlphabet
              )
              //this.validatorUniqueUsername(this)
            ]
          ],
          emailAddress: [
            "",
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.email)
              //    this.validatorUniqueEmailAddress(this)
            ]
          ],
          alternativeEmailAddress: [
            "",
            [Validators.pattern(this.commonService.patterns.email)]
          ],
          mobileNumber: [
            "",
            [
              Validators.required,
              Validators.minLength(10),
              Validators.maxLength(10),
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ],
          department: ["", [Validators.required]],
          role: ["", [Validators.required]],
          approver: ["", [Validators.required]]
        },
        { validator: this.matchPrimaryAndAlternateEmailAddress }
      ),
      configuration: this.formBuilder.group(
        {
          category: ["", [Validators.required]],
          channel: ["", [Validators.required]],
          tps: ["", [Validators.required]],
          ipWhitelist: [""],
          duplicateCheckFlag: [false],
          templateCheckFlag: [false],
          templateApprovalCheckFlag: [false],
          senderIdCheckFlag: [false],
          senderIdApprovalFlag: [false],
          DLRReportRequestFlag: [false],
          internationalRouteFlag: [false],
          longMessageSupportFlag: [false],
          unicodeMessageSupportFlag: [false],
          maxMessagePartLength_bulk: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ),
          maxFileSize_bulk: new FormControl({ disabled: true, value: "" }, [
            Validators.required,
            Validators.pattern(this.commonService.patterns.numberOnly)
          ]),
          maxRecipientCount_bulk: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ),
          maxSenderId_bulk: new FormControl({ disabled: true, value: "" }, [
            Validators.required,
            Validators.pattern(this.commonService.patterns.numberOnly)
          ]),
          maxTemplate_bulk: new FormControl({ disabled: true, value: "" }, [
            Validators.required,
            Validators.pattern(this.commonService.patterns.numberOnly)
          ]),
          maxDailyThresholdLimit_bulk: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ),
          maxHttpConnectionCount: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ),
          DLRCallbackTopology: new FormControl({ disabled: true, value: "" }, [
            Validators.required
          ]),
          DLRCallbackPrimaryURL: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.url)
            ]
          ),
          DLRCallbackSecondaryURL: new FormControl(
            { disabled: true, value: "" },
            [Validators.pattern(this.commonService.patterns.url)]
          ),
          // DLRCallbackQueue: new FormControl({ disabled: true, value: "" }, [
          //   Validators.pattern(this.commonService.patterns.alphaNumericOnly)
          // ]),
          // DLRCallbackTopic: new FormControl({ disabled: true, value: "" }, [
          //   Validators.pattern(this.commonService.patterns.alphaNumericOnly)
          // ]),
          DLRCallbackMaxConnectionCount: new FormControl(
            { disabled: true, value: "" },
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          )
        },
        { validator: this.matchPrimaryAndSecondaryURL }
      ),
      authorization: this.formBuilder.group({
        reportPrivileges: ["", [Validators.required]],
        transactionId: [
          "",
          [
            Validators.required,
            Validators.pattern(this.commonService.patterns.numberOnly)
          ]
        ]
      })
    });

    let authFormGroup = <FormGroup>this.mainForm.controls.authorization;
    this.accessPermissionViewList.forEach(element => {
      if (!element["isSubModule"]) {
        authFormGroup.addControl(
          element["formControlName"],
          new FormControl("H", Validators.required)
        );
      } else {
        let subModuleList = element["subModuleList"];
        subModuleList.forEach(subElement => {
          authFormGroup.addControl(
            subElement["formControlName"],
            new FormControl("H", Validators.required)
          );
        });
      }
    });

    this.personalInfoForm["userName"].valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe(value => {
        if (this.personalInfoForm["userName"].valid) {
          let obj = {
            id: this.userId != -1 ? this.userId : 0,
            value: value
          };
          this.manageUserService.validateUsername(obj).subscribe(data => {
            if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
              this.isUniqueUsername = data["data"];
            } else {
              this.isUniqueUsername = false;
            }
          });
        }
      });

    this.personalInfoForm["emailAddress"].valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged()
      )
      .subscribe(value => {
        if (this.personalInfoForm["emailAddress"].valid) {
          let obj = {
            id: this.userId != -1 ? this.userId : 0,
            value: value
          };
          this.manageUserService.validateEmailAddress(obj).subscribe(data => {
            if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
              this.isUniqueEmailAddress = data["data"];
            } else {
              this.isUniqueEmailAddress = false;
            }
          });
        }
      });
  }

  loadDepartmentList() {
    if (this.departmentList.length == 0) {
      this.spinner.show();
      this.manageUserService.getDepartmentList().subscribe(
        data => {
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.departmentList = [];
            this.allDepartmentList = data["data"];
            let sysDeptId = this.manageUserService.SYSTEM_DEPT_ID;
            if (this.selectedRole["isSystemRole"] == this.FLAG_YES) {
              let dept = this.allDepartmentList.find(x => x["id"] == sysDeptId);
              this.departmentList.push(dept);
            } else {
              this.departmentList = this.allDepartmentList.filter(
                x => x["id"] != sysDeptId
              );
            }
          }else{
            this.commonService.showErrorToast(data['result']['userMsg']);
          }
          this.spinner.hide();
        });
    } else {
      this.departmentList = [];
      let sysDeptId = this.manageUserService.SYSTEM_DEPT_ID;
      if (this.selectedRole["isSystemRole"] == this.FLAG_YES) {
        let dept = this.allDepartmentList.find(x => x["id"] == sysDeptId);
        this.departmentList.push(dept);
      } else {
        this.departmentList = this.allDepartmentList.filter(
          x => x["id"] != sysDeptId
        );
      }
    }
  }

  loadRoleList() {
    this.manageUserService.getRoleList().subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.roleList = data["data"];
      }
    });
  }

  loadApproverList() {
    let dept = this.personalInfoForm["department"].value.trim();
    let role = this.personalInfoForm["role"].value.trim();
    if (dept != "" && role != "") {
      this.spinner.show();
      this.manageUserService.getApproverList(dept, role).subscribe(data => {
        this.spinner.hide();
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          this.approverList = data["data"];
        }
      });
    }
  }

  fetchExistingUserDetails() {
    if (this.userId != -1) {
      this.spinner.show();
      this.manageUserService.getUser(this.userId).subscribe(data => {
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          this.userInfo = data["data"];
          this.loadExistingUserDetails();
        } else {
          this.commonService.showErrorToast(data['result']['userMsg']);
          this.router.navigate(["/main/manage-user/view-user"]);
        }
      });
    }
  }

  loadExistingUserDetails() {
    this.personalInfoForm["userName"].setValue(this.userInfo["username"]);
    this.personalInfoForm["userName"].disable();
    this.personalInfoForm["firstName"].setValue(this.userInfo["firstName"]);
    this.personalInfoForm["lastName"].setValue(this.userInfo["lastName"]);
    this.personalInfoForm["emailAddress"].setValue(this.userInfo["email"]);
    this.personalInfoForm["alternativeEmailAddress"].setValue(
      this.userInfo["alternateEmails"] ? this.userInfo["alternateEmails"] : ""
    );
    this.personalInfoForm["mobileNumber"].setValue(this.userInfo["msisdn"]);
    this.personalInfoForm["department"].setValue(
      this.userInfo["deptId"]["id"] + ""
    );
    this.personalInfoForm["role"].setValue(this.userInfo["roleId"]["id"] + "");
    this.personalInfoForm["approver"].setValue(
      this.userInfo["approverId"] + ""
    );

    this.selectedRole = this.userInfo["roleId"];

    if (
      this.userInfo["roleId"]["configurationFlag"] == "R" ||
      (this.userInfo["roleId"]["configurationFlag"] == "N" &&
        this.userInfo["categoryId"]["id"] != 0)
    ) {
      this.configurationForm["category"].setValue(
        this.userInfo["categoryId"]["id"]
      );
      this.configurationForm["channel"].setValue(
        this.userInfo["channelId"]["id"]
      );
      this.configurationForm["tps"].setValue(this.userInfo["tps"]);
      this.configurationForm["duplicateCheckFlag"].setValue(
        this.userInfo["duplicateCheck"] == "Y"
      );
      this.configurationForm["templateCheckFlag"].setValue(
        this.userInfo["templateCheck"] == "Y"
      );
      this.configurationForm["templateApprovalCheckFlag"].setValue(
        this.userInfo["templateApprovalReq"] == "Y"
      );
      this.configurationForm["senderIdCheckFlag"].setValue(
        this.userInfo["senderidCheck"] == "Y"
      );
      this.configurationForm["senderIdApprovalFlag"].setValue(
        this.userInfo["senderidApprovalReq"] == "Y"
      );
      this.configurationForm["DLRReportRequestFlag"].setValue(
        this.userInfo["dlrReportsReq"] == "Y"
      );
      this.configurationForm["maxHttpConnectionCount"].setValue(
        this.configurationForm["channel"].value == this.HTTP
          ? this.userInfo["maxConnection"]
          : ""
      );

      if (this.configurationForm["channel"].value == this.BULK) {
        this.configurationForm["maxMessagePartLength_bulk"].setValue(
          this.userInfo["maxMessageParts"]
        );
        this.configurationForm["maxFileSize_bulk"].setValue(
          this.userInfo["maxFilesize"]
        );
        this.configurationForm["maxRecipientCount_bulk"].setValue(
          this.userInfo["maxRecipientCount"]
        );
        this.configurationForm["maxSenderId_bulk"].setValue(
          this.userInfo["maxSenderid"]
        );
        this.configurationForm["maxTemplate_bulk"].setValue(
          this.userInfo["maxTemplate"]
        );
      }

      if (this.userInfo["dlrReportsReq"] == "Y") {
        this.configurationFormFlagChanged("DLRReportRequestFlag", [
          // "DLRCallbackQueue",
          // "DLRCallbackTopic",
          "DLRCallbackMaxConnectionCount",
          "DLRCallbackTopology",
          "DLRCallbackPrimaryURL",
          "DLRCallbackSecondaryURL"
        ]);
        this.configurationForm["DLRCallbackPrimaryURL"].setValue(
          this.userInfo["userDlrCallback"]["primaryUrl"]
        );
        this.configurationForm["DLRCallbackSecondaryURL"].setValue(
          this.userInfo["userDlrCallback"]["secondaryUrl"]
            ? this.userInfo["userDlrCallback"]["secondaryUrl"]
            : ""
        );
        // this.configurationForm["DLRCallbackTopic"].setValue(
        //   this.userInfo["userDlrCallback"]["topicId"]["id"]
        // );
        this.configurationForm["DLRCallbackTopology"].setValue(
          this.userInfo["userDlrCallback"]["topologyMode"]
        );
        this.configurationForm["DLRCallbackMaxConnectionCount"].setValue(
          this.userInfo["userDlrCallback"]["maxConnection"]
        );
      }

      this.channelChanged(this.configurationForm["channel"].value);
      this.loadApproverList();
    }

    this.authorizationForm["reportPrivileges"].setValue(
      this.userInfo["accessJson"]
    );

    this.authorizationForm["transactionId"].setValue(
      this.userInfo["transactionId"]
    );

    let pJson = JSON.parse(this.userInfo["permissionJson"]);
    this.accessPermissionViewList.forEach(element => {
      let val = "H";
      if (pJson[element["formControlName"]])
        val = pJson[element["formControlName"]];

      this.authorizationForm[element["formControlName"]].setValue(val);
    });

    this.spinner.hide();
  }

  saveUser() {
    this.stepThree.formSubmitted = true;

    if (!this.mainForm.controls.authorization.valid) {
      return;
    }

    if (
      this.mainForm.valid ||
      ((this.selectedRole["configurationFlag"] == "S" ||
        this.selectedRole["configurationFlag"] == "N") &&
        this.mainForm.controls["personalInfo"].valid &&
        this.mainForm.controls["authorization"].valid)
    ) {
      this.spinner.show();
      this.disableSubmit = true;

      let permissionJson = {};
      this.accessPermissionViewList.forEach(element => {
        if (element["isSubModule"]) {
          let subModuleList = element["subModuleList"];
          subModuleList.forEach(ele => {
            permissionJson[ele["formControlName"]] = this.authorizationForm[
              ele["formControlName"]
            ].value;
          });
        } else {
          permissionJson[element["formControlName"]] = this.authorizationForm[
            element["formControlName"]
          ].value;
        }
      });

      let finalObj = {
        firstName: this.personalInfoForm["firstName"].value,
        lastName: this.personalInfoForm["lastName"].value,
        username: this.personalInfoForm["userName"].value,
        email: this.personalInfoForm["emailAddress"].value,
        alternateEmails:
          this.personalInfoForm["alternativeEmailAddress"].value.trim().length >
          0
            ? this.personalInfoForm["alternativeEmailAddress"].value
            : null,
        msisdn: this.personalInfoForm["mobileNumber"].value,
        deptId: Number(this.personalInfoForm["department"].value),
        roleId: Number(this.personalInfoForm["role"].value),
        approverId: Number(this.personalInfoForm["approver"].value),

        categoryId: Number(this.configurationForm["category"].value),
        channelId: Number(this.configurationForm["channel"].value),
        maxConnection: this.configurationForm["maxHttpConnectionCount"].value
          ? Number(this.configurationForm["maxHttpConnectionCount"].value)
          : 0,
        maxMessageParts: this.configurationForm["maxMessagePartLength_bulk"]
          .value
          ? Number(this.configurationForm["maxMessagePartLength_bulk"].value)
          : 0,
        maxFilesize: this.configurationForm["maxFileSize_bulk"].value
          ? Number(this.configurationForm["maxFileSize_bulk"].value)
          : 0,
        maxRecipientCount: this.configurationForm["maxRecipientCount_bulk"]
          .value
          ? Number(this.configurationForm["maxRecipientCount_bulk"].value)
          : 0,
        maxSenderid: this.configurationForm["maxSenderId_bulk"].value
          ? Number(this.configurationForm["maxSenderId_bulk"].value)
          : 0,
        maxTemplate: this.configurationForm["maxTemplate_bulk"].value
          ? Number(this.configurationForm["maxTemplate_bulk"].value)
          : 0,
        maxDailyThresholdLimit: this.configurationForm[
          "maxDailyThresholdLimit_bulk"
        ].value
          ? Number(this.configurationForm["maxDailyThresholdLimit_bulk"].value)
          : 0,
        tps: Number(this.configurationForm["tps"].value),
        ipWhitelistDetails: this.configurationForm["ipWhitelist"].value,
        duplicateCheck: this.configurationForm["duplicateCheckFlag"].value
          ? "Y"
          : "N",
        internationalRouteAllowed: this.configurationForm[
          "internationalRouteFlag"
        ].value
          ? "Y"
          : "N",
        templateCheck: this.configurationForm["templateCheckFlag"].value
          ? "Y"
          : "N",
        templateApprovalReq: this.configurationForm["templateApprovalCheckFlag"]
          .value
          ? "Y"
          : "N",
        senderidCheck: this.configurationForm["senderIdCheckFlag"].value
          ? "Y"
          : "N",
        senderidApprovalReq: this.configurationForm["senderIdApprovalFlag"]
          .value
          ? "Y"
          : "N",
        longMessageAllowed: this.configurationForm["longMessageSupportFlag"]
          .value
          ? "Y"
          : "N",
        unicodeSupport: this.configurationForm["unicodeMessageSupportFlag"]
          .value
          ? "Y"
          : "N",
        dlrReportsReq: this.configurationForm["DLRReportRequestFlag"].value
          ? "Y"
          : "N",
        userDlrCallbackDetails: {
          primaryUrl:
            this.configurationForm["DLRReportRequestFlag"].value &&
            this.configurationForm["DLRCallbackPrimaryURL"].value
              ? this.configurationForm["DLRCallbackPrimaryURL"].value
              : null,
          secondaryUrl:
            this.configurationForm["DLRReportRequestFlag"].value &&
            this.configurationForm["DLRCallbackSecondaryURL"].value
              ? this.configurationForm["DLRCallbackSecondaryURL"].value
              : null,
          // queue:
          //   this.configurationForm["DLRReportRequestFlag"].value &&
          //   this.configurationForm["DLRCallbackTopic"].value
          //     ? this.configurationForm["DLRCallbackTopic"].value
          //     : "",
          // topic:
          //   this.configurationForm["DLRReportRequestFlag"].value &&
          //   this.configurationForm["DLRCallbackTopic"].value
          //     ? this.configurationForm["DLRCallbackTopic"].value
          //     : "",
          topologyMode:
            this.configurationForm["DLRReportRequestFlag"].value &&
            this.configurationForm["DLRCallbackTopology"].value
              ? Number(this.configurationForm["DLRCallbackTopology"].value)
              : 0,
          maxConnection:
            this.configurationForm["DLRReportRequestFlag"].value &&
            this.configurationForm["DLRCallbackMaxConnectionCount"].value
              ? Number(
                  this.configurationForm["DLRCallbackMaxConnectionCount"].value
                )
              : 0
        },

        accessJson: this.authorizationForm["reportPrivileges"].value,
        permissionJson: JSON.stringify(permissionJson),

        solaceKey: null,
        createdBy: this.commonService.getUserName(),
        expiryDate: null,
        skipRouting: this.skipConfigurations ? "Y" : "N",
        loginId: this.commonService.getUser()
      };
      if (this.userId == -1) {
        this.manageUserService
          .saveUser(finalObj, this.authorizationForm["transactionId"].value)
          .subscribe(
            data => {
              this.disableSubmit = false;
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessAlertMessage(
                  "User created successfully"
                );
                this.router.navigate(["/main/manage-user/view-user"]);
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            },
            error => {
              this.disableSubmit = false;              
            }
          );
      } else {
        this.manageUserService
          .updateUser(
            this.userId,
            finalObj,
            this.authorizationForm["transactionId"].value
          )
          .subscribe(
            data => {
              this.disableSubmit = false;
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessAlertMessage(
                  "User updated successfully"
                );
                this.router.navigate(["/main/manage-user/view-user"]);
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            },
            error => {
              this.disableSubmit = false;              
            }
          );
      }
    }
  }

  matchPrimaryAndAlternateEmailAddress(group: FormGroup) {
    let primaryEmailAddress = group.controls["emailAddress"].value.trim();
    let alternativeEmailAddress = group.controls[
      "alternativeEmailAddress"
    ].value.trim();
    if (
      primaryEmailAddress.length > 0 &&
      alternativeEmailAddress.length > 0 &&
      primaryEmailAddress == alternativeEmailAddress
    ) {
      return { emailAddressMatched: true };
    }
    return null;
  }

  matchPrimaryAndSecondaryURL(group: FormGroup) {
    if (group.controls["DLRReportRequestFlag"].value) {
      let primaryURL = group.controls["DLRCallbackPrimaryURL"].value.trim();
      let secondaryURL = group.controls["DLRCallbackSecondaryURL"].value.trim();

      if (
        primaryURL.length > 0 &&
        secondaryURL.length > 0 &&
        primaryURL == secondaryURL
      ) {
        return { DLRCallbackURLsMatched: true };
      }
    }
    return null;
  }

  topologyChanged(value) {
    if (value == 1) {
      this.configurationForm["DLRCallbackSecondaryURL"].clearValidators();
      this.configurationForm["DLRCallbackSecondaryURL"].setValidators([
        Validators.required,
        Validators.pattern(this.commonService.patterns.url)
      ]);
      this.configurationForm[
        "DLRCallbackSecondaryURL"
      ].updateValueAndValidity();
    } else {
      this.configurationForm["DLRCallbackSecondaryURL"].clearValidators();
      this.configurationForm["DLRCallbackSecondaryURL"].setValidators([
        Validators.pattern(this.commonService.patterns.url)
      ]);
      this.configurationForm[
        "DLRCallbackSecondaryURL"
      ].updateValueAndValidity();
    }
  }

  configurationFormFlagChanged(flagName, fields) {
    if (this.configurationForm[flagName].value) {
      for (let x = 0; x < fields.length; x++)
        this.configurationForm[fields[x]].enable();
    } else {
      for (let x = 0; x < fields.length; x++)
        this.configurationForm[fields[x]].disable();
    }
  }

  departmentChanged() {
    this.loadApproverList();
  }

  roleChanged() {
    this.selectedRole = this.roleList.find(
      x => x["id"] == this.personalInfoForm["role"].value
    );
    this.personalInfoForm["department"].setValue("");
    this.loadDepartmentList();

    let pJson = JSON.parse(this.selectedRole["permissionJson"]);

    this.accessPermissionViewList.forEach(element => {
      let val = "H";
      if (pJson[element["formControlName"]])
        val = pJson[element["formControlName"]];
      this.authorizationForm[element["formControlName"]].setValue(val);
    });

    if (this.stepOne.formCompleted)
      this.mainForm.controls["configuration"].reset({
        category: "",
        channel: "",
        tps: "",
        ipWhitelist: "",
        duplicateCheckFlag: false,
        templateCheckFlag: false,
        templateApprovalCheckFlag: false,
        senderIdCheckFlag: false,
        senderIdApprovalFlag: false,
        DLRReportRequestFlag: false,
        longMessageSupportFlag: false,
        unicodeMessageSupportFlag: false,
        maxMessagePartLength_bulk: "",
        maxFileSize_bulk: "",
        maxRecipientCount_bulk: "",
        maxSenderId_bulk: "",
        maxTemplate_bulk: "",
        maxHttpConnectionCount: "",
        DLRCallbackTopology: "",
        DLRCallbackPrimaryURL: "",
        DLRCallbackSecondaryURL: "",
        // DLRCallbackTopic: "",
        DLRCallbackMaxConnectionCount: ""
      });
  }

  channelChanged(value) {
    if (value == this.HTTP) {
      this.configurationForm["maxHttpConnectionCount"].enable();

      this.configurationForm["maxMessagePartLength_bulk"].disable();
      this.configurationForm["maxFileSize_bulk"].disable();
      this.configurationForm["maxRecipientCount_bulk"].disable();
      this.configurationForm["maxSenderId_bulk"].disable();
      this.configurationForm["maxTemplate_bulk"].disable();
      this.configurationForm["maxDailyThresholdLimit_bulk"].disable();
    } else if (value == this.BULK) {
      this.configurationForm["maxHttpConnectionCount"].disable();

      this.configurationForm["maxMessagePartLength_bulk"].enable();
      this.configurationForm["maxFileSize_bulk"].enable();
      this.configurationForm["maxRecipientCount_bulk"].enable();
      this.configurationForm["maxSenderId_bulk"].enable();
      this.configurationForm["maxTemplate_bulk"].enable();
      this.configurationForm["maxDailyThresholdLimit_bulk"].enable();
    }
  }

  modulePermissionChanged(item, permission) {
    if (permission == "H") {
      let subModule = this.accessPermissionViewList.find(
        x => x["formControlName"] == item["formControlName"] && x["isSubModule"]
      );
      if (subModule) {
        for (var x = 0; x <= subModule["subModuleList"].length - 1; x++) {
          this.authorizationForm[
            subModule["subModuleList"][x]["formControlName"]
          ].setValue("H");
        }
      }
    }
  }

  subModulePermissionChanged(item, permission) {
    if (
      (permission == "RW" || permission == "R") &&
      this.authorizationForm[item.formControlName].value == "H"
    )
      this.authorizationForm[item.formControlName].setValue("R");
  }

  get personalInfoForm() {
    let x = <FormGroup>this.mainForm.controls.personalInfo;
    return x.controls;
  }

  get configurationForm() {
    let x = <FormGroup>this.mainForm.controls.configuration;
    return x.controls;
  }

  get authorizationForm() {
    let x = <FormGroup>this.mainForm.controls.authorization;
    return x.controls;
  }

  skipConfigurationsMethod() {
    Swal.fire({
      title: "Are you sure?",
      text: "User will not able to access all features on site!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ok, Proceed"
    }).then(result => {
      if (result.value) {
        this.skipConfigurations = true;
        this.stepTwo.formSubmitted = false;
        this.currentStep = 3;
        if (!this.stepTwo.formCompleted) {
          this.stepTwo.formCompleted = this.mainForm.controls.configuration.valid;
          this.reportPrivileges = this.manageUserService.getReportPrivilegesList();
        }
        this.stepper.next();
      }
    });
  }

  cancel() {
    Swal.fire({
      title: "Are you sure?",
      text: "All information will be discarded!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ok, Left this page"
    }).then(result => {
      if (result.value) {
        this.router.navigate(["/main/manage-user/view-user"]);
      }
    });
  }

  /* multi-step form method*/
  next() {
    if (this.currentStep == 1) {
      this.stepOne.formSubmitted = true;
      if (
        this.mainForm.controls.personalInfo.valid &&
        this.isUniqueUsername &&
        this.isUniqueEmailAddress
      ) {
        if (
          this.selectedRole["configurationFlag"] == "R" ||
          this.selectedRole["configurationFlag"] == "N"
        ) {
          this.stepTwo.formSubmitted = false;
          this.currentStep = 2;
          if (!this.stepOne.formCompleted) {
            this.stepOne.formCompleted = this.mainForm.controls.personalInfo.valid;

            this.topologyList = this.manageUserService.getTopologyList();

            this.manageUserService.getChannelList().subscribe(data => {
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.channelList = data["data"];
              }
            });

            this.manageUserService.getCategoryList().subscribe(data => {
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.categoryList = data["data"];
              }
            });

            // this.manageUserService.getTopicList().subscribe(data => {
            //   if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            //     this.topicList = data["data"];
            //   }
            // });
          }
        } else if (this.selectedRole["configurationFlag"] == "S") {
          this.skipConfigurations = true;
          this.currentStep = 3;
          this.reportPrivileges = this.manageUserService.getReportPrivilegesList();
          this.stepper.next();
        }
        this.stepper.next();
      }
    } else if (this.currentStep == 2) {
      this.stepTwo.formSubmitted = true;

      if (this.mainForm.controls.configuration.valid) {
        this.skipConfigurations = false;
        this.currentStep = 3;
        if (!this.stepTwo.formCompleted) {
          this.stepTwo.formCompleted = this.mainForm.controls.configuration.valid;
          this.reportPrivileges = this.manageUserService.getReportPrivilegesList();
        }
        this.stepper.next();
      }
    }
  }

  /* multi-step form method*/
  previous() {
    if (this.currentStep == 3) {
      if (
        this.selectedRole["configurationFlag"] == "R" ||
        this.selectedRole["configurationFlag"] == "N"
      ) {
        this.currentStep = 2;
        this.stepper.previous();
      }

      if (this.selectedRole["configurationFlag"] == "S") {
        this.currentStep = 1;
        this.stepper.previous();
        this.stepper.previous();
      }
    } else {
      this.currentStep = 1;
      this.stepper.previous();
    }
  }
}
