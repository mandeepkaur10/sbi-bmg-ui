import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";
import { MODULE } from "src/app/common/common.const";

@Injectable()
export class ManageUserService {
  NO_HEADER = null;
  SYSTEM_DEPT_ID = 2000;
  loginId = "loginId=";
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {
    this.loginId = this.loginId + this._commonService.getUser();
  }

  validateUsername(obj) {
    return this._networkService.get(
      "user/validate/username/" +
        obj.value +
        "?userId=" +
        obj.id +
        "&" +
        this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  validateEmailAddress(obj) {
    return this._networkService.get(
      "user/validate/email/" +
        obj.value +
        "?userId=" +
        obj.id +
        "&" +
        this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  saveUser(user, txnId) {
    return this._networkService.post(
      "user" + "?transactionId=" + txnId + "&" + this.loginId,
      user,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getUser(id) {
    return this._networkService.get(
      "user/" + id + "?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  updateUser(userId, user, txnId) {
    return this._networkService.put(
      "user/" + userId + "?transactionId=" + txnId + "&" + this.loginId,
      user,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  updateUserStatus(userId, obj) {
    return this._networkService.put(
      "user/" + userId + "/status/update?" + this.loginId,
      obj,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  deleteUser(userId, loginUserId, remarks) {
    return this._networkService.delete(
      "user/" + userId + "?loginId=" + loginUserId + "&remarks=" + remarks,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getUsersList(type) {
    return this._networkService.get(
      "user/list/" + type + "?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getApproverList(dept, role) {
    return this._networkService.get(
      "user/approver/list?department=" +
        dept +
        "&role=" +
        role +
        "&" +
        this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getDepartmentList() {
    return this._networkService.get(
      "master/department?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getTopicList() {
    return this._networkService.get(
      "master/topic",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getRoleList() {
    return this._networkService.get(
      "master/role?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getCategoryList() {
    return this._networkService.get(
      "master/category?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getChannelList() {
    return this._networkService.get(
      "master/channel?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
    //return [{ id: 1, name: "HTTP" }, { id: 2, name: "Bulk" }];
  }

  getStatusList() {
    return this._networkService.get(
      "user/status/list?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getPrimaryRouteList() {
    //return this._networkService.get("department", this.NO_HEADER, this._commonService.bearer);

    return [
      { id: 1, name: "RouteOne" },
      { id: 2, name: "RouteTwo" },
      { id: 3, name: "RouteThree" }
    ];
  }

  getInternationalRoutesList() {
    //return this._networkService.get("department", null, this._commonService.bearer);

    return [
      { id: 1, name: "RouteOne" },
      { id: 2, name: "RouteTwo" },
      { id: 3, name: "RouteThree" }
    ];
  }

  getRetryRoutesList() {
    //return this._networkService.get("department", null, this._commonService.bearer);

    return [
      { id: 1, name: "RouteOne" },
      { id: 2, name: "RouteTwo" },
      { id: 3, name: "RouteThree" }
    ];
  }

  getReportPrivilegesList() {
    return [
      { id: 1, name: "Self" },
      { id: 2, name: "Department" },
      { id: 3, name: "All" }
    ];
  }

  getTopologyList() {
    return [
      { id: 1, name: "Active/Active" },
      { id: 2, name: "Active/Passive" }
    ];
  }

  getPermissionsViewList() {
    let list = [];

    list.push({
      sno: "1",
      view: "Dashboard",
      formControlName: MODULE.DASHBOARD,
      isSubModule: false
    });

    list.push({
      sno: "2",
      view: "Registration Requests",
      formControlName: MODULE.REGISTRATION_REQUEST,
      isSubModule: false
    });

    list.push({
      sno: "3",
      view: "Manage User",
      formControlName: MODULE.USER,
      isSubModule: false
    });

    var userManagementSubModules = [];

    userManagementSubModules.push({
      sno: "A",
      view: "Approval Requests",
      formControlName: MODULE.USER_APPROVAL
    });

    list.push({
      sno: "3",
      view: "Manage User",
      formControlName: MODULE.USER,
      isSubModule: true,
      subModuleList: userManagementSubModules
    });

    list.push({
      sno: "4",
      view: "Route Management",
      formControlName: MODULE.ROUTES,
      isSubModule: false
    });

    list.push({
      sno: "5",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: false
    });

    var campaignManagementSubModules = [];

    campaignManagementSubModules.push({
      sno: "A",
      view: "Approval Requests",
      formControlName: MODULE.CAMPAIGN_APPROVAL
    });

    list.push({
      sno: "5",
      view: "Manage Campaign",
      formControlName: MODULE.CAMPAIGN,
      isSubModule: true,
      subModuleList: campaignManagementSubModules
    });

    // list.push({
    //   sno: "4",
    //   view: "Manage Aggregator",
    //   formControlName: MODULE.AGGREGATOR,
    //   isSubModule: false
    // });

    list.push({
      sno: "6",
      view: "Manage Address Book",
      formControlName: MODULE.ADDRESS_BOOK,
      isSubModule: false
    });

    list.push({
      sno: "7",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: false
    });

    var senderIdManagementSubModules = [];

    senderIdManagementSubModules.push({
      sno: "A",
      view: "Approval Requests",
      formControlName: MODULE.SENDERID_APPROVAL
    });

    senderIdManagementSubModules.push({
      sno: "B",
      view: "Bulk Upload",
      formControlName: MODULE.SENDERID_BULKUPLOAD
    });

    list.push({
      sno: "7",
      view: "Manage Sender CLI",
      formControlName: MODULE.SENDERID,
      isSubModule: true,
      subModuleList: senderIdManagementSubModules
    });

    list.push({
      sno: "8",
      view: "Manage SMS Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: false
    });

    var smsTemplateManagementSubModules = [];

    smsTemplateManagementSubModules.push({
      sno: "A",
      view: "Approval Requests",
      formControlName: MODULE.SMS_TEMPLATE_APPROVAL
    });

    smsTemplateManagementSubModules.push({
      sno: "B",
      view: "Bulk Upload",
      formControlName: MODULE.SMS_TEMPLATE_BULK_UPLOAD
    });

    list.push({
      sno: "8",
      view: "Manage SMS Template",
      formControlName: MODULE.SMS_TEMPLATE,
      isSubModule: true,
      subModuleList: smsTemplateManagementSubModules
    });

    list.push({
      sno: "9",
      view: "Manage MO Route",
      formControlName: MODULE.MO_ROUTE,
      isSubModule: false
    });

    list.push({
      sno: "10",
      view: "Manage MO Keywords",
      formControlName: MODULE.MO_KEYWORD,
      isSubModule: false
    });

    // list.push({
    //   sno: "7",
    //   view: "Manage Spam Keywords",
    //   formControlName: MODULE.SPAM_KEYWORD,
    //   isSubModule: false
    // });

    // list.push({
    //   sno: "9",
    //   view: "Department Configuration",
    //   formControlName: MODULE.DEPARTMENT_CONFIGURATION,
    //   isSubModule: false
    // });
    list.push({
      sno: "11",
      view: "System Configuration",
      formControlName: MODULE.SYSTEM_CONFIGURATION,
      isSubModule: false
    });

    list.push({
      sno: "12",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: false
    });

    var reportingSubModules = [];

    reportingSubModules.push({
      sno: "A",
      view: "Summary Reports",
      formControlName: MODULE.SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "B",
      view: "Detailed Report",
      formControlName: MODULE.DETAILED_REPORT
    });

    reportingSubModules.push({
      sno: "C",
      view: "MO Summary Reports",
      formControlName: MODULE.MO_SUMMARY_REPORT
    });

    reportingSubModules.push({
      sno: "D",
      view: "MO Detailed Report",
      formControlName: MODULE.MO_DETAILED_REPORT
    });

    list.push({
      sno: "12",
      view: "Reporting",
      formControlName: MODULE.REPORTS,
      isSubModule: true,
      subModuleList: reportingSubModules
    });

    return list;
  }
}
