import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ManageUserComponent } from "./manage-user.component";
import { ViewUserComponent } from "./view-user/view-user.component";
import { AddUserComponent } from "./add-user/add-user.component";
import { PendingRequestComponent } from "./pending-request/pending-request.component";
import { routing } from "./manage-user.routing";
import { SharedModule } from "../../common/shared.modules";

@NgModule({
  imports: [CommonModule, routing, SharedModule],
  declarations: [
    ManageUserComponent,
    ViewUserComponent,
    AddUserComponent,
    PendingRequestComponent
  ]
})
export class ManageUserModule {}
