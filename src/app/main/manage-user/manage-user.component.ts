import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../common/common.service";
import { MODULE } from "src/app/common/common.const";

@Component({
  selector: "app-manage-user",
  templateUrl: "./manage-user.component.html",
  styleUrls: ["./manage-user.component.scss"]
})
export class ManageUserComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) {}

  ngOnInit() {}
}
