import { Component, OnInit } from "@angular/core";
import {
  NgbModal,
  NgbModalRef,
  NgbModalOptions,
  ModalDismissReasons
} from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { Router } from "@angular/router";

import { CommonService } from "../../../common/common.service";
import { ManageUserService } from "../manage-user.service";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE, MODULE } from "src/app/common/common.const";

@Component({
  selector: "app-view-user",
  templateUrl: "./view-user.component.html",
  styleUrls: ["./view-user.component.scss"],
  providers: [ManageUserService]
})
export class ViewUserComponent implements OnInit {
  MODULES = MODULE;
  private modalRef: NgbModalRef;
  largeModalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false,
    size: "lg"
  };

  modalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;

  userSearchType: number = 1;

  userDetails: object;
  accessPermissionViewList: Array<object> = [];
  viewPermissionList: object = {};

  statusFormSubmitted: boolean = false;
  deleteFormSubmitted: boolean = false;

  statusForm: FormGroup;
  deleteForm: FormGroup;
  statusList: Array<object> = [];

  rows = [];
  temp = [];
  columns = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number;
  p: number = 1;
  table: DatatableComponent;
  searchText: string = "";
  userNameSorted: boolean = false;
  firstNameSorted: boolean = false;
  lastNameSorted: boolean = false;
  emailSorted: boolean = false;
  mobileNumberSorted: boolean = false;
  departmentSorted: boolean = false;
  roleSorted: boolean = false;
  statusDescSorted: boolean = false;
  creatadBySorted: boolean = false;
  createdOnSorted: boolean = false;

  constructor(
    public commonService: CommonService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private manageUserService: ManageUserService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getUsersList();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  getUsersList() {
    this.spinner.show();
    this.manageUserService.getUsersList(this.userSearchType).subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.rows = this.temp = data["data"];
      } else {
        this.rows = this.temp = [];
        this.commonService.showErrorToast(data['result']['userMsg']);
      }
      this.spinner.hide();
    });
  }

  // applySearch() {
  //   let self = this;
  //   this.rows = this.temp.filter(function(item) {
  //     return JSON.stringify(Object.values(item))
  //       .toLowerCase()
  //       .includes(self.searchText.toLowerCase());
  //   });
  // }
  editLink(id) {
    this.router.navigate(["/main/manage-user/add-user?userId=" + id]);
  }

  userSearchTypeChanged() {
    this.getUsersList();
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      newStatus: ["", [Validators.required]],
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.remarksDescription)
        ]
      ]
    });
  }

  viewUpdateStatusModal(user, content) {
    // btn &&
    //   btn.parentElement &&
    //   btn.parentElement.parentElement &&
    //   btn.parentElement.parentElement.blur();

    this.userDetails = user;
    this.statusFormSubmitted = false;
    this.prepareUpdateStatusForm();
    this.loadStatusList();
    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  loadStatusList() {
    if (this.statusList.length == 0) {
      this.spinner.show();
      this.manageUserService.getStatusList().subscribe(data => {
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          let tempList = data["data"];
          tempList = tempList.filter(
            x => x["id"] != this.userDetails["status"]
          );
          tempList = tempList.filter(x => x["id"] != 0);
          tempList = tempList.filter(x => x["id"] != 4);
          this.statusList = tempList;
        }
        this.spinner.hide();
      });
    }
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {  
    this.rows = this.commonService.sortArray(isAssending, sortBy, this.rows, isNumber );    
  }

  updateUserStatus(test) {
    this.statusFormSubmitted = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      let finalObj = {
        loginId: 0,
        status: this.statusForm.controls.newStatus.value,
        remarks: this.statusForm.controls.remarks.value
      };
      this.manageUserService
        .updateUserStatus(this.userDetails["userId"], finalObj)
        .subscribe(data => {
          this.spinner.hide();        
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast('update');
            this.modalRef.close();
            this.getUsersList();
          } else {
            this.commonService.showErrorToast(data['result']['userMsg']);
          }
        });
    }
  }

  prepareDeleteUserForm() {
    this.deleteForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.remarksDescription)
        ]
      ]
    });
  }

  viewDeleteUserModal(user, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.prepareDeleteUserForm();
    this.deleteFormSubmitted = false;
    this.userDetails = user;
    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  deleteUser() {
    this.deleteFormSubmitted = true;

    if (this.deleteForm.valid) {
      Swal.fire({
        title: "Are you sure?",
        text: "Do you really want to delete this user?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok, Proceed"
      }).then(result => {
        if (result.value) {
          this.spinner.show();
          let userId = this.userDetails["userId"];
          let remarks = this.deleteForm.controls.remarks.value;
          let loginId = 0;
          this.manageUserService
            .deleteUser(userId, loginId, remarks)
            .subscribe(data => {
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showSuccessToast('create');
                this.modalRef.close();
                this.getUsersList();
              } else {
                this.commonService.showErrorToast(data['result']['userMsg']);
              }
            });
        }
      });
    }
  }

  viewUser(userId, content) {
    // btn &&
    //   btn.parentElement &&
    //   btn.parentElement.parentElement &&
    //   btn.parentElement.parentElement.blur();

    this.spinner.show();
    this.manageUserService.getUser(userId).subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.userDetails = data["data"];
        this.viewPermissionList = JSON.parse(
          this.userDetails["permissionJson"]
        );

        if (this.accessPermissionViewList.length == 0)
          this.accessPermissionViewList = this.manageUserService.getPermissionsViewList();

        this.spinner.hide();
        this.modalRef = this.modalService.open(content, this.largeModalOptions);
        this.modalRef.result.then(
          result => {
            this.closeResult = `Closed with: ${result}`;
          },
          reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
        );
      } else {
        this.commonService.showErrorToast(data['result']['userMsg']);
      }
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
