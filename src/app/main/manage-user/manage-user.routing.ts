import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { AddUserComponent } from "./add-user/add-user.component";
import { ManageUserComponent } from "./manage-user.component";
import { ViewUserComponent } from "./view-user/view-user.component";
import { PendingRequestComponent } from "./pending-request/pending-request.component";
import { AuthGuardService, ModuleGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageUserComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-user/view-user",
        pathMatch: "full"
      },
      {
        path: "view-user",
        component: ViewUserComponent,
        data: { moduleName: MODULE.USER, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "add-user",
        component: AddUserComponent,
        data: { moduleName: MODULE.USER, permissions: ["RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "pending-request",
        component: PendingRequestComponent,
        data: {
          moduleName: MODULE.USER_APPROVAL,
          permissions: ["R", "RW"]
        },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
