import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";

@Injectable()
export class RegistrationRequestsService {
  NO_HEADERS = null;
  userId = "";
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {
    this.userId = "userId=" + this._commonService.getUser();
  }

  getRequestsList(role) {
    return this._networkService.get(
      "register/approve/history/" + role + "?" + this.userId,
      this.NO_HEADERS,
      this._commonService.bearer
    );
  }

  updateRequestStatus(id, data) {
    return this._networkService.put(
      "register/approve/" + id + "?" + this.userId,
      data,
      this.NO_HEADERS,
      this._commonService.bearer
    );
  }

  getStatusList() {
    return [{ id: 6, name: "Approve" }, { id: 7, name: "Reject" }];
  }

  downloadFile(id) {
    return this._networkService.getFile("register/form/" + id);
  }
}
