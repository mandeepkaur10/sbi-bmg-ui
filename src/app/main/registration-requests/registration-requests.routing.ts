import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ViewComponent } from "./view/view.component";
import { RegistrationRequestsComponent } from "./registration-requests.component";
import { ModuleGuardService, AuthGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: RegistrationRequestsComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/registration-requests/view",
        pathMatch: "full"
      },
      {
        path: "view",
        component: ViewComponent,
        data: {
          moduleName: MODULE.REGISTRATION_REQUEST,
          permissions: ["R", "RW"]
        },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
