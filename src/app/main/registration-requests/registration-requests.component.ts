import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../common/common.service";
import { MODULE } from "src/app/common/common.const";

@Component({
  selector: "app-registration-requests",
  templateUrl: "./registration-requests.component.html",
  styleUrls: ["./registration-requests.component.scss"]
})
export class RegistrationRequestsComponent implements OnInit {
  MODULES = MODULE;

  constructor(public commonService: CommonService) {}

  ngOnInit() {}
}
