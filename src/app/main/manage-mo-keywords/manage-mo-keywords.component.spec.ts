import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageMoKeywordsComponent } from './manage-mo-keywords.component';

describe('ManageMoKeywordsComponent', () => {
  let component: ManageMoKeywordsComponent;
  let fixture: ComponentFixture<ManageMoKeywordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageMoKeywordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageMoKeywordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
