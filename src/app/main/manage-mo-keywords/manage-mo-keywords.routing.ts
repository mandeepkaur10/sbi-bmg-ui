import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageMoKeywordsComponent } from "./manage-mo-keywords.component";
import { ListMoKeywordsComponent } from "./list-mo-keywords/list-mo-keywords.component";
import { CreateMoKeywordsComponent } from "./create-mo-keywords/create-mo-keywords.component";
import { ModuleGuardService, AuthGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageMoKeywordsComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-mo-keywords/list-mo-keywords",
        pathMatch: "full"
      },
      {
        path: "list-mo-keywords",
        component: ListMoKeywordsComponent,
        data: { moduleName: MODULE.MO_KEYWORD, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "create-mo-keywords",
        component: CreateMoKeywordsComponent,
        data: { moduleName: MODULE.MO_KEYWORD, permissions: ["RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
