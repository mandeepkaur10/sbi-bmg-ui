import { Component, OnInit } from '@angular/core';
import { CommonService } from "../../common/common.service";
import { MODULE } from "../../common/common.const";

@Component({
  selector: 'app-manage-mo-keywords',
  templateUrl: './manage-mo-keywords.component.html',
  styleUrls: ['./manage-mo-keywords.component.scss']
})
export class ManageMoKeywordsComponent implements OnInit {

  MODULES = MODULE;
  constructor( public commonService: CommonService ) { }

  ngOnInit() {
  }

}
