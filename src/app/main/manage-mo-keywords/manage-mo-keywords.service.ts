
import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';

@Injectable()
export class MoKeywordsService {

  constructor(private _networkService: NetworkService) { }

  getkeywordsList(id: any) {
    return this._networkService.get('mo/route?epoch=' + id + '&showAll=true', null, 'bearer');
  }

  // getAllUsers() {
  //   return this._networkService.get('user', null, 'basic');
  // }

  // getAllAccountTypes() {
  //   return this._networkService.get('types', null, 'basic');
  // }
  getDepartmentList() {
    return this._networkService.get('master/department', null, 'bearer');
  }
  getRouteList() {
    return this._networkService.get('mo/server', null, 'bearer');
  }

  saveMoKeyword(req: any) {
    return this._networkService.post('mo/route', req, null, 'bearer');
  }

  // getPendingSenderIdList(id: any){
  //   return this._networkService.get('senderid/info/'+ id +'/pending', null, 'basic');
  // }

  deleteMoKeyword(req: any) {
    return this._networkService.delete('mo/route/' + req['id'] + '?loginId=' + req['loginId'] + '&remarks=' + req['remarks'], null, null, 'bearer');
  }

  // updateSattus(req: any){
  //   return this._networkService.put('senderid/approve/'+ req["senderid"] +'?approvedBy='+ req["approvedBy"] +'&status=' + req["status"], null, null, 'basic');
  // }

}

