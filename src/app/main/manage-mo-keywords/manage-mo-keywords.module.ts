import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageMoKeywordsComponent } from './manage-mo-keywords.component';
import { ListMoKeywordsComponent } from './list-mo-keywords/list-mo-keywords.component';
import { CreateMoKeywordsComponent } from './create-mo-keywords/create-mo-keywords.component';
import { routing } from "./manage-mo-keywords.routing";
import { SharedModule } from "../../common/shared.modules";
import { MoKeywordsService } from "./manage-mo-keywords.service";

@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule
  ],
  declarations: [ManageMoKeywordsComponent, ListMoKeywordsComponent, CreateMoKeywordsComponent],
  providers: [ MoKeywordsService ]
})
export class ManageMoKeywordsModule { }
