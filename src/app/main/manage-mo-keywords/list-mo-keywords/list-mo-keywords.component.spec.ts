import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListMoKeywordsComponent } from './list-mo-keywords.component';

describe('ListMoKeywordsComponent', () => {
  let component: ListMoKeywordsComponent;
  let fixture: ComponentFixture<ListMoKeywordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListMoKeywordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListMoKeywordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
