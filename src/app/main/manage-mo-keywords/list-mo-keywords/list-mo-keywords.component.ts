import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { MoKeywordsService } from '../manage-mo-keywords.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { RESPONSE, MODULE } from "../../../common/common.const";
import { ExcelService } from "../../../common/excel.service";

@Component({
  selector: 'app-list-mo-keywords',
  templateUrl: './list-mo-keywords.component.html',
  styleUrls: ['./list-mo-keywords.component.scss']
})
export class ListMoKeywordsComponent implements OnInit {

  MODULES = MODULE;
  pageLimit: number;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  moKeywordList = [];
  p: number = 1;
  usernameSorted: boolean = false;
  keywordSorted: boolean = false;
  caseSensitivityFlagSorted: boolean = false;
  statusSorted: boolean = false;

  constructor(private modalService: NgbModal,
    private spinner: NgxSpinnerService, private moKeywordsService: MoKeywordsService,
    public commonService: CommonService, private excelService: ExcelService) { }

    ngOnInit() {
      this.getMoKeywordList();
      this.pageLimit = this.commonService.recordsPerPage[0];
    }
  
    getMoKeywordList() {
      let id = 0;
      this.spinner.show();
      this.moKeywordsService.getkeywordsList(id).subscribe(
      res => {
        this.spinner.hide();       
          if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
            this.moKeywordList = res["data"];
          }else{
            this.commonService.showErrorToast(res['result']['userMsg']);
            this.moKeywordList = [];
          }        
        });
    }
  
    confirmDeleteRecord(record: any) {
      
      let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
      modalRef.componentInstance.content = 'Are you sure you want to delete this record?';
      modalRef.componentInstance.leftButton = 'No';
      modalRef.componentInstance.rightButton = 'Yes';
      modalRef.result.then(
        (data: any) => {
          if (data == "yes") {
            this.deleteRecord(record);
          }
        },
        (reason: any) => {
  
        });
    }
  
    deleteRecord(record: any) {
      let req = {
        "id": record['id'],
        "loginId": this.commonService.getUser(),
        "remarks": "Delete Record"
      };
      this.spinner.show();
      this.moKeywordsService.deleteMoKeyword(req).subscribe(
        res => {
          this.spinner.hide();
          if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
            this.commonService.showSuccessToast("delete");
            this.getMoKeywordList();
          }else{
            this.commonService.showErrorToast(res['result']['userMsg']);
          }          
        });
    }

    sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {  
      this.moKeywordList = this.commonService.sortArray(isAssending, sortBy, this.moKeywordList, isNumber );    
    }

    exportAs(fileType: string){
      let data = [];   
      this.moKeywordList.forEach(element => {
        let excelDataObject = {};
        excelDataObject["Username"] = element.username;
        excelDataObject["Keyword"] = element.keyword;
        excelDataObject["Primary Route"] = element.primaryRoute;    
        excelDataObject["Secondary Route"] = element.secondaryRoute;  
        excelDataObject["Case Sensitivity Flag"] = element.caseSensitivityFlag.toLowerCase() == 'y' ? 'True' : 'False';  
        excelDataObject["Status"] = element.status == 1 ? 'Enable' : 'Disable';
        data.push(excelDataObject);
      });
      switch(fileType){
        case 'excel':
        this.excelService.exportAsExcelFile(data, 'MO-Keywords-List');
        break;
        case 'csv':
        this.excelService.exportAsCsvFile(data, 'MO-Keywords-List');
        break;
        default:
        this.excelService.exportAsExcelFile(data, 'MO-Keywords-List');
        break;
      }
    }

}
