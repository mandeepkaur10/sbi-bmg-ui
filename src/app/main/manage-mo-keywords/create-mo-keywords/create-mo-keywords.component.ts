import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";
import { MoKeywordsService } from "../manage-mo-keywords.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";

@Component({
  selector: "app-create-mo-keywords",
  templateUrl: "./create-mo-keywords.component.html",
  styleUrls: ["./create-mo-keywords.component.scss"]
})
export class CreateMoKeywordsComponent implements OnInit {

  moKeywordForm: FormGroup;
  formSubmitted: boolean = false;
  primaryDropdownList = [];
  secondaryDropdownList = [];
  dropdownSettings = {};
  departments = [];
  routes = [];

  constructor( private fb: FormBuilder, private spinner: NgxSpinnerService,
    private moKeywordsService: MoKeywordsService, private commonService: CommonService) {  }

  ngOnInit() {    
  
    this.dropdownSettings = {
      singleSelection: true,
      idField: 'id',
      textField: 'name',
      selectAllText: 'Select All',
      placeholder: 'Select Filter',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 10,
      allowSearchFilter: true,
      enableCheckAll: false
    };   

    this.getRouteList();
    this.getDepartmentList();
    this.initialiseForms();
  }

  initialiseForms() {
    this.moKeywordForm = this.fb.group({
      caseSensitivityFlag: ['', [Validators.required]],
      code: ['', [Validators.required]],
      keywords: ['', [Validators.required]],
      primaryRouteId: ['', [Validators.required]],
      secondaryRouteId: [''],
      department: ['', [Validators.required]],
      status: [''],
      url: [''],
      retryRequired: [''],
      retryCount: [''],
    });
  }

  resetFrom(){
    this.moKeywordForm.controls['caseSensitivityFlag'].setValue('');
    this.moKeywordForm.controls['code'].setValue('');
    this.moKeywordForm.controls['keywords'].setValue('');
    this.moKeywordForm.controls['primaryRouteId'].setValue('');
    this.moKeywordForm.controls['secondaryRouteId'].setValue('');
    this.moKeywordForm.controls['department'].setValue('');
  }

  getRouteList() {
    this.spinner.show();
    this.moKeywordsService.getRouteList().subscribe(
      res => {
        this.spinner.hide();        
        if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
          this.primaryDropdownList = res['data'];
          this.secondaryDropdownList = JSON.parse(JSON.stringify(this.primaryDropdownList));
        }else{
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.primaryDropdownList = [];
          this.secondaryDropdownList = [];
        }     
      });
  }

  getDepartmentList() {
    this.spinner.show();
    this.moKeywordsService.getDepartmentList().subscribe(
      res => {
        this.spinner.hide();
        if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
          this.departments = res['data'];
        }else{
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.departments = [];
        }
      });
  }

  submitForm() {
    this.formSubmitted = true;    
    if (this.moKeywordForm.valid) {
      let req = {
        "caseSensitivityFlag": this.moKeywordForm.controls['caseSensitivityFlag'].value,
        "code": this.moKeywordForm.controls['code'].value,       
        "keyword": this.moKeywordForm.controls['keywords'].value,
        "loginId": this.commonService.getUser(),
        "primaryRouteId": this.moKeywordForm.controls['primaryRouteId'].value[0]['id'],
        "secondaryRouteId": this.moKeywordForm.controls['secondaryRouteId'].value[0]['id'],
        "department": this.moKeywordForm.controls['department'].value
      };

      this.spinner.show();
      this.moKeywordsService.saveMoKeyword(req).subscribe(
        res => {
          this.spinner.hide();
          if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
            this.commonService.showSuccessToast('create');            
            this.initialiseForms(); 
          }else{
            this.commonService.showErrorToast(res['result']['userMsg']);          
          }
          this.formSubmitted = false;         
        });
    }
  }
  onItemSelect(event) {}
  OnItemDeSelect(event) {}
}
