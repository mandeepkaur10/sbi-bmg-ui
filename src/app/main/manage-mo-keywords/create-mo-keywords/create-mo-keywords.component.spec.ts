import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMoKeywordsComponent } from './create-mo-keywords.component';

describe('CreateMoKeywordsComponent', () => {
  let component: CreateMoKeywordsComponent;
  let fixture: ComponentFixture<CreateMoKeywordsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMoKeywordsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMoKeywordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
