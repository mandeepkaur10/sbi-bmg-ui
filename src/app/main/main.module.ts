import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MainComponent } from "./main.component";
import { SidebarComponent } from "./sidebar/sidebar.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { routing } from "./main.routing";
import { PerfectScrollbarModule } from "ngx-perfect-scrollbar";
import { SharedModule } from "../common/shared.modules";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  imports: [SharedModule, CommonModule, PerfectScrollbarModule, routing],
  declarations: [
    MainComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    ChangePasswordComponent,
    PageNotFoundComponent
  ]
})
export class MainModule {}
