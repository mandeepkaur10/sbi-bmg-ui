import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main.component";
import { ModuleWithProviders } from "@angular/core";
import { AuthGuardService, ModuleGuardService } from "../guards/auth.guard";
import { ChangePasswordComponent } from "./change-password/change-password.component";
import { MODULE } from "../common/common.const";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";

export const routes: Routes = [
  {
    path: "main",
    component: MainComponent,
    children: [
      { path: "", redirectTo: "/main/dashboard", pathMatch: "full" },
      {
        path: "dashboard",
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        data: { moduleName: MODULE.DASHBOARD, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService]
      },
      {
        path: "manage-user",
        loadChildren: "./manage-user/manage-user.module#ManageUserModule",
        data: { moduleName: MODULE.USER, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "registration-requests",
        loadChildren: "./registration-requests/registration-requests.module#RegistrationRequestsModule",
        data: { moduleName: MODULE.REGISTRATION_REQUEST, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "manage-sms-template",
        loadChildren:
          "./manage-sms-template/manage-sms-template.module#ManageSmsTemplateModule",
        data: { moduleName: MODULE.SMS_TEMPLATE, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "manage-sender-id",
        loadChildren:
          "./manage-sender-id/manage-sender-id.module#ManageSenderIdModule",
        data: { moduleName: MODULE.SENDERID, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "manage-spam-keyword",
        loadChildren:
          "./manage-spam-keyword/manage-spam-keyword.module#ManageSpamKeywordModule",
        data: { moduleName: MODULE.SPAM_KEYWORD, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "manage-mo-keywords",
        loadChildren:
          "./manage-mo-keywords/manage-mo-keywords.module#ManageMoKeywordsModule",
        data: { moduleName: MODULE.MO_KEYWORD, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "smsc-tps",
        loadChildren: "./smsc-tps/smsc-tps.module#SmscTpsModule",
        canActivate: [AuthGuardService]
      },
      {
        path: "manage-campaign",
        loadChildren:"./manage-campaign/manage-campaign.module#ManageCampaignModule",
        data: { moduleName: MODULE.CAMPAIGN, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "reporting",
        loadChildren: "./reporting/reporting.module#ReportingModule",
        data: { moduleName: MODULE.REPORTS, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },      
      {
        path: "manage-route",
        loadChildren: "./manage-route/manage-route.module#ManageRouteModule",
        data: { moduleName: MODULE.ROUTES, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "change-password",
        component: ChangePasswordComponent,
        canActivate: [AuthGuardService]
      },
      {
        path: "manage-aggregator",
        loadChildren:
          "./manage-aggregator/manage-aggregator.module#ManageAggregatorModule",
        data: { moduleName: MODULE.AGGREGATOR, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "manage-address-book",
        loadChildren:
          "./manage-address-book/manage-address-book.module#ManageAddressBookModule",
        data: { moduleName: MODULE.ADDRESS_BOOK, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      // {
      //   path: "**",
      //   component: PageNotFoundComponent
      //   // data: { moduleName: MODULE.ADDRESS_BOOK, permissions: ["R", "RW"] },
      //   // canActivate: [AuthGuardService, ModuleGuardService]
      // },
      { path: "**", redirectTo: "/auth/login" }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
