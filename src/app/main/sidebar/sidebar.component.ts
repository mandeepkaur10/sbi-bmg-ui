import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../common/common.service";
import { MODULE } from "src/app/common/common.const";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"]
})
export class SidebarComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) {}

  ngOnInit() {}
}
