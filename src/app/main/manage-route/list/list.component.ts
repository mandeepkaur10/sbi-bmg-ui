import { Component, OnInit } from "@angular/core";
import {
  NgbModal,
  NgbModalRef,
  NgbModalOptions,
  ModalDismissReasons
} from "@ng-bootstrap/ng-bootstrap";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray
} from "@angular/forms";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { Router } from "@angular/router";

import { CommonService } from "../../../common/common.service";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE, MODULE } from "src/app/common/common.const";
import { ManageRouteService } from "../manage-route.service";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
  providers: [ManageRouteService]
})
export class ListComponent implements OnInit {
  MODULES = MODULE;
  ACTIVE_ACTIVE = 1;
  DOMESTIC = "D";
  INTERNATIONAL = "I";

  private modalRef: NgbModalRef;

  modalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;

  searchType: string = "D";

  rows = [];
  temp = [];
  columns = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number;
  p: number = 1;
  table: DatatableComponent;
  searchText: string = "";

  isEdit: boolean = false;
  bulkRouteUpdateForm: FormGroup;

  primaryAggregatorList: Array<object> = [];
  secondaryAggregatorList: Array<object> = [];

  domesticRoutesList: Array<object> = [];
  internationalRoutesList: Array<object> = [];

  viewRouteDetails: object;

  userNameSorted: boolean = false;
  topologyModeSorted: boolean = false;
  retryFlagSorted: boolean = false;
  retryRouteSorted: boolean = false;

  constructor(
    public commonService: CommonService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private router: Router,
    private manageRouteService: ManageRouteService
  ) {}

  ngOnInit() {
    this.getRoutesList();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  getRoutesList() {
    this.spinner.show();
    this.manageRouteService.getRoutesList().subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        let tmp = data["data"];
        let self = this;
        this.domesticRoutesList = tmp.filter(function(item) {
          return item["routeType"] == self.DOMESTIC;
        });

        this.internationalRoutesList = tmp.filter(function(item) {
          return item["routeType"] == self.INTERNATIONAL;
        });

        if (this.searchType == this.DOMESTIC)
          this.rows = this.temp = this.domesticRoutesList;
        else if (this.searchType == this.INTERNATIONAL)
          this.rows = this.temp = this.internationalRoutesList;
      } else {
        this.rows = this.temp = [];
        this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
      }
      this.spinner.hide();
    });
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {  
    this.rows = this.commonService.sortArray(isAssending, sortBy, this.rows, isNumber );    
  }

  searchTypeChanged() {
    this.spinner.show();

    if (this.searchType == this.DOMESTIC)
      this.rows = this.temp = this.domesticRoutesList;
    else if (this.searchType == this.INTERNATIONAL)
      this.rows = this.temp = this.internationalRoutesList;

    this.spinner.hide();
  }

  // applySearch() {
  //   let self = this;
  //   this.rows = this.temp.filter(function(item) {
  //     return JSON.stringify(Object.values(item))
  //       .toLowerCase()
  //       .includes(self.searchText.toLowerCase());
  //   });
  // }

  editMode() {
    this.isEdit = true;
    this.prepareEditForm();
    this.getAggregatorsList();
  }

  cancelEdit() {
    this.isEdit = false;
  }

  prepareEditForm() {
    this.bulkRouteUpdateForm = this.formBuilder.group({
      selectAll: false,
      routeItemsList: this.formBuilder.array([])
    });

    let routeItemsArray = <FormArray>(
      this.bulkRouteUpdateForm.controls.routeItemsList
    );

    for (let i = 0; i < this.temp.length; i++) {
      routeItemsArray.push(
        this.formBuilder.group({
          isSelectedRoute: [false],
          primaryRoute: [this.temp[i]["primaryRouteId"]["id"]],
          secondaryRoute: [this.temp[i]["secondaryRouteId"]["id"]],
          tps: [this.temp[i]["tps"]]
        })
      );
    }
  }

  getAggregatorsList() {
    this.spinner.show();
    this.manageRouteService.getAggregatorList().subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        let tmp = data["data"];
        let aggregatorList = tmp.filter(
          x => x["aggregatorType"] == this.searchType
        );
        this.primaryAggregatorList = aggregatorList;
        this.secondaryAggregatorList = aggregatorList;
      }else {
        this.primaryAggregatorList = [];
        this.secondaryAggregatorList = [];
        this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
      }
      this.spinner.hide();
    });
  }

  routeSelectionChanged(index, event) {
    let value = event.target.checked;
    let routeItemsArray = <FormArray>(
      this.bulkRouteUpdateForm.controls.routeItemsList
    );
    if (index != -1) {
      let routeItem = routeItemsArray.controls[index]["controls"];
      this.updateRouteSelectionValidation(index, routeItem, value);

      let selectedCount = 0;
      for (var i = 0; i < routeItemsArray.controls.length; i++) {
        if (routeItemsArray.controls[i]["controls"]["isSelectedRoute"].value)
          selectedCount++;
      }

      if (selectedCount == routeItemsArray.controls.length)
        this.bulkRouteUpdateForm.controls.selectAll.setValue(true);
      else this.bulkRouteUpdateForm.controls.selectAll.setValue(false);
    } else {
      for (var i = 0; i < routeItemsArray.controls.length; i++) {
        let routeItem = routeItemsArray.controls[i]["controls"];
        this.updateRouteSelectionValidation(i, routeItem, value);
      }
    }
  }

  updateRouteSelectionValidation(index, routeItem, value) {
    if (value) {
      routeItem["isSelectedRoute"].setValue(true);
      routeItem["primaryRoute"].setValidators([Validators.required]);
      if (this.temp[index]["topologyMode"] == this.ACTIVE_ACTIVE)
        routeItem["secondaryRoute"].setValidators([Validators.required]);
      routeItem["tps"].setValidators([
        Validators.required,
        Validators.pattern(this.commonService.patterns.numberOnly)
      ]);
    } else {
      routeItem["isSelectedRoute"].setValue(false);
      routeItem["primaryRoute"].clearValidators();
      routeItem["secondaryRoute"].clearValidators();
      routeItem["tps"].clearValidators();
    }
    routeItem["primaryRoute"].updateValueAndValidity();
    routeItem["secondaryRoute"].updateValueAndValidity();
    routeItem["tps"].updateValueAndValidity();
  }

  aggregatorChanged(index, type) {
    let routeItemsArray = <FormArray>(
      this.bulkRouteUpdateForm.controls.routeItemsList
    );
    let routeItem = routeItemsArray.controls[index]["controls"];
    let primaryRoute = routeItem["primaryRoute"].value;
    let secondaryRoute = routeItem["secondaryRoute"].value;
    if (primaryRoute && secondaryRoute && primaryRoute == secondaryRoute) {
      routeItem["secondaryRoute"].setValue("");
      this.commonService.showInfoAlertMessage(
        "Primary route and secondary route can't be same"
      );
    }
  }

  viewRouteInfo(routeInfo, content) {
    // btn &&
    //   btn.parentElement &&
    //   btn.parentElement.parentElement &&
    //   btn.parentElement.parentElement.blur();

    this.viewRouteDetails = routeInfo;

    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  updateRotuesInfo() {
    if (this.bulkRouteUpdateForm.valid) {
      this.spinner.show();
      let finalList = [];
      let routeItemsArray = <FormArray>(
        this.bulkRouteUpdateForm.controls.routeItemsList
      );
      for (let x = 0; x < routeItemsArray.length; x++) {
        let routeItem = routeItemsArray.controls[x]["controls"];
        if (routeItem["isSelectedRoute"].value) {
          let obj = {
            primaryRouteId: routeItem["primaryRoute"].value,
            routeId: this.temp[x]["id"],
            secondaryRouteId:
              routeItem["secondaryRoute"].value != ""
                ? routeItem["secondaryRoute"].value
                : null,
            tps: routeItem["tps"].value,
            userId: this.temp[x]["user"]
          };
          finalList.push(obj);
        }
      }

      if (finalList.length > 0) {
        this.manageRouteService.updateRouteBulk(finalList).subscribe(
          data => {
            this.spinner.hide();
            if (
              data &&
              data["result"] &&
              data["result"]["statusDesc"] &&
              data["result"]["statusDesc"] == RESPONSE.FAILURE
            ) {
              this.commonService.showErrorAlertMessage(
                data["result"]["userMsg"]
              );
            } else {
              this.commonService.showSuccessAlertMessage(
                "Route details updated successfully"
              );
              this.isEdit = false;
              this.getRoutesList();
            }
          },
          error => {
            this.spinner.hide();
            this.commonService.showErrorAlertMessage(
              error["error"]["result"]["userMsg"]
            );
          }
        );
      } else {
        this.commonService.showInfoAlertMessage(
          "Please select atleast one row."
        );
      }
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
