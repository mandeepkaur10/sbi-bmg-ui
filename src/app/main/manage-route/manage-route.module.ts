import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ManageRouteComponent } from "./manage-route.component";
import { AddComponent } from "./add/add.component";
import { ListComponent } from "./list/list.component";
import { SharedModule } from "../../common/shared.modules";
import { routing } from "./manage-route.routing";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  imports: [CommonModule, SharedModule, routing, NgbModule],
  declarations: [ManageRouteComponent, AddComponent, ListComponent]
})
export class ManageRouteModule {}
