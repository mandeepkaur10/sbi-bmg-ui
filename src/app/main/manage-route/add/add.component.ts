import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  FormArray
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageUserService } from "../../manage-user/manage-user.service";
import { ManageRouteService } from "../manage-route.service";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"],
  providers: [ManageRouteService, ManageUserService]
})
export class AddComponent implements OnInit {
  DOMESTIC: string = "domestic";
  INTERNATIONAL: string = "international";

  routeForm: FormGroup;
  isFormSubmitted: boolean = false;

  selectedUser: object = {};
  userRoute: object;
  userList: Array<object> = [];
  domesticAggregatorList: Array<object> = [];
  domesticPrimaryAggregatorList: Array<object> = [];
  domesticSecondaryAggregatorList: Array<object> = [];
  domesticRetryAggregatorList: Array<object> = [];

  internationalAggregatorList: Array<object> = [];
  internationalPrimaryAggregatorList: Array<object> = [];
  internationalSecondaryAggregatorList: Array<object> = [];
  internationalRetryAggregatorList: Array<object> = [];

  primaryTrafficPercentageList: Array<object> = [];
  topologyList: Array<object> = [];
  retryCountList: Array<object> = [];

  isUpdate: boolean = false;

  userIdQueryParam: any;

  defaultRouteDetails = {
    primaryRouteId: {
      id: ""
    },
    secondaryRouteId: {
      id: ""
    },
    retryRouteId: {
      id: ""
    },
    topologyMode: "",
    primaryTrafficPer: "",
    retryFlag: "N",
    maxRetryCount: "",
    retryDetails: []
  };
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private manageRouteService: ManageRouteService,
    private manageUserService: ManageUserService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params["userId"]) this.userIdQueryParam = params["userId"];
    });
    this.prepareRouteForm();
    this.getUserList();
    this.getStaticDropdownLists();
  }

  prepareRouteForm() {
    this.routeForm = this.formBuilder.group({
      userId: ["", [Validators.required]],
      domestic: this.formBuilder.group({
        topology: ["", [Validators.required]],
        primaryRoute: ["", [Validators.required]],
        secondaryRoute: [""],
        primaryTrafficPercentage: ["", [Validators.required]],
        retryFlag: [false],
        retryRoute: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        retryCount: new FormControl({ disabled: true, value: "" }, [
          Validators.required,
          Validators.pattern(this.commonService.patterns.numberOnly)
        ]),
        retryIntervalArray: this.formBuilder.array([])
      }),
      international: this.formBuilder.group({
        internationalRoute: [false],
        topology: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        primaryRoute: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        secondaryRoute: new FormControl({ disabled: true, value: "" }),
        primaryTrafficPercentage: new FormControl(
          { disabled: true, value: "" },
          [Validators.required]
        ),
        retryFlag: [false],
        retryRoute: new FormControl({ disabled: true, value: "" }, [
          Validators.required
        ]),
        retryCount: new FormControl({ disabled: true, value: "" }, [
          Validators.required,
          Validators.pattern(this.commonService.patterns.numberOnly)
        ]),
        retryIntervalArray: this.formBuilder.array([])
      })
    });
  }

  getStaticDropdownLists() {
    this.topologyList = this.manageRouteService.getTopologyList();
    this.primaryTrafficPercentageList = this.manageRouteService.getPrimaryTrafficPercentageList();
    this.retryCountList = this.manageRouteService.getRetryCountList();
  }

  getUserList() {
    this.spinner.show();
    this.manageUserService.getUsersList(1).subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        let tmpList = data["data"];
        this.userList = tmpList.filter(function(item) {
          return (
            item["status"] != 5 &&
            item["status"] != 7 &&
            (item["roleId"] == 1 || item["roleId"] == 4)
          );
        });
        if (this.userIdQueryParam) {
          this.routeForm.controls.userId.setValue(this.userIdQueryParam);
          this.userSelectionChanged();
        }
      } else {
        this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
      }
      this.spinner.hide();
    });
  }

  getUserRouteDetails() {
    this.spinner.show();
    this.isUpdate = false;
    this.manageRouteService
      .getRoute(this.selectedUser["userId"])
      .subscribe(data => {
        this.spinner.hide();
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          if (data["data"] && data["data"].length > 0) {
            this.isUpdate = true;
            let routeData = data["data"];
            if (routeData.length == 2) {
              for (let i = 0; i < routeData.length; i++) {
                if (routeData[i]["routeType"] == "D") {
                  this.updateFormDetailsRoutewise(this.DOMESTIC, routeData[i]);
                } else if (routeData[i]["routeType"] == "I") {
                  this.updateFormDetailsRoutewise(
                    this.INTERNATIONAL,
                    routeData[i]
                  );
                }
              }
            } else {
              this.updateFormDetailsRoutewise(this.DOMESTIC, routeData[0]);
            }
          } else {
            this.resetFormDetails();
          }
        }
      });
  }

  updateFormDetailsRoutewise(type, data) {
    if (type == this.INTERNATIONAL) {
      if (data["primaryRouteId"]["id"] != "")
        this.routeForm.controls[type]["controls"][
          "internationalRoute"
        ].setValue(true);
      else
        this.routeForm.controls[type]["controls"][
          "internationalRoute"
        ].setValue(false);
      this.flagChanged(this.INTERNATIONAL, "internationalRoute", [
        "topology",
        "primaryRoute",
        "secondaryRoute",
        "primaryTrafficPercentage"
      ]);
    }

    this.routeForm.controls[type]["controls"]["topology"].setValue(
      data["topologyMode"]
    );
    this.routeForm.controls[type]["controls"]["primaryRoute"].setValue(
      data["primaryRouteId"]["id"]
    );
    this.routeForm.controls[type]["controls"]["secondaryRoute"].setValue(
      data["secondaryRouteId"]["id"]
    );
    this.routeForm.controls[type]["controls"][
      "primaryTrafficPercentage"
    ].setValue(data["primaryTrafficPer"]);

    this.routeForm.controls[type]["controls"]["retryFlag"].setValue(
      data["retryFlag"] == "Y"
    );
    this.flagChanged(type, "retryFlag", ["retryRoute", "retryCount"]);
    this.routeForm.controls[type]["controls"]["retryRoute"].setValue(
      data["retryFlag"] == "Y" ? data["retryRouteId"]["id"] : ""
    );

    this.routeForm.controls[type]["controls"]["retryCount"].setValue(
      data["retryFlag"] == "Y" ? data["maxRetryCount"] : ""
    );

    let retryIntervalControls = <FormArray>(
      this.routeForm.controls[type]["controls"]["retryIntervalArray"]
    );

    for (let i = retryIntervalControls.length - 1; i >= 0; i--) {
      retryIntervalControls.removeAt(i);
    }

    if (data["retryFlag"] == "Y") {
      for (let i = 1; i <= data["retryDetails"].length; i++) {
        let retryIntervalObj = data["retryDetails"].find(
          x => x["retryCountNo"] == i
        );
        retryIntervalControls.push(
          this.formBuilder.group({
            retryInterval: [
              retryIntervalObj["retryInterval"],
              [
                Validators.required,
                Validators.pattern(this.commonService.patterns.numberOnly)
              ]
            ]
          })
        );
      }
    }
  }

  resetFormDetails() {
    this.isFormSubmitted = false;
    this.selectedUser = {};
    this.updateFormDetailsRoutewise(this.DOMESTIC, this.defaultRouteDetails);
    this.updateFormDetailsRoutewise(
      this.INTERNATIONAL,
      this.defaultRouteDetails
    );
  }

  getAggregatorsList() {
    this.spinner.show();
    this.manageRouteService.getAggregatorList().subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        let tmp = data["data"];
        this.domesticAggregatorList = tmp.filter(
          x => x["aggregatorType"] == "D"
        );
        this.domesticPrimaryAggregatorList = this.domesticAggregatorList;
        this.domesticSecondaryAggregatorList = this.domesticAggregatorList;
        this.domesticRetryAggregatorList = this.domesticAggregatorList;

        this.internationalAggregatorList = tmp.filter(
          x => x["aggregatorType"] == "I"
        );

        this.internationalPrimaryAggregatorList = this.internationalAggregatorList;
        this.internationalSecondaryAggregatorList = this.internationalAggregatorList;
        this.internationalRetryAggregatorList = this.internationalAggregatorList;
      }
      this.spinner.hide();
    });
  }

  saveRoute() {
    debugger
    this.isFormSubmitted = true;
    if (this.routeForm.valid) {
      let routeObj = this.routeForm.value;
      let finalArr = [];
      let domesticObj = this.getRouteDetailsFormInfo(
        this.DOMESTIC,
        routeObj,
        "D"
      );
      finalArr.push(domesticObj);

      if (routeObj[this.INTERNATIONAL]["internationalRoute"]) {
        let internationalObj = this.getRouteDetailsFormInfo(
          this.INTERNATIONAL,
          routeObj,
          "I"
        );
        finalArr.push(internationalObj);
      }

      let usrId = this.routeForm.controls.userId.value;
      this.spinner.show();
      if (this.isUpdate) {
        this.manageRouteService.updateRoute(usrId, finalArr).subscribe(
          data => {
            this.spinner.hide();
            if (data == null) {
              //if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
              this.commonService.showSuccessAlertMessage(
                "Route details updated successfully"
              );
              this.isUpdate = false;
              this.routeForm.controls.userId.setValue("");
              this.resetFormDetails();
            } else {
              this.commonService.showErrorAlertMessage(
                data["result"]["userMsg"]
              );
            }
          },
          error => {
            this.commonService.showErrorAlertMessage("Something went wrong");
          }
        );
      } else {
        this.manageRouteService.saveRoute(usrId, finalArr).subscribe(data => {
          this.spinner.hide();
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessAlertMessage(
              "Route details saved successfully"
            );
            this.routeForm.controls.userId.setValue("");
            this.resetFormDetails();
          } else {
            this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
          }
        });
      }
    }
  }

  getRouteDetailsFormInfo(type, routeObj, routeInputType) {
    let retryDetails = null;
    if (routeObj[type]["retryFlag"]) {
      retryDetails = [];
      for (let x = 0; x < routeObj[type]["retryIntervalArray"].length; x++) {
        retryDetails.push({
          retryInterval:
            routeObj[type]["retryIntervalArray"][x]["retryInterval"],
          retryCountNo: x + 1
        });
      }
    }

    let obj = {
      dlrTimeoutRetryInterval: 0,
      dlrTimeoutRetryRequried: "Y",
      maxRetryCount: routeObj[type]["retryFlag"]
        ? routeObj[type]["retryCount"]
        : null,
      primaryRouteId: routeObj[type]["primaryRoute"],
      primaryTrafficPer: routeObj[type]["primaryTrafficPercentage"],
      retryDetails: retryDetails,
      retryFlag: routeObj[type]["retryFlag"] ? "Y" : "N",
      retryRouteId: routeObj[type]["retryFlag"]
        ? routeObj[type]["retryRoute"]
        : null,
      routeType: routeInputType,
      secondaryRouteId: routeObj[type]["secondaryRoute"]
        ? routeObj[type]["secondaryRoute"]
        : null,
      topologyMode: routeObj[type]["topology"]
    };

    return obj;
  }

  userSelectionChanged() {
    let val = this.routeForm.controls.userId.value;
    this.isFormSubmitted = false;
    this.selectedUser = this.userList.find(x => x["userId"] == val);
    this.getAggregatorsList();
    this.getUserRouteDetails();
  }

  topologyChanged(type) {
    let value = this.routeForm.controls[type]["controls"]["topology"].value;
    if (value == 1) {
      this.routeForm.controls[type]["controls"][
        "secondaryRoute"
      ].clearValidators();
      this.routeForm.controls[type]["controls"]["secondaryRoute"].setValidators(
        [Validators.required]
      );
      this.routeForm.controls[type]["controls"][
        "secondaryRoute"
      ].updateValueAndValidity();
    } else {
      this.routeForm.controls[type]["controls"][
        "secondaryRoute"
      ].clearValidators();
      this.routeForm.controls[type]["controls"][
        "secondaryRoute"
      ].updateValueAndValidity();
    }
  }

  flagChanged(type, flagName, fields) {
    debugger
    if (this.routeForm.controls[type]["controls"][flagName].value) {
      for (let x = 0; x < fields.length; x++)
        this.routeForm.controls[type]["controls"][fields[x]].enable();
    } else {
      for (let x = 0; x < fields.length; x++)
        this.routeForm.controls[type]["controls"][fields[x]].disable();
    }
  }

  filterRouteList(list, value) {
    return list.filter(x => x["id"] != value);
  }

  routeChanged(field, type) {
    let primaryVal = this.routeForm.controls[type]["controls"]["primaryRoute"]
      .value;
    let secondaryVal = this.routeForm.controls[type]["controls"][
      "secondaryRoute"
    ].value;
    let retryVal = this.routeForm.controls[type]["controls"]["retryRoute"]
      .value;
    if (type == this.DOMESTIC) {
      if (field == "primaryRoute") {
        this.domesticSecondaryAggregatorList = this.filterRouteList(
          this.domesticAggregatorList,
          primaryVal
        );

        if (retryVal)
          this.domesticSecondaryAggregatorList = this.filterRouteList(
            this.domesticSecondaryAggregatorList,
            retryVal
          );

        this.domesticRetryAggregatorList = this.filterRouteList(
          this.domesticAggregatorList,
          primaryVal
        );

        if (secondaryVal)
          this.domesticRetryAggregatorList = this.filterRouteList(
            this.domesticRetryAggregatorList,
            secondaryVal
          );
      } else if (field == "secondaryRoute") {
        this.domesticPrimaryAggregatorList = this.filterRouteList(
          this.domesticAggregatorList,
          secondaryVal
        );

        this.domesticRetryAggregatorList = this.filterRouteList(
          this.domesticPrimaryAggregatorList,
          primaryVal
        );

        if (retryVal)
          this.domesticPrimaryAggregatorList = this.filterRouteList(
            this.domesticPrimaryAggregatorList,
            retryVal
          );
      } else if (field == "retryRoute") {
        this.domesticPrimaryAggregatorList = this.filterRouteList(
          this.domesticAggregatorList,
          retryVal
        );

        this.domesticSecondaryAggregatorList = this.filterRouteList(
          this.domesticPrimaryAggregatorList,
          primaryVal
        );

        if (secondaryVal) {
          this.domesticPrimaryAggregatorList = this.filterRouteList(
            this.domesticPrimaryAggregatorList,
            secondaryVal
          );
        }
      }
    } else {
      if (field == "primaryRoute") {
        this.internationalSecondaryAggregatorList = this.filterRouteList(
          this.internationalAggregatorList,
          primaryVal
        );

        if (retryVal)
          this.internationalSecondaryAggregatorList = this.filterRouteList(
            this.internationalSecondaryAggregatorList,
            retryVal
          );

        this.internationalRetryAggregatorList = this.filterRouteList(
          this.internationalAggregatorList,
          primaryVal
        );

        if (secondaryVal)
          this.internationalRetryAggregatorList = this.filterRouteList(
            this.internationalRetryAggregatorList,
            secondaryVal
          );
      } else if (field == "secondaryRoute") {
        this.internationalPrimaryAggregatorList = this.filterRouteList(
          this.internationalAggregatorList,
          secondaryVal
        );

        this.internationalRetryAggregatorList = this.filterRouteList(
          this.internationalPrimaryAggregatorList,
          primaryVal
        );

        if (retryVal)
          this.internationalPrimaryAggregatorList = this.filterRouteList(
            this.internationalPrimaryAggregatorList,
            retryVal
          );
      } else if (field == "retryRoute") {
        this.internationalPrimaryAggregatorList = this.filterRouteList(
          this.internationalAggregatorList,
          retryVal
        );

        this.internationalSecondaryAggregatorList = this.filterRouteList(
          this.internationalPrimaryAggregatorList,
          primaryVal
        );

        if (secondaryVal) {
          this.internationalPrimaryAggregatorList = this.filterRouteList(
            this.internationalPrimaryAggregatorList,
            secondaryVal
          );
        }
      }
    }
  }

  retryCountChanged(type) {
    let retryCount = this.routeForm.controls[type]["controls"]["retryCount"]
      .value;
    let retryIntervalControl = <FormArray>(
      this.routeForm.controls[type]["controls"]["retryIntervalArray"]
    );

    for (let x = retryIntervalControl.length - 1; x >= 0; x--) {
      retryIntervalControl.removeAt(x);
    }

    for (let x = 0; x < retryCount; x++) {
      retryIntervalControl.push(
        this.formBuilder.group({
          retryInterval: [
            "",
            [
              Validators.required,
              Validators.pattern(this.commonService.patterns.numberOnly)
            ]
          ]
        })
      );
    }
  }

  get domesticRouteForm() {
    let x = <FormGroup>this.routeForm.controls.domestic;
    return x.controls;
  }

  get internationalRouteForm() {
    let x = <FormGroup>this.routeForm.controls.international;
    return x.controls;
  }
}
