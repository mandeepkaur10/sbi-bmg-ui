import { Component, OnInit } from "@angular/core";
import { MODULE } from "src/app/common/common.const";
import { CommonService } from "src/app/common/common.service";

@Component({
  selector: "app-manage-route",
  templateUrl: "./manage-route.component.html",
  styleUrls: ["./manage-route.component.scss"]
})
export class ManageRouteComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) {}

  ngOnInit() {}
}
