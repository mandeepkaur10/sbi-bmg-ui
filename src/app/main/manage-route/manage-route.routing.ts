import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { AddComponent } from "./add/add.component";
import { ListComponent } from "./list/list.component";
import { ManageRouteComponent } from "./manage-route.component";
import { ModuleGuardService, AuthGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageRouteComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-route/list",
        pathMatch: "full"
      },
      {
        path: "list",
        component: ListComponent,
        data: { moduleName: MODULE.ROUTES, permissions: ["R", "RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "add",
        component: AddComponent,
        data: { moduleName: MODULE.ROUTES, permissions: ["RW"] },
        canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
