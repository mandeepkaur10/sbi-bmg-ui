import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";

@Injectable()
export class ManageRouteService {
  NO_HEADER = null;
  loginId: string;
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {
    this.loginId = "loginId=" + this._commonService.getUser();
  }

  saveRoute(userId, route) {
    return this._networkService.post(
      "route/info/" + userId + "?" + this.loginId,
      route,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getRoute(userId) {
    return this._networkService.get(
      "route/info/" + userId + "?" + this.loginId,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  updateRoute(userId, route) {
    return this._networkService.put(
      "route/" + userId + "?" + this.loginId,
      route,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  updateRouteBulk(data) {
    return this._networkService.put(
      "route?" + this.loginId,
      data,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  //   updateUserStatus(userId, obj) {
  //     return this._networkService.put(
  //       "user/" + userId + "/status/update",
  //       obj,
  //       this.NO_HEADER,
  //       this._commonService.bearer
  //     );
  //   }

  //   deleteUser(userId, loginUserId, remarks) {
  //     return this._networkService.delete(
  //       "user/" + userId + "?loginId=" + loginUserId + "&remarks=" + remarks,
  //       this.NO_HEADER,
  //       this._commonService.bearer
  //     );
  //   }

  getRoutesList() {
    return this._networkService.get(
      "route/info?epoch=" + this._commonService.getTimestamp(),
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getUsersDropdownList() {
    return this._networkService.get(
      "user/list/",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getAggregatorList() {
    return this._networkService.get(
      "agg?epoch=0",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getStatusList() {
    return this._networkService.get(
      "user/status/list",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getTopologyList() {
    return [
      { id: 1, name: "Active/Active" },
      { id: 2, name: "Active/Passive" }
    ];
  }

  getRouteTypeList() {
    return [{ id: "D", name: "Domestic" }, { id: "I", name: "International" }];
  }

  // getPrimaryTrafficPercentageList() {
  //   return [
  //     { id: "0-10", name: "0-10" },
  //     { id: "11-20", name: "11-20" },
  //     { id: "21-30", name: "21-30" },
  //     { id: "31-40", name: "31-40" },
  //     { id: "41-50", name: "41-50" },
  //     { id: "51-60", name: "51-60" },
  //     { id: "61-70", name: "61-70" },
  //     { id: "71-80", name: "71-80" },
  //     { id: "81-90", name: "81-90" },
  //     { id: "91-100", name: "91-100" }
  //   ];
  // }

  getPrimaryTrafficPercentageList() {
    return [
      { id: "10", name: "10" },
      { id: "20", name: "20" },
      { id: "30", name: "30" },
      { id: "40", name: "40" },
      { id: "50", name: "50" },
      { id: "60", name: "60" },
      { id: "70", name: "70" },
      { id: "80", name: "80" },
      { id: "90", name: "90" },
      { id: "100", name: "100" }
    ];
  }

  getRetryCountList() {
    return [{ id: 1, name: 1 }, { id: 2, name: 2 }, { id: 3, name: 3 }];
  }
}
