import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageAddressBookComponent } from './manage-address-book.component';
import { ViewAddressBookComponent } from './view-address-book/view-address-book.component';
import { CreateAddressBookComponent } from './create-address-book/create-address-book.component';
import { routing } from "./manage-address-book.routing";
import { AddressBookService } from "./manage-address-book.service";
import { SharedModule } from "../../common/shared.modules";

@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule
  ],
  declarations: [ManageAddressBookComponent, ViewAddressBookComponent, CreateAddressBookComponent],
  providers: [AddressBookService]
})
export class ManageAddressBookModule { }
