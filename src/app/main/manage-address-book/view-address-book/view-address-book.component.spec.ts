import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAddressBookComponent } from './view-address-book.component';

describe('ViewAddressBookComponent', () => {
  let component: ViewAddressBookComponent;
  let fixture: ComponentFixture<ViewAddressBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewAddressBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAddressBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
