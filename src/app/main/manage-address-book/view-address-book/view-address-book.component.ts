import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { AddressBookService } from '../manage-address-book.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { MODULE } from "../../../common/common.const";

@Component({
  selector: 'app-view-address-book',
  templateUrl: './view-address-book.component.html',
  styleUrls: ['./view-address-book.component.scss']
})
export class ViewAddressBookComponent implements OnInit {
  MODULES = MODULE;
  pageLimit: number;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  searchText: string = "";
  addressBookList = [];
  p: number = 1;

  formSubmitted: boolean = false;
  addressBookName: string = "";
  fileUploaded: boolean = false;
  selectedFileData: any;
  selectedRecord: any;
  uploadedFileId: any;
  modalRef: any;


  constructor(private modalService: NgbModal, private addressBookService: AddressBookService,
    private spinner: NgxSpinnerService, public commonService: CommonService) { }

  ngOnInit() {
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.getAddressBookList();
  }

  getAddressBookList() {
    let userId = this.commonService.getUser();
    this.spinner.show();
    this.addressBookService.getAddressBookList(userId).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusCode'] == 200) {
          this.addressBookList = res["data"];
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.addressBookList = [];
        }
      });
  }

  confirmDeleteRecord(record: any) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to delete this record?';
    modalRef.componentInstance.leftButton = 'No';
    modalRef.componentInstance.rightButton = 'Yes';
    modalRef.result.then(
      (data: any) => {
        if (data == "yes") {
          this.deleteRecord(record);
        }
      },
      (reason: any) => {

      });
  }

  deleteRecord(record: any) {
    let req = {
      "addressBookId": record["id"],
      "loginId": this.commonService.getUser(),
      "remarks": "delete address book"
    };
    this.spinner.show();
    this.addressBookService.deleteAddressBook(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusCode'] == 200) {
          this.commonService.showSuccessToast("deleteAddressBook");
          this.getAddressBookList();
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  updateRecord(updatemodal: any, record: any) {
    this.selectedRecord = record;
    this.addressBookName = this.selectedRecord['name'];
    this.formSubmitted = false;
    this.fileUploaded = false;
    this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
    this.modalRef.result.then((result) => {

    }, (reason) => {

    });
  }

  onFileSelect(files) {
    let self = this;
    const file = files[0];
    if (file) {
      const filemb = file.size / (1024 * 1024);
      const fileExt = file.name.split(".").pop();
      if (fileExt.toLowerCase() == "csv") {
        let fromDataReq = new FormData();
        fromDataReq.append("fileData", files[0]);
        fromDataReq.append("module", "1");
        fromDataReq.append("userId", this.commonService.getUser());
        fromDataReq.append("username ", this.commonService.getUserName());
        this.spinner.show();
        this.addressBookService.uploadFile(fromDataReq).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
              this.uploadedFileId = res['data'];
              this.fileUploaded = true;
              this.commonService.showSuccessToast("File Uploaded");
            } else {
              this.fileUploaded = false;
              this.commonService.showErrorToast(res['result']['userMsg']);
            }
          },
          err => {
            this.fileUploaded = false;
          });
      } else {
        this.commonService.showErrorToast("Invalid file");
        this.fileUploaded = false;
      }
    }
  }

  updateAddressBook() {
    this.formSubmitted = true;
    if (this.addressBookName.trim() !== "" && this.fileUploaded) {
      let req = {
        "fileId": this.fileUploaded ? this.uploadedFileId : null,
        "name": this.addressBookName.trim(),
        "addressBookId": this.selectedRecord['id'],
        "userId": this.commonService.getUser()
      };
      this.spinner.show();
      this.addressBookService.updateAddressBook(req).subscribe(
        res => {
          this.spinner.hide();
          this.commonService.showSuccessToast('updateAddressBook');
          this.formSubmitted = false;
          this.fileUploaded = false;
          this.modalRef.close();
          this.getAddressBookList();
        });
    }
  }

  downloadRecordDetails(record: any) {
    let req = {
      "addressBookId": record["id"],
      "type": 2,
      "userId": this.commonService.getUser()
    };
    this.spinner.show();
    this.addressBookService.downloadFile(req).subscribe(
      res => {
        this.spinner.hide();
        if (res) {
          this.commonService.downloadFile(res, record["name"], "application/csv", ".csv")
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }
}
