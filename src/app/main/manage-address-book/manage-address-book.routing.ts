import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ManageAddressBookComponent } from "./manage-address-book.component";
import { ViewAddressBookComponent } from "./view-address-book/view-address-book.component";
import { CreateAddressBookComponent } from "./create-address-book/create-address-book.component";
import { AuthGuardService, ModuleGuardService } from "src/app/guards";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ManageAddressBookComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/manage-address-book/view-address-book",
        pathMatch: "full"
      },
      {
        path: "view-address-book",
        component: ViewAddressBookComponent,
        data: { moduleName: MODULE.ADDRESS_BOOK, permissions: ["RW"] },
        // canActivate: [AuthGuardService, ModuleGuardService]
      },
      {
        path: "create-address-book",
        component: CreateAddressBookComponent,
        data: { moduleName: MODULE.ADDRESS_BOOK, permissions: ["R", "RW"] },
        // canActivate: [AuthGuardService, ModuleGuardService]
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
