
import { Injectable } from '@angular/core';
import { NetworkService } from '../../common/network.service';

@Injectable()
export class AddressBookService {

  constructor(private _networkService: NetworkService) {}    

  getAddressBookList(id: any) {
    return this._networkService.get('addressBook?userId=' + id, null, 'bearer');
  }

  deleteAddressBook(req: any) {
    return this._networkService.delete('addressBook/'+ req['addressBookId'] +'?loginId=' + req['loginId'] + '&remarks=' + req['remarks'], null, 'bearer');
  }

  createAddressBook(req: any) {  
    return this._networkService.post('addressBook?userId='  + req["userId"], req, null, 'bearer');
  }

  updateAddressBook(req: any) {  
    return this._networkService.put('addressBook/' + req['addressBookId'] + '?userId='  + req["userId"], req, null, 'bearer');
  }

  downloadFile(req: any){   
    return this._networkService.getFile('addressBook/'+ req['addressBookId'] +'/download/' + req['type'] + '?userId=' + req['userId'], null, 'bearer');
  }

  uploadFile(req: any){
    return this._networkService.uploadFile('master/fileUpload', req, null, 'bearer');
  }

}

