import { Component, OnInit } from '@angular/core';
import { MODULE } from "../../common/common.const";
import { CommonService } from "../../common/common.service";

@Component({
  selector: 'app-manage-address-book',
  templateUrl: './manage-address-book.component.html',
  styleUrls: ['./manage-address-book.component.scss']
})
export class ManageAddressBookComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) {}

  ngOnInit() {
  }

}
