import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReportingService } from '../reporting.service';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";

@Component({
  selector: 'app-archived-report',
  templateUrl: './archived-report.component.html',
  styleUrls: ['./archived-report.component.scss']
})
export class ArchivedReportComponent implements OnInit {

  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  reportList = [];
  searchText:string = "";
  pageLimit: number;
  p: number = 1;
  groupBy: string;
  dateSorted: boolean = false;
  departmentSorted: boolean = false;
  shortLongCodeSorted: boolean = false;
  keywordSorted: boolean = false;
  totalCountSorted: boolean = false;
  successCountSorted: boolean = false;
  failedCountSorted: boolean = false;

  constructor( private fb: FormBuilder, private spinner: NgxSpinnerService, private reportingService: ReportingService,
    private excelService: ExcelService, public commonService: CommonService ) { }

  ngOnInit() {
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
  }

  initializeForm(){
    this.reportingForm = this.fb.group({
      keyword: [''],
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
      groupBy: ['date']   
    });
    this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
  }

  submitForm(){
    this.formSubmitted = true;
    if(this.reportingForm.valid){
      let req = {
        keyword: this.reportingForm.controls['keyword'].value,
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        // groupBy: this.reportingForm.controls['groupBy'].value 
        groupBy: this.commonService.getGroupBy("mo")
      };

      this.spinner.show();
      this.reportingService.getMOReports(req).subscribe(
        res => {
          this.spinner.hide();  
          this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);         
          if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
            this.reportList = res['data'];
          }else{            
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }        
        });
    }
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {  
    this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber );    
  }

  exportAs(fileType: string){
    let data = [];   
    this.reportList.forEach(element => {
      let excelDataObject = {};
      // excelDataObject[this.groupBy] = this.groupBy == "Date" ? new Date(element.date).toLocaleDateString() : element[this.reportingForm.controls['groupBy'].value];
      excelDataObject["Date"] = new Date(element.date).toLocaleDateString();
      excelDataObject["Department"] = element.department;    
      excelDataObject["Keyword"] = element.keyword;
      excelDataObject["Short/Long Code"] = element.shortLongCode;
      excelDataObject["Total Count"] = element.countTotal;
      excelDataObject["Success Count"] = element.countSuccess;       
      excelDataObject["Failed Count"] = element.countFailed;     
      data.push(excelDataObject);
    });
    switch(fileType){
      case 'excel':
      this.excelService.exportAsExcelFile(data, 'MO-Report');
      break;
      case 'csv':
      this.excelService.exportAsCsvFile(data, 'MO-Report');
      break;
      default:
      this.excelService.exportAsCsvFile(data, 'MO-Report');
      break;
    }
  }

}
