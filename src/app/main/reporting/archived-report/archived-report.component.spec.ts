import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedReportComponent } from './archived-report.component';

describe('ArchivedReportComponent', () => {
  let component: ArchivedReportComponent;
  let fixture: ComponentFixture<ArchivedReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivedReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
