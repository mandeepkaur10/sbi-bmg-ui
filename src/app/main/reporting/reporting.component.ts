import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../common/common.service';
import { MODULE } from "../../common/common.const";

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.scss']
})
export class ReportingComponent implements OnInit {

  MODULES = MODULE;

  constructor( public commonService: CommonService ) { }

  ngOnInit() {
  }

}
