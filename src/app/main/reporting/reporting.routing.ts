import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ReportingComponent } from "./reporting.component";
import { SummaryReportComponent } from "./summary-report/summary-report.component";
import { DetailedReportComponent } from "./detailed-report/detailed-report.component";
import { ArchivedReportComponent } from "./archived-report/archived-report.component";
import { MODULE } from "src/app/common/common.const";

export const routes: Routes = [
  {
    path: "",
    component: ReportingComponent,
    children: [
      {
        path: "",
        redirectTo: "/main/reporting/summary-report",
        pathMatch: "full"
      },
      {
        path: "summary-report",
        component: SummaryReportComponent,
        data: { moduleName: MODULE.REPORTS, permissions: ["R", "RW"] }
      },
      {
        path: "detailed-report",
        component: DetailedReportComponent,
        data: { moduleName: MODULE.REPORTS, permissions: ["R", "RW"] }
      },
      {
        path: "archived-report",
        component: ArchivedReportComponent,
        data: { moduleName: MODULE.REPORTS, permissions: ["R", "RW"] }
      }
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);
