import { Injectable } from "@angular/core";
import { NetworkService } from '../../common/network.service';

@Injectable()
export class ReportingService{

    constructor( private _networkService: NetworkService ){}

    getAllChannel(){
        return this._networkService.get('master/channel', null, 'bearer');
    }

    getAllDepartments(){
        return this._networkService.get('master/department', null, 'bearer');
    }

    getAllReports(req: any){
        return this._networkService.get('summary/reports?aggregator='+ req['aggregator'] +
        '&campaignId='+ req['campaign'] +'&endDate='+ req['endDate'] + '&startDate=' + req['startDate'] +'&userId=' + req['userId'] + '&groupBy=' + req['groupBy'] + '&senderId='+ req['sender'] +'&username=' + req['username'], null, 'bearer');
    }

    getMOReports(req: any){
        return this._networkService.get('summary/reports/mo?endDate='+ req['endDate'] +
        '&groupBy='+ req['groupBy'] +'&keyword='+ req['keyword'] + '&startDate=' + req['startDate'], null, 'bearer');
    }

    getDetailReports(req: any){
        return this._networkService.getMG('report/'+ req['msdin'] +'?endDate='+ req['endDate'] +'&limit=10&startDate='+ req['startDate'], null, 'bearer');
    }
    getAggregatorList(){
        return this._networkService.get('agg?epoch=0', null, 'bearer');
    }

}