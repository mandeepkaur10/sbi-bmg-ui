import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReportingComponent} from './reporting.component';
import {SummaryReportComponent} from './summary-report/summary-report.component';
import {DetailedReportComponent} from './detailed-report/detailed-report.component';
import {ArchivedReportComponent} from './archived-report/archived-report.component';
import { routing } from './reporting.routing';
import { ReportingService } from './reporting.service';
import { SharedModule } from "../../common/shared.modules";


@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule
  ],
  declarations: [ReportingComponent, SummaryReportComponent, DetailedReportComponent, ArchivedReportComponent],
  providers: [ReportingService]
})
export class ReportingModule { }
