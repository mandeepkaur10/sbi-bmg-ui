import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { ReportingService } from "../reporting.service";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE, USER_ROLE } from "../../../common/common.const";

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary-report.component.html',
  styleUrls: ['./summary-report.component.scss']
})
export class SummaryReportComponent implements OnInit {

  USER_ROLES = USER_ROLE;
  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  searchText: string = "";
  channels = [];
  aggregators = [];
  departments = [];
  reportList = [];
  pageLimit: number;
  p: number = 1;
  groupBy: string;
  dateSorted: boolean = false;
  senderIdSorted: boolean = false;
  departmentSorted: boolean = false;
  aggregatorSorted: boolean = false;
  usernameSorted: boolean = false;
  channelSorted: boolean = false;
  campaignNameSorted: boolean = false;
  prioritySorted: boolean = false;
  campaignIdSorted: boolean = false;
  totalCountSorted: boolean = false;
  submittedCountSorted: boolean = false;
  deliveredCountSorted: boolean = false;
  countRetriedSorted: boolean = false;
  failedCountSorted: boolean = false;
  expiredCountSorted: boolean = false;
  submitFailCountSorted: boolean = false;
  blockedCountSorted: boolean = false;
  otherCountSorted: boolean = false;
  invalidCountSorted: boolean = false;
  dndCountSorted: boolean = false;
  // minDate: string;
  // maxDate: string;
  constructor(private spinner: NgxSpinnerService, private reportingService: ReportingService,
    private fb: FormBuilder, private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    this.getChannelList();
    this.getDepartmentList();
    // this.minDate = "05/20/2019";
    // this.maxDate = "05/28/2019";
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    if (this.commonService.getRoleId() != this.USER_ROLES.FUNCTIONAL && this.commonService.getRoleId() != this.USER_ROLES.HOD) {
      this.getAggregatorList();
    }
    this.submitForm();
  }

  getAggregatorList() {
    this.spinner.show();
    this.reportingService.getAggregatorList().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.aggregators = res['data'];
        } else {
          this.aggregators = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }
  getChannelList() {
    this.spinner.show();
    this.reportingService.getAllChannel().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.channels = res['data'];
        } else {
          this.channels = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  error: any = { isError: false, errorMessage: '' };

  getDepartmentList() {
    this.spinner.show();
    this.reportingService.getAllDepartments().subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.departments = res['data'];
        } else {
          this.departments = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  initializeForm() {
    this.reportingForm = this.fb.group({
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)],
      channel: [''],
      department: [''],
      aggregator: [''],
      campaign: [''],
      sender: [''],
      user: [''],
      groupBy: ['senderId', [Validators.required]]
    });
    this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.reportingForm.valid) {
      let req = {
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value,
        channel: this.reportingForm.controls['channel'].value,
        department: this.reportingForm.controls['department'].value,
        aggregator: this.reportingForm.controls['aggregator'].value,
        campaign: this.reportingForm.controls['campaign'].value,
        sender: this.reportingForm.controls['sender'].value,
        username: this.reportingForm.controls['user'].value,
        userId: this.commonService.getUser(),
        groupBy: this.commonService.getGroupBy('summary')
      };

      this.spinner.show();
      this.reportingService.getAllReports(req).subscribe(
        res => {
          this.spinner.hide();
          this.groupBy = this.commonService.titleCase(this.reportingForm.controls['groupBy'].value);
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.reportList = res['data'];
          } else {
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber);
  }

  exportAs(fileType: string) {
    let data = [];
    this.reportList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Submitted On"] = new Date(element.date).toLocaleDateString();
      excelDataObject["Channel"] = element['campaignId'] == 0 ? 'API' : 'Bulk';
      excelDataObject["Campaign ID"] = element.campaignId;
      excelDataObject["Campaign Name"] = element.campaignName;
      if (this.commonService.getRoleId() != this.USER_ROLES.FUNCTIONAL && this.commonService.getRoleId() != this.USER_ROLES.HOD) {
        excelDataObject["Department Name"] = element.department;
      }
      if (this.commonService.getRoleId() != this.USER_ROLES.FUNCTIONAL) {
        excelDataObject["Username"] = element.username;
      }
      if (this.commonService.getRoleId() != this.USER_ROLES.FUNCTIONAL && this.commonService.getRoleId() != this.USER_ROLES.HOD) {
        excelDataObject["Aggregator Name"] = element.aggregator;
      }
      excelDataObject["Sender CLI"] = element.senderId;
      excelDataObject["Priority"] = element.categoryId;
      excelDataObject["Total SMS"] = element.countTotal;
      excelDataObject["Successful SMS"] = element.countDelivered;
      excelDataObject["SMS Retried"] = element.countRetried;
      excelDataObject["Failed SMS"] = element.countSubmitfail;
      excelDataObject["Delivery Expired"] = element.countExpired;
      excelDataObject["Invalid SMS"] = element.countFailed;
      excelDataObject["DND Count"] = element.countDnd;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'Summary-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'Summary-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'Summary-Report');
        break;
    }
  }

}
