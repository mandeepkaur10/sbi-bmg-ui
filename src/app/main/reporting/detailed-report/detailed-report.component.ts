import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReportingService } from '../reporting.service';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";

@Component({
  selector: 'app-detailed-report',
  templateUrl: './detailed-report.component.html',
  styleUrls: ['./detailed-report.component.scss']
})
export class DetailedReportComponent implements OnInit {

  reportingForm: FormGroup;
  formSubmitted: boolean = false;
  reportList = [];
  searchText: string = "";
  pageLimit: number;
  p: number = 1;
  channelSorted: boolean = false;
  campaignIDSorted: boolean = false;
  campaignNameSorted: boolean = false;
  departmentNameSorted: boolean = false;
  departmentUserSorted: boolean = false;
  smsTypeSorted: boolean = false;
  smsContentSorted: boolean = false;
  senderIdSorted: boolean = false;
  prioritySorted: boolean = false;
  internationalSorted: boolean = false;
  scheduledOnSorted: boolean = false;
  submittedOnSorted: boolean = false;
  deliveredOnSorted: boolean = false;
  deliveryStatusSorted: boolean = false;

  constructor( private fb: FormBuilder, private spinner: NgxSpinnerService, private reportingService: ReportingService,
    private excelService: ExcelService, public commonService: CommonService ) { }

  ngOnInit() {
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    // this.submitForm();
  }

  initializeForm(){
    this.reportingForm = this.fb.group({
      msdin: ['', [Validators.required]],
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)]
    });
  }

  submitForm(){   
    this.formSubmitted = true;
    if(this.reportingForm.valid){
      let req = {
        msdin: this.reportingForm.controls['msdin'].value,
        startDate: this.reportingForm.controls['startDate'].value,
        endDate: this.reportingForm.controls['endDate'].value           
      };
      this.spinner.show();
      
      this.reportingService.getDetailReports(req).subscribe(
        res => {
          this.spinner.hide();         
          if(res['result']['statusDesc'] == RESPONSE.SUCCESS){
            this.reportList = res['data'];
          }else{
            this.reportList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }        
        });
    }
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {  
    this.reportList = this.commonService.sortArray(isAssending, sortBy, this.reportList, isNumber );    
  }

  exportAs(fileType: string){
    let data = [];   
    this.reportList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Channel"] = new Date(element.date).toLocaleDateString();
      excelDataObject["Campaign ID"] = element.countTotal;
      excelDataObject["Campaign Name"] = element.countSuccess;       
      excelDataObject["Failed Count"] = element.countFailed;     
      data.push(excelDataObject);
    });
    switch(fileType){
      case 'excel':
      this.excelService.exportAsExcelFile(data, 'Detailed-Report');
      break;
      case 'csv':
      this.excelService.exportAsCsvFile(data, 'Detailed-Report');
      break;
      default:
      this.excelService.exportAsCsvFile(data, 'Detailed-Report');
      break;
    }
  }

}
