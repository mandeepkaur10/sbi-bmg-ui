import { Component, OnInit, ViewChild } from "@angular/core";
import Stepper from "bs-stepper";
// import * as XLSX from 'ts-xlsx';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageCampaignService } from "../manage-campaign.service";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";
import { SenderIdService } from "../../manage-sender-id/manage-sender-id.service";
import * as Excel from "exceljs/dist/exceljs.min.js";
import * as Stream from 'readable-stream';
import { UpperCasePipe } from "@angular/common";

@Component({
  selector: 'app-schedule-campaign',
  templateUrl: './schedule-campaign.component.html',
  styleUrls: ['./schedule-campaign.component.scss'],
  providers: [SenderIdService]
})
export class ScheduleCampaignComponent implements OnInit {
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  modalRef: any;
  dynamicHeader: string[];
  smsTemplateForm: FormGroup;
  private stepper: Stepper;
  isPlaceholder: boolean;
  isAddPlaceholderBtn: boolean;
  isAddEnable: boolean;
  isbtnNeed: boolean;
  templateType: number;
  HTTP: number = 1;
  BULK: number = 2;
  FLAG_YES: string = "Y";
  currentStep: number = 1;
  isUniqueUsername: boolean = true;
  isUniqueEmailAddress: boolean = true;
  skipConfigurations: boolean = false;
  disableSubmit: boolean = false;
  mainForm: FormGroup;
  stepOne = { formSubmitted: false, formCompleted: false };
  stepTwo = { formSubmitted: false, formCompleted: false };

  topologyList: Array<object> = [];
  reportPrivileges: Array<object> = [];

  categoryList: Array<object> = [];
  channelList: Array<object> = [];
  topicList: Array<object> = [];
  approverList: Array<object> = [];
  roleList: Array<object> = [];
  departmentList: Array<object> = [];
  accessPermissionViewList: Array<object> = [];

  selectedRole: object = {};

  userId = -1;
  userInfo: object = {};
  senderIdList = [];
  spamKeywordList = [];
  dynamicHeaders = [];
  math = Math;
  addressBookList = [];
  selectedPredefinedTemlate: any;
  predefinedTemplats = [];
  createdData: any;
  isEditable: boolean;


  constructor(
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public commonService: CommonService,
    private manageCampaignService: ManageCampaignService,
    private spinner: NgxSpinnerService,
    private senderIdService: SenderIdService
  ) { }
  @ViewChild('fileInput') fileInput: any;
  ngOnInit() {
    // this.stepper = new Stepper(document.querySelector('#campaign-stepper'), {
    //   linear: true,
    //   animation: true
    // });
    this.isEditable = false;
    this.scheduleCampaignFormSetup();
    this.getSenderIdList();
    this.getSpamKeywords();
    this.dynamicHeaders = ["test01", "test02", "test03"];

    this.readHeaders();
  }

  getSenderIdList() {
    this.spinner.show();
    this.senderIdService.getSenderIdList(0).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.senderIdList = res["data"];
        } else {
          this.senderIdList = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  getSpamKeywords() {
    let req = {
      "userId": this.commonService.getUser()
    };
    this.spinner.show();
    this.manageCampaignService.getSpamKeywords(req).subscribe(
      res => {

        this.spinner.hide();
        if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
          this.spamKeywordList = res["data"];
        } else {
          this.spamKeywordList = [];
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });
  }

  getSelectedTemplate() {
    this.isEditable = true;
    this.modalRef.close();
    this.campaignInfoForm.content.setValue(this.selectedPredefinedTemlate['content']);
  }

  scheduleCampaignFormSetup() {
    this.createdData = undefined;
    this.stepOne = { formSubmitted: false, formCompleted: false };
    this.stepTwo = { formSubmitted: false, formCompleted: false };
    this.isbtnNeed = false;
    this.currentStep = 1;
    if (this.fileInput) {
      this.fileInput.nativeElement.value = '';
    }
    // this.stepper.reset();
    this.mainForm = this.formBuilder.group({
      campaignInfo: this.formBuilder.group(
        {
          campaignName: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(30), Validators.pattern(this.commonService.patterns.alphaNumericSpaceOnly)]],
          templateType: ["1", [Validators.required]],
          smsContentType: ["1", [Validators.required]],
          content: ["", [Validators.required]],
          saveFlag: [false],
          templateName: [""],
          senderCli: ["", [Validators.required]],
          sendVia: ["3"],
          duplicateCheck: ["Yes"],
          dynamicOption: [""],
          phone: [""],
          addressBook: [""],
          file: [null, [Validators.required]]
        }
      ),
      scheduleInfo: this.formBuilder.group({
        sendNow: [false],
        date: ["", [Validators.required]],
        time: ["", [Validators.required]]
      })
    });
  }

  get campaignInfoForm() {
    let x = <FormGroup>this.mainForm.controls.campaignInfo;
    return x.controls;
  }

  get scheduleInfoForm() {
    let x = <FormGroup>this.mainForm.controls.scheduleInfo;
    return x.controls;
  }
  saveFlagChanged(event) {
    if (event) {
      this.campaignInfoForm.templateName.setValidators(Validators.required);
      this.campaignInfoForm.templateName.updateValueAndValidity();
    } else {
      this.campaignInfoForm.templateName.clearValidators();
      this.campaignInfoForm.templateName.updateValueAndValidity();
    }
  }

  sendNowChanged(event) {
    if (event) {
      this.scheduleInfoForm.date.clearValidators();
      this.scheduleInfoForm.date.updateValueAndValidity();
      this.scheduleInfoForm.time.clearValidators();
      this.scheduleInfoForm.time.updateValueAndValidity();
    } else {
      this.scheduleInfoForm.date.setValidators(Validators.required);
      this.scheduleInfoForm.date.updateValueAndValidity();
      this.scheduleInfoForm.time.setValidators(Validators.required);
      this.scheduleInfoForm.time.updateValueAndValidity();
    }
  }

  showModal: boolean = false;
  toClearContent(event: any) {
    // if (event.target.value == "3") {
    this.selectedPredefinedTemlate = undefined;
    // }
    this.campaignInfoForm.content.setValue("");
    if (this.fileInput) {
      this.fileInput.nativeElement.value = '';
    }
    this.campaignInfoForm.sendVia.setValue("3");
    this.campaignInfoForm.file.setValidators(Validators.required);
    this.campaignInfoForm.file.updateValueAndValidity();
    this.campaignInfoForm.phone.clearValidators();
    this.campaignInfoForm.phone.updateValueAndValidity();
    this.campaignInfoForm.addressBook.clearValidators();
    this.campaignInfoForm.addressBook.updateValueAndValidity();

  }

  showPredefinedTemplates(updatemodal: any) {
    let req = {
      "userId": this.commonService.getUser()
    }
    this.spinner.show();
    this.manageCampaignService.getPredefinedTemplates(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
          this.predefinedTemplats = res['data'];
          if (this.predefinedTemplats && this.predefinedTemplats.length > 0) {
            this.campaignInfoForm.content.setValue("");
            this.modalRef = this.modalService.open(updatemodal, this.modelOptions);
            this.modalRef.result.then((result) => {
            }, (reason) => {
            });
          } else {
            this.commonService.showErrorToast("No record found!");
          }
        } else {
          this.selectedPredefinedTemlate = undefined;
          this.commonService.showErrorToast(res['result']['userMsg']);
          this.predefinedTemplats = [];
        }
      });
  }

  closePopup() {
    this.modalRef.close();
  }

  sendViaChanged(event: any) {
    // this.selectedPredefinedTemlate = undefined;
    let sendOption = this.campaignInfoForm.sendVia.value;
    this.campaignInfoForm.phone.clearValidators();
    this.campaignInfoForm.phone.updateValueAndValidity();
    this.campaignInfoForm.file.clearValidators();
    this.campaignInfoForm.file.updateValueAndValidity();
    this.campaignInfoForm.addressBook.clearValidators();
    this.campaignInfoForm.addressBook.updateValueAndValidity();
    switch (sendOption) {
      case "3":
        this.campaignInfoForm.file.setValidators(Validators.required);
        this.campaignInfoForm.file.updateValueAndValidity();
        break;
      case "1":
        this.campaignInfoForm.phone.setValidators([Validators.required, Validators.pattern(this.commonService.patterns.tenDigitCommaSeparatedNo)]);
        this.campaignInfoForm.phone.updateValueAndValidity();
        break;
      case "2":
        this.campaignInfoForm.addressBook.setValidators(Validators.required);
        this.campaignInfoForm.addressBook.updateValueAndValidity();
        this.spinner.show();
        this.manageCampaignService.getAddressBookList(this.commonService.getUser()).subscribe(
          res => {
            this.spinner.hide();
            if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
              this.addressBookList = res['data'];
            } else {
              this.commonService.showErrorToast(res['result']['userMsg']);
              this.addressBookList = [];
            }
          });
        break;
      default:
        this.campaignInfoForm.file.setValidators(Validators.required);
        this.campaignInfoForm.file.updateValueAndValidity();
        break;
    }

  }

  isSecondPage: boolean = false;
  next() {
    this.isSecondPage = false;
    if (this.currentStep == 1) {
      this.stepOne.formSubmitted = true;
      if (this.mainForm.controls.campaignInfo.valid && !this.checkSpamKeyword()) {
        if (this.campaignInfoForm.sendVia.value == "3") {
          let fromDataReq = new FormData();
          fromDataReq.append("fileData", this.campaignInfoForm.file.value);
          fromDataReq.append("module", "2");
          fromDataReq.append("userId", this.commonService.getUser());
          fromDataReq.append("username ", this.commonService.getUserName());
          this.spinner.show();
          this.manageCampaignService.uploadFile(fromDataReq).subscribe(
            res => {
              this.spinner.hide();
              if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
                this.submitCreateRequest(res['data']);
                this.isSecondPage = true;
              } else {
                this.commonService.showErrorToast(res['result']['userMsg']);
              }
            });
        } else {
          this.submitCreateRequest();
        }
      }
    } else if (this.currentStep == 2) {
      this.stepTwo.formSubmitted = true;
      if (this.mainForm.controls.scheduleInfo.valid) {
        let req = {
          "campaignId": this.createdData['id'],
          "scheduleTime": this.scheduleInfoForm.sendNow.value ? null : this.scheduleInfoForm.date.value + " " + this.scheduleInfoForm.time.value + ":00",
          "sendNow": this.scheduleInfoForm.sendNow.value ? 'Y' : 'N',
          "userId": this.commonService.getUser()
        };
        this.spinner.show();
        this.manageCampaignService.scheduleCampaign(req).subscribe(
          res => {
            this.spinner.hide();
            this.commonService.showSuccessToast('Campaign scheduled successfully');
            this.router.navigate(["/main/manage-campaign/view-schedule-campaign"]);
            this.mainForm.reset();
            this.scheduleCampaignFormSetup();
            this.stepOne.formCompleted = false;
          });
      }
    }
  }


  checkSpamKeyword() {
    let smscontent = this.campaignInfoForm.content.value;
    smscontent = smscontent.split(" ");
    smscontent = smscontent.map(function (value) {
      return value.toUpperCase();
    });
    let matchedKeywordsList = [];
    for (let i = 0; i < this.spamKeywordList.length; i++) {
      debugger;
      var keywordExist = smscontent.indexOf(this.spamKeywordList[i].keyword.toUpperCase());
      if (keywordExist != -1) {
        matchedKeywordsList.push(this.spamKeywordList[i].keyword);
      }
    }
    if(matchedKeywordsList.length > 0){
      this.commonService.showErrorToast("Keyword(s) " + matchedKeywordsList.join(",") + " are not allowed to be used in the SMS content.");
    }
    return matchedKeywordsList.length == 0 ? false : true;
  }

  submitCreateRequest(fileId?: any) {
    let req = {
      "campaignName": this.campaignInfoForm.campaignName.value,
      "templateType": this.campaignInfoForm.templateType.value == 3 ? this.selectedPredefinedTemlate.type : this.campaignInfoForm.templateType.value,
      "messageType": this.campaignInfoForm.smsContentType.value,
      "senderId": this.campaignInfoForm.senderCli.value,
      "campaignType": this.campaignInfoForm.sendVia.value,
      "fileId": fileId ? fileId : null,
      "mobileNumbers": this.campaignInfoForm.phone.value.split(","),
      "addressBookId": this.campaignInfoForm.addressBook.value,
      "messageText": this.campaignInfoForm.content.value,
      "saveTemplateFlag": this.campaignInfoForm.saveFlag.value ? 'Y' : 'N',
      "scheduleDate": this.scheduleInfoForm.date.value,
      "scheduleTime": this.scheduleInfoForm.time.value,
      "templateName": this.campaignInfoForm.templateName.value,
      "duplicateCheckFlag": this.campaignInfoForm.duplicateCheck.value ? 'Y' : 'N',
      "userId": this.commonService.getUser()
    };
    this.spinner.show();
    this.manageCampaignService.createCampaign(req).subscribe(
      res => {
        this.spinner.hide();
        if (res['result']['statusDesc'] === RESPONSE.SUCCESS) {
          this.createdData = res['data'];
          this.currentStep = 2;
          this.stepOne.formCompleted = true;
          // this.stepper.next();
        } else {
          this.commonService.showErrorToast(res['result']['userMsg']);
        }
      });

  }

  // get_header_row(sheet) {
  //
  //   var headers = [];
  //   var range = XLSX.utils.decode_range(sheet['!ref']);
  //   var C, R = range.s.r; /* start in the first row */
  //   /* walk every column in the range */
  //   for (C = range.s.c; C <= range.e.c; ++C) {
  //     var cell = sheet[XLSX.utils.encode_cell({ c: C, r: R })] /* find the cell in the first row */

  //     var hdr = "UNKNOWN " + C; // <-- replace with your desired default
  //     // if(cell && cell.t) hdr = XLSX.utils.format_cell(cell);

  //     headers.push(hdr);
  //   }
  //   return headers;
  // }

  readHeaders() {
    //
    //       let sheet:any;
    //     var workbook = new Excel.Workbook();
    // workbook.xlsx.readFile("assets/xlsx_large_personalized.xlsx")
    //     .then(function() {
    //
    //         var worksheet = workbook.getWorksheet('Employee');
    //         worksheet.eachRow({ includeEmpty: true }, function(row, rowNumber) {
    //           console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));

    //         });
    //     });
    // ;
    // var obj = xlsx.parse('assets/xlsx_large_personalized.xlsx'); // parses a file

    // var workBookReader = new XlsxStreamReader();
    // workBookReader.on('error', function (error) {
    //     throw(error);
    // });
    // workBookReader.on('sharedStrings', function () {
    //     // do not need to do anything with these,
    //     // cached and used when processing worksheets
    //     console.log(workBookReader.workBookSharedStrings);
    // });

    // workBookReader.on('styles', function () {
    //     // do not need to do anything with these
    //     // but not currently handled in any other way
    //     console.log(workBookReader.workBookStyles);
    // });

    // workBookReader.on('worksheet', function (workSheetReader) {
    //     if (workSheetReader.id > 1){
    //         // we only want first sheet
    //         workSheetReader.skip();
    //         return;
    //     }
    //     // print worksheet name
    //     console.log(workSheetReader.name);

    //     // if we do not listen for rows we will only get end event
    //     // and have infor about the sheet like row count
    //     workSheetReader.on('row', function (row) {
    //         if (row.attributes.r == 1){
    //             // do something with row 1 like save as column names
    //         }else{
    //             // second param to forEach colNum is very important as
    //             // null columns are not defined in the array, ie sparse array
    //             row.values.forEach(function(rowVal, colNum){
    //                 // do something with row values
    //             });
    //         }
    //     });
    //     workSheetReader.on('end', function () {
    //         console.log(workSheetReader.rowCount);
    //     });

    //     // call process after registering handlers
    //     workSheetReader.process();
    // });
    // workBookReader.on('end', function () {
    //     // end of workbook reached
    // });
    //
    // fs.createReadStream(fileName).pipe(workBookReader);

  }


  onFileSelect(files) {
    let self = this;
    if (files && files.length > 0) {
      const file: File = files.item(0);
      var fileExtension = file.name.split('.').pop();
      var reader = new FileReader();
      //FOR EXCEL FILE
      if (fileExtension.toLowerCase() == "xlsx" || fileExtension.toLowerCase() == "xls" || fileExtension.toLowerCase() == "csv") {
        self.campaignInfoForm.file.setValue(file);
        if (this.campaignInfoForm.templateType.value > 1) {
          let fromDataReq = new FormData();
          if (fileExtension.toLowerCase() == "xlsx" || fileExtension.toLowerCase() == "xls") {
            reader.onload = function (e) {
              let workbook = new Excel.Workbook();
              let stream = new Stream();

              // stream.push(e.target.result); // file is ArrayBuffer variable
              // stream.push(null);
              //
              workbook.xlsx.load(e.target.result).then((res) => {

                // get worksheet, read rows, etc
              });
              // workbook.xlsx.readFile(stream)
              //     .then((res)=> {
              //
              //         let worksheet = res;
              //         // worksheet.eachRow({ includeEmpty: true }, function(row, rowNumber) {
              //         //   console.log("Row " + rowNumber + " = " + JSON.stringify(row.values));

              //         // });
              //     });

            }
            reader.onerror = function (ex) {
              console.log(ex);
            };
            reader.readAsArrayBuffer(file);
          }
          //FOR OTHER FILE
          else {
            reader.onload = (progressEvent) => {
              // Entire file
              // console.log("whole data..."+reader.result);
              // By lines
              var lines: String = reader.result.split('\n').shift();
              var newStr = lines.replace('"', '');
              self.dynamicHeader = newStr.split(','); // split string on comma space
              if (self.dynamicHeader.length > 1) {
                self.isAddPlaceholderBtn = true;
                self.isbtnNeed = true;
              }
              this.validateHeaders();
            };
            reader.readAsText(file);
          }
        }
      }
    }
  }
  containsAllElements: boolean = true;
  validateHeaders() {
    if (this.campaignInfoForm.templateType.value == 3 && this.selectedPredefinedTemlate && this.selectedPredefinedTemlate.type == 2) {
      let self = this;
      if (this.campaignInfoForm.templateType.value == 3) {
        let str = this.campaignInfoForm.content.value;
        const headersInText = str.match(/<!(.*?)!>/g).map(val => {
          return val.replace(/<!|!>/g, '');
        });
        console.log(this.dynamicHeader);
        self.containsAllElements = headersInText.every(function (item) {
          return self.dynamicHeader.indexOf(item) > -1;
        });
        // self.containsAllElements=false;

      }
    }
  }

  valueOfHeader(value: string) {
    this.isAddEnable = true;
  }
  appendPlaceholder() {
    if (this.campaignInfoForm.dynamicOption.value != "") {
      let contentValue = this.campaignInfoForm.content.value;
      this.campaignInfoForm.content.setValue(contentValue + " <!" + this.campaignInfoForm.dynamicOption.value + "!> ");
      this.campaignInfoForm.dynamicOption.setValue("");
    }
  }
  addPlaceholder() {
    this.isPlaceholder = true;
  }
  previous() {
    // this.stepper.previous();
    this.currentStep = 1;
  }

  noOfMobileNo: string;
  lengthOfNo: number;
  arrayOfMobileNo = [];
  mobileString: any;
  checkMobileNo(mobileNo) {
    this.arrayOfMobileNo = [];
    // this.noOfMobileNo = mobileNo.split('\n').join(',');
    this.lengthOfNo = this.noOfMobileNo.split(',').length;
    this.mobileString = this.noOfMobileNo.split(",", this.lengthOfNo).toString();
  }
}
