import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewScheduledCampaignComponent } from './view-scheduled-campaign.component';

describe('ViewScheduledCampaignComponent', () => {
  let component: ViewScheduledCampaignComponent;
  let fixture: ComponentFixture<ViewScheduledCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewScheduledCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewScheduledCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
