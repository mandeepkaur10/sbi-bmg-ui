import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ManageCampaignService } from '../manage-campaign.service';
import { ExcelService } from "../../../common/excel.service";
import { CommonService } from "../../../common/common.service";
import { RESPONSE } from "../../../common/common.const";
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationComponent } from "../../../common/components/confirmation/confirmation.component";
import { MODULE } from "../../../common/common.const";

@Component({
  selector: 'app-pending-approval',
  templateUrl: './pending-approval.component.html',
  styleUrls: ['./pending-approval.component.scss']
})
export class PendingApprovalComponent implements OnInit {
  MODULES = MODULE;
  modelOptions: NgbModalOptions = {
    backdrop: 'static',
    keyboard: false
  };
  campaignForm: FormGroup;
  formSubmitted: boolean = false;
  campaignList = [];
  searchText: string = "";
  pageLimit: number;
  p: number = 1;
  campIdSorted: boolean = false;
  statusSorted: boolean = false;
  scheduleTimeSorted: boolean = false;
  campNameSorted: boolean = false;
  createdTimeSorted: boolean = false;
  messageTypeSorted: boolean = false;
  messageTextSorted: boolean = false;
  senderIdSorted: boolean = false;
  totalRecordCountSorted: boolean = false;
  successfulRecordCountSorted: boolean = false;
  failedRecordCountSorted: boolean = false;
  expiredRecordCountSorted: boolean = false;
  retriedRecordCountSorted: boolean = false;
  deliveryExpiredCountSorted: boolean = false;
  invalidRecordCountSorted: boolean = false;

  constructor(private modalService: NgbModal, private fb: FormBuilder, private spinner: NgxSpinnerService, private manageCampaignService: ManageCampaignService,
    private excelService: ExcelService, public commonService: CommonService) { }

  ngOnInit() {
    this.initializeForm();
    this.pageLimit = this.commonService.recordsPerPage[0];
    this.submitForm();
  }

  initializeForm() {
    this.campaignForm = this.fb.group({
      startDate: [new Date().toISOString().substr(0, 10)],
      endDate: [new Date().toISOString().substr(0, 10)]
    });
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.campaignForm.valid) {
      let req = {
        "startDate": this.campaignForm.controls['startDate'].value,
        "endDate": this.campaignForm.controls['endDate'].value,
        "type": this.commonService.getUserType(true),
        "userId": this.commonService.getUser()
      };
      this.spinner.show();

      this.manageCampaignService.getCampaignList(req).subscribe(
        res => {
          this.spinner.hide();
          if (res['result']['statusDesc'] == RESPONSE.SUCCESS) {
            this.campaignList = res['data'];
          } else {
            this.campaignList = [];
            this.commonService.showErrorToast(res['result']['userMsg']);
          }
        });
    }
  }

  toApprove(record: any) {

    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to approve the scheduled campaign?';
    modalRef.componentInstance.leftButton = 'Cancel';
    modalRef.componentInstance.rightButton = 'Approve';
    modalRef.result.then(
      (data: any) => {
        debugger
        if (data == "yes") {
          this.changeStatus(record, true);
        }
      },
      (reason: any) => {

      });
  }
  toReject(record: any) {
    let modalRef = this.modalService.open(ConfirmationComponent, this.modelOptions);
    modalRef.componentInstance.content = 'Are you sure you want to reject the scheduled campaign?';
    modalRef.componentInstance.leftButton = 'Cancel';
    modalRef.componentInstance.rightButton = 'Reject';
    modalRef.result.then(
      (data: any) => {
        debugger
        if (data == "yes") {
          this.changeStatus(record, false);
        }
      },
      (reason: any) => {

      });
  }

  changeStatus(record: any, forApprove: boolean) {
    let req = {
      "campaignId": record['id'],
      "userId": this.commonService.getUser(),
      "remarks": forApprove ? "Approve" : "Reject",
      "status": forApprove ? 1 : 2,
      "username": this.commonService.getUserName(),
    };
    this.spinner.show();
    this.manageCampaignService.updateSattus(req).subscribe(
      res => {
        this.spinner.hide();
        if (forApprove) {
          this.commonService.showSuccessToast("Campaign has been approved");
        } else {
          this.commonService.showSuccessToast("Campaign has been rejected");
        }
        this.submitForm();
      },
    err=>{
      debugger
      this.commonService.showErrorToast(err['error']['userMsg']);
    });
  }

  sortTable(sortBy: string, isAssending: boolean, isNumber?: boolean) {
    this.campaignList = this.commonService.sortArray(isAssending, sortBy, this.campaignList, isNumber);
  }

  exportAs(fileType: string) {
    let data = [];
    this.campaignList.forEach(element => {
      let excelDataObject = {};
      excelDataObject["Channel"] = new Date(element.date).toLocaleDateString();
      excelDataObject["Campaign ID"] = element.id;
      excelDataObject["Status"] = element.statusDesc;
      excelDataObject["Scheduled On"] = new Date(element.scheduleTime).toLocaleDateString();
      excelDataObject["Submittted On"] = new Date(element.createdOn).toLocaleDateString();
      excelDataObject["SMS Type"] = element.messageType;
      excelDataObject["SMS Content"] = element.messageText;
      excelDataObject["Sender CLI"] = element.senderId;
      excelDataObject["Total SMS"] = element.totalRecordCount;
      excelDataObject["Successful SMS"] = element.successfulRecordCount;
      excelDataObject["Failed SMS"] = element.failedRecordCount;
      excelDataObject["Expired SMS"] = element.expiredRecordCount;
      excelDataObject["SMS Re-Tried"] = element.retriedRecordCount;
      excelDataObject["Delivery Expired"] = element.deliveryExpiredCount;
      excelDataObject["Invalid SMS"] = element.invalidRecordCount;
      data.push(excelDataObject);
    });
    switch (fileType) {
      case 'excel':
        this.excelService.exportAsExcelFile(data, 'Detailed-Report');
        break;
      case 'csv':
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
      default:
        this.excelService.exportAsCsvFile(data, 'Detailed-Report');
        break;
    }
  }

}
