import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ManageCampaignComponent} from './manage-campaign.component';
import {ScheduleCampaignComponent} from './schedule-campaign/schedule-campaign.component';
import {ViewScheduledCampaignComponent} from './view-scheduled-campaign/view-scheduled-campaign.component';
import { routing } from './manage-campaign.routing';
import { SharedModule } from "../../common/shared.modules";
import { ManageCampaignService } from "../manage-campaign/manage-campaign.service";
import { PendingApprovalComponent } from './pending-approval/pending-approval.component';


@NgModule({
  imports: [
    CommonModule,
    routing,
    SharedModule
  ],
  declarations: [ManageCampaignComponent, ScheduleCampaignComponent, ViewScheduledCampaignComponent, PendingApprovalComponent],
  providers: [ManageCampaignService]
})
export class ManageCampaignModule { }
