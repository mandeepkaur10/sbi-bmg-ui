import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";

@Injectable()
export class ManageCampaignService {
  constructor(private _networkService: NetworkService) {}

    getCampaignList(req: any){
      return this._networkService.get('campaign?type=' + req["type"] +'&userId=' + req['userId'] +
       '&startDate='+req['startDate'] +'&endDate=' + req['endDate'], null, 'bearer');
    }

    createCampaign(req: any){
      return this._networkService.post("campaign?userId=" + req["userId"], req, null, 'bearer');
    }

    scheduleCampaign(req: any){
      return this._networkService.put('campaign/' + req['campaignId'] + '/schedule?userId='  +  req['userId'], req, null, 'bearer');
    }

    uploadFile(req: any){
      return this._networkService.uploadFile('master/fileUpload', req, null, 'bearer');
    }

    getAddressBookList(id: any) {
      return this._networkService.get('addressBook?userId=' + id, null, 'bearer');
    }

    getPredefinedTemplates(req: any){
      return this._networkService.get('campaign/' + req['userId']+ '/template', null, 'bearer');
    }

    deleteCampaign(req: any){
      return this._networkService.delete('campaign/' +req['campaignId'] + '?loginId='+  req['loginId']+'&remarks='+ req['remarks'], null, 'bearer');
    }


    cancelCampaign(req: any){
      return this._networkService.put('campaign/'+ req['campaignId'] +'/status/'+ req['campaignStatus'] +'?userId=' + req['userId'], null, null, 'bearer');
    }

    updateSattus(req: any){
      return this._networkService.put('campaign/'+ req['campaignId'] +'/apporve/?userId=' + req['userId'], req, null, 'bearer');
    }

    getSpamKeywords(req: any){
      return this._networkService.get('spamkeyword?userId='+ req['userId'], null, 'bearer');
    }


}
