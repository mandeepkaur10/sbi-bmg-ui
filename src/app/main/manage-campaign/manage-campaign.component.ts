import { Component, OnInit } from "@angular/core";
import { MODULE } from "src/app/common/common.const";
import { CommonService } from "../../common/common.service";

@Component({
  selector: "app-manage-campaign",
  templateUrl: "./manage-campaign.component.html",
  styleUrls: ["./manage-campaign.component.scss"]
})
export class ManageCampaignComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) {}

  ngOnInit() {}
}
