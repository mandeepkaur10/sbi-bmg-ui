import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {ViewSmscTpsComponent} from './view-smsc-tps/view-smsc-tps.component';
import {SmscTpsComponent} from './smsc-tps.component';


export const routes: Routes = [
  {
    path: '', component: SmscTpsComponent,
    children: [
      {path: '', redirectTo: '/main/smsc-tps/view-smsc-tps', pathMatch: 'full'},
      {
        path: 'view-smsc-tps',
        component: ViewSmscTpsComponent
      },
    ]
  }
];
export const routing: ModuleWithProviders = RouterModule.forChild(routes);

