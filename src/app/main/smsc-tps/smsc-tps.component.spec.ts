import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmscTpsComponent } from './smsc-tps.component';

describe('SmscTpsComponent', () => {
  let component: SmscTpsComponent;
  let fixture: ComponentFixture<SmscTpsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmscTpsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmscTpsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
