import { Injectable } from "@angular/core";
import { NetworkService } from "../../common/network.service";
import { CommonService } from "../../common/common.service";

@Injectable()
export class ManageAggregatorService {
  NO_HEADER = null;
  constructor(
    private _networkService: NetworkService,
    private _commonService: CommonService
  ) {}

  saveAggregator(data) {
    return this._networkService.post(
      "aggregator",
      data,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getAggregator(id) {
    return this._networkService.get(
      "aggregator/" + id,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  updateAggregator(id, aggregator) {
    return this._networkService.put(
      "aggregator/" + id,
      aggregator,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  updateAggregatorStatus(id, data) {
    return this._networkService.put(
      "aggregator/status/" + id,
      data,
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getAllAggregatoraList() {
    return this._networkService.get(
      "aggregator",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  getStatusList() {
    return this._networkService.get(
      "aggregator",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }

  deleteAggregator(id,remarks) {
    return this._networkService.get(
      "aggregator",
      this.NO_HEADER,
      this._commonService.bearer
    );
  }


  getTopologyList() {
    return [
      { id: 1, name: "Active/Active" },
      { id: 2, name: "Active/Passive" }
    ];
  }

  getAggregatorTypeList() {
    return [{ id: "D", name: "Domestic" }, { id: "I", name: "International" }];
  }

  getAggregatorProtocolList() {
    return [{ id: "HTTP", name: "HTTP" }, { id: "SMPP", name: "SMPP" }];
  }

}
