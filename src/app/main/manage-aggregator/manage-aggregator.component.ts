import { Component, OnInit } from '@angular/core';
import { MODULE } from 'src/app/common/common.const';
import { CommonService } from 'src/app/common/common.service';

@Component({
  selector: 'app-manage-aggregator',
  templateUrl: './manage-aggregator.component.html',
  styleUrls: ['./manage-aggregator.component.scss']
})
export class ManageAggregatorComponent implements OnInit {
  MODULES = MODULE;
  constructor(public commonService: CommonService) {}

  ngOnInit() {
  }

}
