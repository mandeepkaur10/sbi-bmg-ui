import { Component, OnInit } from "@angular/core";
import {
  NgbModal,
  NgbModalRef,
  NgbModalOptions,
  ModalDismissReasons
} from "@ng-bootstrap/ng-bootstrap";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { Router } from "@angular/router";

import { CommonService } from "../../../common/common.service";
import { ManageAggregatorService } from "../manage-aggregator.service";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";
@Component({
  selector: "app-view",
  templateUrl: "./view.component.html",
  styleUrls: ["./view.component.scss"],
  providers: [ManageAggregatorService]
})
export class ViewComponent implements OnInit {
  private modalRef: NgbModalRef;

  modalOptions: NgbModalOptions = {
    backdrop: "static",
    keyboard: false
  };
  closeResult: string;

  aggregatorDetails: object;

  statusFormSubmitted: boolean = false;
  deleteFormSubmitted: boolean = false;

  statusForm: FormGroup;
  deleteForm: FormGroup;
  statusList: Array<object> = [];

  rows = [];
  temp = [];
  columns = [];
  loadingIndicator: boolean = true;
  reorderable: boolean = true;
  pageLimit: number = 10;
  table: DatatableComponent;
  searchText: string = "";

  constructor(
    public commonService: CommonService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private manageAggregatorService: ManageAggregatorService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {}

  ngOnInit() {}

  getUsersList() {
    this.spinner.show();
    this.manageAggregatorService.getAllAggregatoraList().subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.rows = this.temp = data["data"];
      } else {
        this.rows = this.temp = [];
        this.commonService.showErrorToast(data["result"]["userMsg"]);
      }
      this.spinner.hide();
    });
  }

  applySearch() {
    let self = this;
    this.rows = this.temp.filter(function(item) {
      return JSON.stringify(Object.values(item))
        .toLowerCase()
        .includes(self.searchText.toLowerCase());
    });
  }
  editLink(id) {
    this.router.navigate(["/main/manage-aggregator/add?id=" + id]);
  }

  prepareUpdateStatusForm() {
    this.statusForm = this.formBuilder.group({
      newStatus: ["", [Validators.required]],
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.remarksDescription)
        ]
      ]
    });
  }

  viewUpdateStatusModal(aggregator, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.aggregatorDetails = aggregator;
    this.statusFormSubmitted = false;
    this.prepareUpdateStatusForm();
    this.loadStatusList();
    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  loadStatusList() {
    if (this.statusList.length == 0) {
      this.spinner.show();
      this.manageAggregatorService.getStatusList().subscribe(data => {
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          let tempList = data["data"];
          tempList = tempList.filter(
            x => x["id"] != this.aggregatorDetails["status"]
          );
          tempList = tempList.filter(x => x["id"] != 0);
          tempList = tempList.filter(x => x["id"] != 4);
          this.statusList = tempList;
          this.spinner.hide();
        }
      });
    }
  }

  updateUserStatus(test) {
    this.statusFormSubmitted = true;
    if (this.statusForm.valid) {
      this.spinner.show();
      let finalObj = {
        loginId: 0,
        status: this.statusForm.controls.newStatus.value,
        remarks: this.statusForm.controls.remarks.value
      };
      this.manageAggregatorService
        .updateAggregatorStatus(this.aggregatorDetails["id"], finalObj)
        .subscribe(data => {
          this.spinner.hide();
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessToast('update');
            this.modalRef.close();
            this.getUsersList();
          } else {
            this.commonService.showErrorToast(data["result"]["userMsg"])
          }
        });
    }
  }

  prepareDeleteUserForm() {
    this.deleteForm = this.formBuilder.group({
      remarks: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(200),
          Validators.pattern(this.commonService.patterns.remarksDescription)
        ]
      ]
    });
  }

  viewDeleteUserModal(aggregator, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.prepareDeleteUserForm();
    this.deleteFormSubmitted = false;
    this.aggregatorDetails = aggregator;
    this.modalRef = this.modalService.open(content, this.modalOptions);
    this.modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  deleteUser() {
    this.deleteFormSubmitted = true;

    if (this.deleteForm.valid) {
      Swal.fire({
        title: "Are you sure?",
        text: "Do you really want to delete this user?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ok, Proceed"
      }).then(result => {
        if (result.value) {
          this.spinner.show();

          let id = this.aggregatorDetails["id"];
          let remarks = this.deleteForm.controls.remarks.value;
          let loginId = 0;
          this.manageAggregatorService
            .deleteAggregator(id, remarks)
            .subscribe(data => {
              this.spinner.hide();
              if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
                this.commonService.showErrorToast('delete');
                this.modalRef.close();
                this.getUsersList();
              } else {
                this.commonService.showErrorToast(data["result"]["userMsg"])
              }
            });
        }
      });
    }
  }

  viewUser(id, content, btn) {
    btn &&
      btn.parentElement &&
      btn.parentElement.parentElement &&
      btn.parentElement.parentElement.blur();

    this.spinner.show();
    this.manageAggregatorService.getAggregator(id).subscribe(data => {
      if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
        this.aggregatorDetails = data["data"];

        this.spinner.hide();
        this.modalRef = this.modalService.open(content, this.modalOptions);
        this.modalRef.result.then(
          result => {
            this.closeResult = `Closed with: ${result}`;
          },
          reason => {
            this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
          }
        );
      } else {
        this.commonService.showErrorToast(data["result"]["userMsg"])
      }
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
}
