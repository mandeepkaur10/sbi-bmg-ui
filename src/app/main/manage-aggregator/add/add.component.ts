import { Component, OnInit } from "@angular/core";
import Stepper from "bs-stepper";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { CommonService } from "../../../common/common.service";
import { ManageAggregatorService } from "../manage-aggregator.service";
import Swal from "sweetalert2";
import { NgxSpinnerService } from "ngx-spinner";
import { RESPONSE } from "src/app/common/common.const";

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"],
  providers: [ManageAggregatorService]
})
export class AddComponent implements OnInit {
  INTERNATIONAL: string = "I";
  DOMESTIC: string = "D";

  HTTP: string = "HTTP";
  SMPP: string = "SMPP";

  aggregatorForm: FormGroup;
  formSubmitted: boolean = false;
  disableSubmit: boolean = false;

  aggregatorTypeList: Array<object> = [];
  aggregatorProtocolList: Array<object> = [];

  isEdit: boolean = false;
  aggregatorId = -1;
  aggregatorInfo: object = {};

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    public commonService: CommonService,
    private manageAggregatorService: ManageAggregatorService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (params["id"]) this.aggregatorId = params["id"];
    });

    if (this.aggregatorId != -1) {
      this.isEdit = true;
      this.fetchExistingAggregatorDetails();
    }

    this.prepareAggregatorForm();
    this.loadAggregatorTypeList();
    this.loadAggregatorProtocolList();
  }

  loadAggregatorTypeList() {
    this.aggregatorTypeList = this.manageAggregatorService.getAggregatorTypeList();
  }

  loadAggregatorProtocolList() {
    this.aggregatorProtocolList = this.manageAggregatorService.getAggregatorProtocolList();
  }

  prepareAggregatorForm() {
    this.aggregatorForm = this.formBuilder.group({
      aggregatorName: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
          Validators.pattern(this.commonService.patterns.name)
        ]
      ],
      aggregatorType: ["", [Validators.required]],
      tps: [
        "",
        [
          Validators.required,
          Validators.pattern(this.commonService.patterns.numberOnly)
        ]
      ],
      topic: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
          Validators.pattern(
            this.commonService.patterns.alphaNumericStartWithOnlyAlphabet
          )
        ]
      ],
      aggregatorProtocol: ["", [Validators.required]],
      url: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.url)
      ]),
      sourceIp: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.anyThing)
      ]),
      sourcePort: new FormControl({ disabled: true, value: "" }, [
        Validators.required,
        Validators.pattern(this.commonService.patterns.numberOnly)
      ]),
      userName: [
        "",
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
          Validators.pattern(
            this.commonService.patterns.alphaNumericStartWithOnlyAlphabet
          )
        ]
      ],
      passWord: [
        "",
        [Validators.required, Validators.minLength(2), Validators.maxLength(50)]
      ]
    });
  }

  fetchExistingAggregatorDetails() {
    if (this.aggregatorId != -1) {
      this.spinner.show();
      this.manageAggregatorService
        .getAggregator(this.aggregatorId)
        .subscribe(data => {
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.aggregatorInfo = data["data"];
            this.loadExistingAggregatorDetails();
          } else {
            this.commonService.showErrorToast(data['result']['userMsg']);
            this.router.navigate(["/main/manage-user/view-user"]);
          }
        });
    }
  }

  loadExistingAggregatorDetails() {}

  saveAggregator() {
    this.formSubmitted = true;
  }

  aggregatorProtocolChanged() {
    if (this.aggregatorForm.controls.aggregatorProtocol.value == this.HTTP) {
      this.aggregatorForm.controls.url.enable();
      this.aggregatorForm.controls.sourceIp.disable();
      this.aggregatorForm.controls.sourcePort.disable();
    } else if (
      this.aggregatorForm.controls.aggregatorProtocol.value == this.SMPP
    ) {
      this.aggregatorForm.controls.url.disable();
      this.aggregatorForm.controls.sourceIp.enable();
      this.aggregatorForm.controls.sourcePort.enable();
    }
  }

  cancel() {
    Swal.fire({
      title: "Are you sure?",
      text: "All information will be discarded!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ok, Left this page"
    }).then(result => {
      if (result.value) {
        this.router.navigate(["/main/manage-aggregator/view"]);
      }
    });
  }
}
