import { ManageAggregatorModule } from './manage-aggregator.module';

describe('ManageAggregatorModule', () => {
  let manageAggregatorModule: ManageAggregatorModule;

  beforeEach(() => {
    manageAggregatorModule = new ManageAggregatorModule();
  });

  it('should create an instance', () => {
    expect(manageAggregatorModule).toBeTruthy();
  });
});
