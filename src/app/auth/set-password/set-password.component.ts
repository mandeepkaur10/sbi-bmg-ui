import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from "@angular/forms";
import { AuthService } from "../auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from "src/app/common/common.service";
import { RESPONSE } from "src/app/common/common.const";

@Component({
  selector: "app-set-password",
  templateUrl: "./set-password.component.html",
  styleUrls: ["./set-password.component.scss"]
})
export class SetPasswordComponent implements OnInit {
  setPasswordForm: FormGroup;
  formSubmitted: boolean = false;
  isValidToken: boolean = false;
  userInfo: object;
  token: string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.initialiseForms();
    this.route.params.subscribe(params => {
      this.token = params["token"];
      this.validateToken(this.token);
    });
  }

  initialiseForms() {
    this.setPasswordForm = this.formBuilder.group({
      newPassword: ["", [Validators.required]],
      confirmNewPassword: [
        "",
        Validators.compose([
          Validators.required,
          this.matchNewAndConfirmPassword
        ])
      ]
    });
  }

  matchNewAndConfirmPassword(control: FormControl) {
    if (control.root && control.root["controls"]) {
      let confirmNewPassword = control.root["controls"][
        "confirmNewPassword"
      ].value.trim();
      let newPassword = control.root["controls"]["newPassword"].value.trim();
      if (
        confirmNewPassword.length > 0 &&
        newPassword.length > 0 &&
        confirmNewPassword != newPassword
      ) {
        return { confirmAndNewPasswordNotMatched: true };
      }
    }
    return null;
  }
  validateToken(token) {
    this.spinner.show();
    this.authService.validateToken(token).subscribe(
      data => {
        this.spinner.hide();
        if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
          this.isValidToken = true;
          this.userInfo = data["data"];
        } else {
          this.router.navigate(["/auth/login"]);
          this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
        }
      },
      error => {
        this.spinner.hide();
        this.router.navigate(["/auth/login"]);
        this.commonService.showErrorAlertMessage(
          error["error"]["result"]["userMsg"]
        );
      }
    );
  }

  submitForm() {
    this.formSubmitted = true;
    if (this.setPasswordForm.valid) {
      let obj = {
        username: this.userInfo["username"],
        password: this.setPasswordForm.controls["newPassword"].value
      };
      this.spinner.show();
      this.authService.setPassword(obj,this.token).subscribe(
        data => {
          this.spinner.hide();
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            sessionStorage.removeItem("_basicTkn");
            this.commonService.showSuccessAlertMessage(
              "Password updated successfully"
            );
            this.router.navigate(["/auth/login"]);
          } else {
            this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
          }
        },
        error => {
          this.spinner.hide();
          this.router.navigate(["/auth/login"]);
          this.commonService.showErrorAlertMessage(
            error["error"]["result"]["userMsg"]
          );
        }
      );
    }
  }
}
