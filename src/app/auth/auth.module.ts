import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthComponent } from "./auth.component";
import { LoginModule } from "./login/login.module";
import { routing } from "./auth.routing";
import { AuthService } from "./auth.service";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { SetPasswordComponent } from "./set-password/set-password.component";
import { SharedModule } from "../common/shared.modules";

@NgModule({
  imports: [CommonModule, routing, SharedModule],
  declarations: [AuthComponent, ForgotPasswordComponent, SetPasswordComponent],
  providers: [AuthService]
})
export class AuthModule {}
