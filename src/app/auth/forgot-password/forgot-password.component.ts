import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from "src/app/common/common.service";
import { RESPONSE } from "src/app/common/common.const";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"]
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  formSubmitted: boolean = false;

  constructor(
    private router: Router,
    private commonService: CommonService,
    private fb: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.initialiseForms();
  }

  initialiseForms() {
    this.forgotPasswordForm = this.fb.group({
      userName: ["", [Validators.required]]
    });
  }

  submitForm() {
    this.formSubmitted = true;
    // debugger
    if (this.forgotPasswordForm.valid) {
      let username = this.forgotPasswordForm.controls["userName"].value;
      this.spinner.show();
      this.authService.forgotPasswordLink(username).subscribe(
        data => {
          this.spinner.hide();
          if (data["result"]["statusDesc"] == RESPONSE.SUCCESS) {
            this.commonService.showSuccessAlertMessage(
              "Reset password link is sent to your register email id."
            );
            this.router.navigate(["/auth/login"]);
          } else {
            this.commonService.showErrorAlertMessage(data["result"]["userMsg"]);
          }
        },
        error => {
          this.spinner.hide();
          this.commonService.showErrorAlertMessage(
            error["error"]["result"]["userMsg"]
          );
        }
      );
    }
  }
}
