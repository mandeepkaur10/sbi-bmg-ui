import { AuthComponent } from "./auth.component";
import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { SetPasswordComponent } from "./set-password/set-password.component";

export const routes: Routes = [
  {
    path: "auth",
    component: AuthComponent,
    children: [
      { path: "", redirectTo: "/auth/login", pathMatch: "full" },
      {
        path: "login",
        loadChildren: "./login/login.module#LoginModule",
        data: { title: "Login" }
      },
      { path: "forgot-password", component: ForgotPasswordComponent },
      { path: "set-password/:token", component: SetPasswordComponent },
      { path: "**", redirectTo: "/auth/login" }
    ]
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
