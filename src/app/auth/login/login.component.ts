import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { HttpResponse } from "@angular/common/http";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  formSubmitted: boolean = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private authService: AuthService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.initialiseForms();
  }

  initialiseForms() {
    this.loginForm = this.fb.group({
      userName: ["", [Validators.required]],
      password: ["", [Validators.required]]
    });
  }

  login() {
    this.formSubmitted = true;
    // debugger
    if (this.loginForm.valid) {
      let req = {
        username: this.loginForm.controls["userName"].value,
        password: this.loginForm.controls["password"].value
      };
      this.spinner.show();
      // this.authService.validateLogin(req).subscribe(
      //   (res: HttpResponse<any>)  => {
      //     debugger
      //     console.log(res.headers.get('x-auth-token'));

      //     this.spinner.hide();
      //     sessionStorage.clear();
      //     this.router.navigate(['/main/manage-user/add-user']);
      //   });
      this.authService.validateLogin(req).subscribe((res: Response) => {
        debugger

        sessionStorage.setItem("_userdata", JSON.stringify(res["data"]));
        // console.log(res.headers.get('x-auth-token'));
        sessionStorage.removeItem("_basicTkn");
        this.spinner.hide();
        // sessionStorage.clear();
        this.router.navigate(["/main"]);
      });
    }
  }
}
