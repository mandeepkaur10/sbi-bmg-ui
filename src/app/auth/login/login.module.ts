import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import {Routes, RouterModule} from '@angular/router';
import { SharedModule } from "../../common/shared.modules";

const router: Routes = [
  {path: '', component: LoginComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(router),
    SharedModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
