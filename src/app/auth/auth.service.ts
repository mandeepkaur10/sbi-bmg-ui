import { Injectable } from "@angular/core";
import { NetworkService } from "../common/network.service";
import { Observable } from "rxjs";
import { CommonService } from "../common/common.service";

@Injectable()
export class AuthService {
  NO_HEADER = null;
  NO_AUHTORIZATION = null;
  NO_PARAMS = null;
  constructor(
    private networkService: NetworkService,
    private _commonService: CommonService
  ) {}

  validateLogin(req: any): Observable<Object> {
    let basicAuthToken = btoa(req["username"] + ":" + req["password"]);
    sessionStorage.setItem("_basicTkn", basicAuthToken);
    return this.networkService.get("login", null, "basic");
  }

  forgotPasswordLink(username) {
    return this.networkService.put("login/password/forgot/" + username);
  }

  validateToken(token) {
    return this.networkService.put("login/link/validate?link=" + token);
  }

  setPassword(req, token) {
    let basicAuthToken = btoa(req["username"] + ":" + req["password"]);
    sessionStorage.setItem("_basicTkn", basicAuthToken);
    return this.networkService.post(
      "login/password/set?link=" + token,
      this.NO_PARAMS,
      this.NO_HEADER,
      this._commonService.basic
    );
  }
}
