import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "../../environments/environment";
import { CommonService } from "../common/common.service";
@Injectable()
export class NetworkService {
  private backendApiURL = environment.backendApiURL;
  private backendApiURLMG = environment.backendApiURLMG;

  constructor(
    private http: HttpClient,
    private _commonService: CommonService
  ) {}

  getMG(url: any, head?: any, auth?: any): Observable<HttpResponse<any>> {
    debugger
    return this.http.get<HttpResponse<any>>(
      this.backendApiURLMG + url,
      this._commonService.getToken(auth)
    );
  }

  get(url: any, head?: any, auth?: any): Observable<HttpResponse<any>> {
    return this.http.get<HttpResponse<any>>(
      this.backendApiURL + url,
      this._commonService.getToken(auth)
    );
  }

  // getFile(url: any, head?: any, auth?: any): Observable<any> {
  //   return this.http.get(this.backendApiURL + url, {
  //     headers: this._commonService.getToken(auth),
  //     responseType: "blob" as "json"
  //   });
  // }

  getFile(url: any, head?: any, auth?: any): Observable<any> {
    if(auth){
      return this.http.get(this.backendApiURL + url, {
          headers: new HttpHeaders({        
              Authorization: "bearer " + sessionStorage.getItem("_bearerTkn")
              }),
            responseType: "blob" as "json"
        });
    }else{
      return this.http.get(this.backendApiURL + url, {
        headers: this._commonService.getToken(auth),
        responseType: "blob" as "json"
      });
    }
   
  }

  post(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.post(
      this.backendApiURL + url,
      param,
      this._commonService.getToken(auth)
    );
  }

  put(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.put(
      this.backendApiURL + url,
      param,
      this._commonService.getToken(auth)
    );
  }

  delete(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    return this.http.delete(
      this.backendApiURL + url,
      this._commonService.getToken(auth)
    );
  }
  uploadFile(url: any, param?: any, head?: any, auth?: any): Observable<any> {
    if(auth == "bearer"){
      return this.http.post(
        this.backendApiURL + url,
        param,
        {
          headers: new HttpHeaders({           
            Authorization: "bearer " + sessionStorage.getItem("_bearerTkn"),
            observe: "response",
            responseType: "text"
          })
        }
        );
     
    }else{
      return this.http.post(
        this.backendApiURL + url,
        param,
       this._commonService.getToken(auth)
      );
    }
    
  }
}

const fileUploadHeaders = {
  "Cache-Control": "no-cache",
  Pragma: "no-cache",
  "Process-Data": false
};
