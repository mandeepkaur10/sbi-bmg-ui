import { Injectable } from "@angular/core";
import { finalize, tap } from "rxjs/operators";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from "@angular/common/http";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { CommonService } from "../../common/common.service";

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {
  constructor(private router: Router, private spinner: NgxSpinnerService, private commonService: CommonService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return next.handle(req).pipe(
      tap(
        event => {
          try {            
            let authToken = event["headers"].get("x-auth-token");
            if (authToken) sessionStorage.setItem("_bearerTkn", authToken);
          } catch (err) { }
          if (
            event instanceof HttpResponse &&
            event["body"] &&
            event["body"]["result"] &&
            event["body"]["result"]["statusCode"] == 401
          ) {
            this.router.navigate(["/auth/login"]);
          }
          this.onSuccess(event);
        },
        error => {
          this.onError(error);
        }
      ),
      finalize(() => { })
    );
  }

  private onSuccess(event: any) {
  }

  private onError(error: any) {
    this.spinner.hide();
    this.commonService.showErrorToast(error['error']['result']['userMsg']);
    if (error['error']['result']['userMsg'].toLowerCase() == "Unauthorized access".toLowerCase()) {
      sessionStorage.clear();
      this.router.navigate([""]);
    }    
  }
}
